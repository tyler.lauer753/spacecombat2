
AddCSLuaFile()
DEFINE_BASECLASS( "player_default" )

if ( CLIENT ) then

	CreateConVar( "cl_playercolor", "0.24 0.34 0.41", { FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD }, "The value is a Vector - so between 0-1 - not between 0-255" )
	CreateConVar( "cl_weaponcolor", "0.30 1.80 2.10", { FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD }, "The value is a Vector - so between 0-1 - not between 0-255" )
	CreateConVar( "cl_playerskin", "0", { FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD }, "The skin to use, if the model has any" )
	CreateConVar( "cl_playerbodygroups", "0", { FCVAR_ARCHIVE, FCVAR_USERINFO, FCVAR_DONTRECORD }, "The bodygroups to use, if the model has any" )

end

local PLAYER = {}


PLAYER.DuckSpeed			= 0.1		-- How fast to go from not ducking, to ducking
PLAYER.UnDuckSpeed			= 0.1		-- How fast to go from ducking, to not ducking

--
-- Creates a Taunt Camera
--
PLAYER.TauntCam = TauntCamera()

--
-- See gamemodes/base/player_class/player_default.lua for all overridable variables
--
PLAYER.WalkSpeed 			= 200
PLAYER.RunSpeed				= 400

-- Set a Race specific colour, this will be used as an identifier
PLAYER.RaceColor			= Color(20,120,225,200)
PLAYER.suit = nil

function PLAYER:getRaceColor()
    return self.RaceColor
end

-- Specify variable to store Race name
PLAYER.RaceName				= "base"

function PLAYER:getRace()
    return self.RaceName
end

function PLAYER:getSuit()
	return self.suit
end

--
-- Set up the network table accessors
--
function PLAYER:SetupDataTables()

	BaseClass.SetupDataTables( self )

end


function PLAYER:Loadout()

	self.Player:RemoveAllAmmo()

	self.Player:Give( "gmod_tool" )
	self.Player:Give( "weapon_physgun" )
	self.Player:Give( "personal_miner" )

	self.Player:SwitchToDefaultWeapon()

end

function PLAYER:SetModel()

	BaseClass.SetModel( self )

	local skin = self.Player:GetInfoNum( "cl_playerskin", 0 )
	self.Player:SetSkin( skin )

	local groups = self.Player:GetInfo( "cl_playerbodygroups" )
	if ( groups == nil ) then groups = "" end
	local groups = string.Explode( " ", groups )
	for k = 0, self.Player:GetNumBodyGroups() - 1 do
		self.Player:SetBodygroup( k, tonumber( groups[ k + 1 ] ) or 0 )
	end

end

function PLAYER:GetIntakeAmount()
    if CLIENT then
        return self.IntakeAmount or 0
    else
        return self:getSuit():getIntake():GetAmount()
    end
end

function PLAYER:GetMaxIntakeAmount()
    if CLIENT then
        return self.IntakeAmount or 0
    else
        return self:getSuit():getIntake():GetMaxAmount()
    end
end

function PLAYER:GetOuttakeAmount()
    if CLIENT then
        return self.OuttakeAmount or 0
    else
        return self:getSuit():getOuttake():GetAmount()
    end
end

function PLAYER:GetMaxOuttakeAmount()
    if CLIENT then
        return self.OuttakeAmount or 0
    else
        return self:getSuit():getOuttake():GetMaxAmount()
    end
end

if CLIENT then
    net.Receive("SB4::PlayerUpdate", function()
        local ply = LocalPlayer()
        ply.IntakeAmount = net.ReadFloat()
        ply.IntakeMaxAmount = net.ReadFloat()
        ply.OuttakeAmount = net.ReadFloat()
        ply.OuttakeMaxAmount = net.ReadFloat()
		ply.Atmo_Temp = net.ReadFloat()
		ply.Atmo_O2Percent = net.ReadFloat()
		ply.Atmo_NPercent = net.ReadFloat()
		ply.Atmo_CO2Percent = net.ReadFloat()
    end)
else
    util.AddNetworkString("SB4::PlayerUpdate")
end

function PLAYER:ClientsideUpdate()
    net.Start("SB4::PlayerUpdate")
		net.WriteFloat(self:getSuit():getIntake():GetAmount())
		net.WriteFloat(self:getSuit():getIntake():GetMaxAmount())
		net.WriteFloat(self:getSuit():getOuttake():GetAmount())
		net.WriteFloat(self:getSuit():getOuttake():GetMaxAmount())

		if self.Player.GetEnvironment then
			local env = self.Player:GetEnvironment()
			local o2 = env:GetResource("Oxygen")
			local n = env:GetResource("Nitrogen")
			local co2 = env:GetResource("Carbon Dioxide")

			net.WriteFloat(env:GetTemperature())

			if o2 then
				local o2per = o2:GetAmount() / o2:GetMaxAmount()

				if o2per == o2per and o2per ~= math.huge then
					net.WriteFloat(o2per)
				else
					net.WriteFloat(0)
				end
			else
				net.WriteFloat(0)
			end

			if n then
				local nper = n:GetAmount() / n:GetMaxAmount()

				if nper == nper and nper ~= math.huge then
					net.WriteFloat(nper)
				else
					net.WriteFloat(0)
				end
			else
				net.WriteFloat(0)
			end

			if co2 then
				local co2per = co2:GetAmount() / co2:GetMaxAmount()

				if co2per == co2per and co2per ~= math.huge then
					net.WriteFloat(co2per)
				else
					net.WriteFloat(0)
				end
			else
				net.WriteFloat(0)
			end
		else
			net.WriteFloat(0)
			net.WriteFloat(0)
			net.WriteFloat(0)
			net.WriteFloat(0)
		end
    net.Send(self.Player)
end

--
-- Called when the player spawns
--
function PLAYER:Spawn()

	BaseClass.Spawn( self )

	local col = self.Player:GetInfo( "cl_playercolor" )
	self.Player:SetPlayerColor( Vector( col ) )

	local col = Vector( self.Player:GetInfo( "cl_weaponcolor" ) )
	if col:Length() == 0 then
		col = Vector( 0.001, 0.001, 0.001 )
	end
	self.Player:SetWeaponColor( col )

    -- Setup player suit here to bind to a player :D
	self.suit = GAMEMODE.class.getClass("PlayerSuit"):new( self.Player ) -- ply, breathable, respired ?
    self.Player.SB4Suit = self.suit
    self.Player.player_class = self

    self:ClientsideUpdate()

    local ply = self.Player
    local function update()
        if not IsValid(ply) then return end
        self:ClientsideUpdate()
        timer.Simple(1, update)
    end
    timer.Simple(1, update)
end

--
-- Return true to draw local (thirdperson) camera - false to prevent - nothing to use default behaviour
--
function PLAYER:ShouldDrawLocal()

	if ( self.TauntCam:ShouldDrawLocalPlayer( self.Player, self.Player:IsPlayingTaunt() ) ) then return true end

end

--
-- Allow player class to create move
--
function PLAYER:CreateMove( cmd )

	if ( self.TauntCam:CreateMove( cmd, self.Player, self.Player:IsPlayingTaunt() ) ) then return true end

end

--
-- Allow changing the player's view
--
function PLAYER:CalcView( view )

	if ( self.TauntCam:CalcView( view, self.Player, self.Player:IsPlayingTaunt() ) ) then return true end

	-- Your stuff here

end

function PLAYER:GetHandsModel()

	-- return { model = "models/weapons/c_arms_cstrike.mdl", skin = 1, body = "0100000" }

	local cl_playermodel = self.Player:GetInfo( "cl_playermodel" )
	return player_manager.TranslatePlayerHands( cl_playermodel )

end

--
-- Reproduces the jump boost from HL2 singleplayer
--
local JUMPING

function PLAYER:StartMove( move )

	-- Only apply the jump boost in FinishMove if the player has jumped during this frame
	-- Using a global variable is safe here because nothing else happens between SetupMove and FinishMove
	if bit.band( move:GetButtons(), IN_JUMP ) ~= 0 and bit.band( move:GetOldButtons(), IN_JUMP ) == 0 and self.Player:OnGround() then
		JUMPING = true
	end

end

function PLAYER:FinishMove( move )

	-- If the player has jumped this frame
	if JUMPING then
		-- Get their orientation
		local forward = move:GetAngles()
		forward.p = 0
		forward = forward:Forward()

		-- Compute the speed boost

		-- HL2 normally provides a much weaker jump boost when sprinting
		-- For some reason this never applied to GMod, so we won't perform
		-- this check here to preserve the "authentic" feeling
		local speedBoostPerc = ( ( not self.Player:Crouching() ) and 0.5 ) or 0.1

		local speedAddition = math.abs( move:GetForwardSpeed() * speedBoostPerc )
		local maxSpeed = move:GetMaxSpeed() * ( 1 + speedBoostPerc )
		local newSpeed = speedAddition + move:GetVelocity():Length2D()

		-- Clamp it to make sure they can't bunnyhop to ludicrous speed
		if newSpeed > maxSpeed then
			speedAddition = speedAddition - (newSpeed - maxSpeed)
		end

		-- Reverse it if the player is running backwards
		if move:GetForwardSpeed() < 0 then
			speedAddition = -speedAddition
		end

		-- Apply the speed boost
		move:SetVelocity(forward * speedAddition + move:GetVelocity())
	end

	JUMPING = nil

end

player_manager.RegisterClass( "player_sandbox", PLAYER, "player_default" )
