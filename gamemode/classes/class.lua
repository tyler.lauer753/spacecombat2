-- Class creation system, responsible for loading all classes and used for instantiation of objects

local GM = GM
local class = GM.class

local loadedClasses = {} -- We'll use this to store our classes in.

function class.new(name, ...)
	if type(name) ~= "string" then name = tostring(name) end

	if not class.getClass(name) then
		error("Class, ".. name .." not found")
		return
	else
		return loadedClasses[name]:new(...)
	end
end

function class.registerClass(name, classTable)
	if not class.getClass(name) then
		loadedClasses[name] = classTable
		print("Registered class "..name)
	end
end

function class.getClass(name)
	return loadedClasses[name]
end