-- For resources such as Oxygen, Nitrogen, Carbon Dioxide etc
local GM = GM

local C = GM.LCS.class({
	type = "",
	resources = {},
    usedstorage = 0,
	storage = 0
})

function C:init(type, storage, resources, usedstorage)
    self:SetType(type or self:GetType())
    self:Resize(storage or self.storage)
    self.resources = resources or self.resources
    self.usedstorage = usedstorage or self.usedstorage
end

function C:GetUsed()
    return self.usedstorage
end

function C:GetSize()
    return self.storage
end

function C:Resize(size)
    if size < self.usedstorage then return false end
    self.storage = size
    return true
end

function C:GetType()
	return self.type
end

function C:SetType( n )
	self.type = n
end

function C:GetStored()
    return self.resources
end

function C:GetResource(name)
    return self.resources[name]
end

function C:GetAmount(name)
    if not name or not self.resources[name] then return 0 end
	return self.resources[name]:GetAmount()
end

function C:SetAmount(name, a)
    if not name or not self.resources[name] then return end
	self.resources[name]:SetAmount(a)
end

function C:GetMaxAmount(name)
    if not name or not self.resources[name] then return 0 end
	return self.resources[name]:GetMaxAmount()
end

function C:SetMaxAmount(name, a)
    if not name or not self.resources[name] then return false end
    local delta = (a - self:GetMaxAmount(name)) * self.resources[name]:GetSize()
    local fits = (self.usedstorage + delta) <= self.storage

    if fits then
	    self.resources[name]:SetMaxAmount(a)
        self.usedstorage = self.usedstorage + delta
        return true
    end

    return false
end

function C:IncreaseMaxAmount(name, a)
    if not self.resources[name] then
        self:AddResourceType(name, a)
        return
    end

    self:SetMaxAmount(name, a + self:GetMaxAmount(name))
end

function C:DecreaseMaxAmount(name, a)
    if not self.resources[name] then
        return
    end

    self:SetMaxAmount(name, self:GetMaxAmount(name) - a)
end

function C:SupplyResource(name, amount)
    if not name or not self.resources[name] then return false end
    return self.resources[name]:SupplyResource(amount)
end

function C:ConsumeResource(name, amount, force)
    if not name or not self.resources[name] then return false end
    return self.resources[name]:ConsumeResource(amount, force)
end

function C:AddResourceType(name, max)
    if not name or self.resources[name] ~= nil then return false end
    local resource = GM:NewResource(name)
    if not resource or resource:GetType() ~= self.type then return false end

    self.resources[name] = resource

    if max ~= nil then
        self:SetMaxAmount(name, max)
    end

    return true
end

function C:RemoveResourceType(name)
    if not name or self.resources[name] then return false end

    self.resources[name] = nil
    return true
end

function C:MergeStorage(storage)
    if storage == nil or storage:GetType() ~= self:GetType() then return end
    self:Resize(self.storage + storage.storage)

    local res = storage.resources
    for i, k in pairs(res) do
        self:IncreaseMaxAmount(i, k:GetMaxAmount())
        self:SupplyResource(i, k:GetAmount())
        storage:SetAmount(i, 0)
    end
end

function C:SplitStorage(storage)
    if storage == nil or storage:GetType() ~= self:GetType() then return end

    for i,k in pairs(storage.resources) do
        local taken = math.Max(0, storage:GetMaxAmount(i) - (self:GetMaxAmount(i) - self:GetAmount(i)))
        storage:SetAmount(i, taken)
        self:ConsumeResource(i, taken)
        self:DecreaseMaxAmount(i, storage:GetMaxAmount(i))
    end
end

function C:WriteCreationPacket()
    if not SERVER then return end

    net.WriteFloat(self.usedstorage)
    net.WriteFloat(self.storage)

    local ResourceCount = table.Count(self.resources)
    net.WriteUInt(ResourceCount, 16)
    if ResourceCount > 0 then
        for i,k in pairs(self.resources) do
            net.WriteInt(k:GetID(), 12)
            k:WriteCreationPacket()
        end
    end
end

function C:ReadCreationPacket()
    if not CLIENT then return end

    self.usedstorage = net.ReadFloat()
    self.storage = net.ReadFloat()

    local TotalResources = net.ReadUInt(16)

    if TotalResources > 0 then
        for I = 1, TotalResources do
            local NewResourceID = GM:GetResourceFromID(net.ReadInt(12)) or "FAIL"
            local NewResource = GM.class.getClass("Resource"):new()
            NewResource:ReadCreationPacket()

            self.resources[NewResourceID] = NewResource
        end
    end

    self:init()
end

function C:Serialize()
    local res = {}
    for i,k in pairs(self.resources) do
        res[i] = k:Serialize()
    end

    return {type = self.type, storage = self.storage, usedstorage = self.usedstorage, resources = res}
end

function C:DeSerialize(data)
    local res = {}
    for i,k in pairs(data.resources) do
        local new = GM.class.getClass("Resource"):new()
        new:DeSerialize(k)
        res[i] = new
    end
    self:init(data.type, data.storage, res, data.usedstorage)
end

GM.class.registerClass("ResourceContainer", C)