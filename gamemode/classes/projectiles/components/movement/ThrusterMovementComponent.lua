local C = GM.class.getClass("MovementComponent"):extends({
    Thrust = 0
})

local BaseClass = C:getClass()

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("ThrusterMovementComponent")

function C:init(Thrust)
    BaseClass.init(self)

    self.Thrust = Thrust or 0
end

function C:PostComponentInit()
    self:SetRelativeAcceleration(Vector(self.Thrust, 0, 0))
end

function C:OnProjectileEvent(Event, Info)
    if Event == "StopThrusters" then
        self:SetAcceleration(Vector(0, 0, 0))
    end

    BaseClass.OnProjectileEvent(self, Event, Info)
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "ThrusterMovementComponent"
end

-- This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end
    BaseClass.WriteCreationPacket(self)

    net.WriteFloat(self.Thrust)
end

-- This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end
    BaseClass.ReadCreationPacket(self)

    self.Thrust = net.ReadFloat()
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)

    Data.Thrust = self.Thrust

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)
    self.Thrust = Data.Thrust or 0
    self:SetRelativeAcceleration(Vector(self.Thrust, 0, 0))
end

GM.class.registerClass("ThrusterMovementComponent", C)