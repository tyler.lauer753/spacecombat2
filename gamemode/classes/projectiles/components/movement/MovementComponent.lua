local C = GM.class.getClass("ProjectileComponent"):extends({
    Velocity = Vector(0, 0, 0),
    AngularVelocity = Angle(0, 0, 0),
    Acceleration = Vector(0, 0, 0),
    AngularAcceleration = Angle(0, 0, 0),
    ClientsidePrediction = true,
    ShouldUpdateClient = false,
    IgnoreProjectileMass = false,
})

local BaseClass = C:getClass()

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("MovementComponent")

function C:init(Velocity, AngularVelocity, Acceleration, AngularAcceleration)
    self.Velocity = Velocity or Vector(0, 0, 0)
    self.AngularVelocity = AngularVelocity or Angle(0, 0, 0)
    self.Acceleration = Acceleration or Vector(0, 0, 0)
    self.AngularAcceleration = AngularAcceleration or Angle(0, 0, 0)

    BaseClass.init(self)
end

function C:SetVelocity(NewVelocity)
    self.Velocity = NewVelocity
    self.ShouldUpdateClient = true
end

function C:GetVelocity()
    return self.Velocity
end

function C:SetRelativeVelocity(NewVelocity)
    self.Velocity = LocalToWorld(NewVelocity, Angle(0, 0, 0), Vector(0, 0, 0), self:GetParent():GetAngles())
    self.ShouldUpdateClient = true
end

function C:AddRelativeVelocity(NewVelocity)
    self.Velocity = self.Velocity + LocalToWorld(NewVelocity, Angle(0, 0, 0), Vector(0, 0, 0), self:GetParent():GetAngles())
    self.ShouldUpdateClient = true
end

function C:SetAngularVelocity(NewVelocity)
    self.AngularVelocity = NewVelocity
    self.ShouldUpdateClient = true
end

function C:GetAngularVelocity()
    return self.AngularVelocity
end

function C:SetRelativeAngularVelocity(NewVelocity)
    local Pos, Ang = LocalToWorld(Vector(0, 0, 0), NewVelocity, Vector(0, 0, 0), self:GetParent():GetAngles())
    self.AngularVelocity = Ang
    self.ShouldUpdateClient = true
end

function C:AddRelativeAngularVelocity(NewVelocity)
    local Pos, Ang = LocalToWorld(Vector(0, 0, 0), NewVelocity, Vector(0, 0, 0), self:GetParent():GetAngles())
    self.AngularVelocity = self.AngularVelocity + Ang
    self.ShouldUpdateClient = true
end

function C:SetAcceleration(NewAcceleration)
    self.Acceleration = NewAcceleration
    self.ShouldUpdateClient = true
end

function C:GetAcceleration()
    return self.Acceleration
end

function C:SetRelativeAcceleration(NewAcceleration)
    self.Acceleration = LocalToWorld(NewAcceleration, Angle(0, 0, 0), Vector(0, 0, 0), self:GetParent():GetAngles())
    self.ShouldUpdateClient = true
end

function C:SetAngularAcceleration(NewAcceleration)
    self.AngularAcceleration = NewAcceleration
    self.ShouldUpdateClient = true
end

function C:GetAngularAcceleration()
    return self.AngularAcceleration
end

function C:SetRelativeAngularAcceleration(NewAcceleration)
    local Pos, Ang = LocalToWorld(Vector(0, 0, 0), NewAcceleration, Vector(0, 0, 0), self:GetParent():GetAngles())
    self.AngularAcceleration = Ang
    self.ShouldUpdateClient = true
end

function C:SetParent(NewParent)
    BaseClass.SetParent(self, NewParent)

    if self:GetParent() ~= nil then
        self:GetParent():SetMovementComponent(self)
    end
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return true
end

-- If this returns true then the C:Think function will be called later than normal
-- Useful when you need to do something after the projectile has moved.
function C:UsesLateThink()
    return false
end

function C:Think()
    local Parent = self:GetParent()
    if not Parent then return end
    if CLIENT and not self.ClientsidePrediction then return end

    local Acceleration = self:GetAcceleration()
    local AngularAcceleration = self:GetAngularAcceleration()
    local Velocity = self:GetVelocity()
    local AngularVelocity = self:GetAngularVelocity()
    local Delta = FrameTime()
    local Mod = self.IgnoreProjectileMass and 1 or self:GetParent():GetMass()

    if not Acceleration:IsZero() then
        Velocity = Velocity + ((Acceleration / Mod) * Delta)
        self:SetVelocity(Velocity)
    end

    if not AngularAcceleration:IsZero() then
        AngularVelocity = AngularVelocity + ((AngularAcceleration / Mod) * Delta)
        self:SetAngularVelocity(AngularVelocity)
    end

    if not Velocity:IsZero() then
        Parent:SetPos(Parent:GetPos() + (Velocity * Delta))
    end

    if not AngularVelocity:IsZero() then
        local NewAngle = Parent:GetAngles() + (AngularVelocity * Delta)
        NewAngle:Normalize()

        Parent:SetAngles(NewAngle)
    end
end

function C:ReadNetworkUpdate()
    if self.ClientsidePrediction then
        local HasUpdate = net.ReadBool()
        if HasUpdate then
            self.Velocity = net.ReadVector()
            self.AngularVelocity = net.ReadAngle()
            self.Acceleration = net.ReadVector()
            self.AngularAcceleration = net.ReadAngle()
        end
    end
end

function C:SendNetworkUpdate()
    if self.ClientsidePrediction then
        net.WriteBool(self.ShouldUpdateClient)

        if self.ShouldUpdateClient then
            net.WriteVector(self.Velocity)
            net.WriteAngle(self.AngularVelocity)
            net.WriteVector(self.Acceleration)
            net.WriteAngle(self.AngularAcceleration)
            self.ShouldUpdateClient = false
        end
    end
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return true
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "MovementComponent"
end

-- This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end

    net.WriteVector(self.Velocity)
    net.WriteAngle(self.AngularVelocity)
    net.WriteVector(self.Acceleration)
    net.WriteAngle(self.AngularAcceleration)
    net.WriteBool(self.ClientsidePrediction)
    net.WriteBool(self.IgnoreProjectileMass)
end

-- This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end

    self.Velocity = net.ReadVector()
    self.AngularVelocity = net.ReadAngle()
    self.Acceleration = net.ReadVector()
    self.AngularAcceleration = net.ReadAngle()
    self.ClientsidePrediction = net.ReadBool()
    self.IgnoreProjectileMass = net.ReadBool()
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)
    Data.Velocity = self.Velocity
    Data.AngularVelocity = self.AngularVelocity
    Data.Acceleration = self.Acceleration
    Data.AngularAcceleration = self.AngularAcceleration
    Data.ClientsidePrediction = self.ClientsidePrediction
    Data.IgnoreProjectileMass = self.IgnoreProjectileMass

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)
    self.Velocity = Data.Velocity
    self.AngularVelocity = Data.AngularVelocity
    self.Acceleration = Data.Acceleration
    self.AngularAcceleration = Data.AngularAcceleration
    self.ClientsidePrediction = Data.ClientsidePrediction
    self.IgnoreProjectileMass = Data.IgnoreProjectileMass
end

GM.class.registerClass("MovementComponent", C)