local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
    HitEffect = "explosion",
    bUsesProjectile = false
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("HitEffectComponent")

function C:SpawnHitEffect(HitInfo)
    if CLIENT then return end
    if self.bUsesProjectile then
        local Position = HitInfo.HitPos
        local Angles = HitInfo.HitNormal:Angle()
        local ComponentClasses = {}

        table.insert(ComponentClasses, self.HitEffect)

        local SpawnData = {
            Position = Vector(0, 0, 0),
            Angles = Angle(0, 0, 0),
            Components = {
                {
                    Color = self:GetColor(),
                    ComponentClass = self.HitEffect,
                    Scale = self:GetScale(),
                    Offset = self.Offset
                }
            },
            Modifiers = {},
            Configuration = {}
        }

        SpawnData.Position = Position
        SpawnData.Angles = Angles
        SpawnData.Owner = self:GetParent():GetOwner()

        local Projectile = GAMEMODE.class.new("Projectile")
        Projectile:DeSerialize(SpawnData)
        if Projectile then
            Projectile.ShouldSendNetUpdates = false
            Projectile:Spawn()
        end
    else
        local effectdata = {}
        effectdata.Start = HitInfo.HitPos
        effectdata.Origin = HitInfo.HitPos
        effectdata.Normal = HitInfo.HitNormal
        effectdata.Magnitude = math.random() + 2 / 2 + 0.25
        effectdata.Scale = self:GetScale()

        SC.CreateEffect(self.HitEffect, effectdata)
    end
end

function C:OnProjectileEvent(Event, Info)
    if Event == "Collision" then
        self:SpawnHitEffect(Info)
    end
end

function C:ShouldThink()
    return false
end

function C:IsClientside()
    return false
end

function C:Serialize()
    -- Call the Serialize function on the base class first!
    local Out = BaseClass.Serialize(self)

    Out.bUsesProjectile = self.bUsesProjectile
    Out.HitEffect = self.HitEffect

    return Out
end

function C:DeSerialize(Data)
    -- Call the DeSerialize function from the base class first!
    BaseClass.DeSerialize(self, Data)

    self.bUsesProjectile = Data.bUsesProjectile
    self.HitEffect = Data.HitEffect
end

function C:GetComponentClass()
    return "HitEffectComponent"
end

GM.class.registerClass("HitEffectComponent", C)