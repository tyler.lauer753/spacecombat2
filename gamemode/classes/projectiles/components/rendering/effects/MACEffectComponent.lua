--[[
/**********************************************
	Scalable Railgun Projectile Effect

	Author: Steeveeo
***********************************************/
--]]
local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
    TrailLength = 3500,
    CurTrailLength = 0,
    Velocity = -15000,
    Trails = {
        Vector(-95, 0, 100),
        Vector(-95, 0, -100)}
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("MACEffectComponent")

local trailMat = Material("effects/ar2ground2")
local spriteFlareMat = Material("effects/flares/halo-flare_001")
local spriteCoreMat = Material("sprites/light_glow03")

function C:Think()
	local Parent = self:GetParent()
	--Grow trail (so it doesn't shoot out the back of the gun)
	self.CurTrailLength = math.Clamp(self.CurTrailLength + (self.Velocity * FrameTime()), 0, self.TrailLength)
	--Spinny Particle Trail
	for i = 1, 2 do
		local trail = self.Trails[i]
        local pos = LocalToWorld(trail, Angle(0,0,0), Parent:GetPos(), Parent:GetAngles())

		for ii = 1, 5 do
			local velocity = Parent:GetAngles():Forward() * (self.Velocity * 0.25) + (VectorRand() * math.Rand(10, 20))

			local p = self:GetEmitter():Add("effects/flares/light-rays_001", pos)
			p:SetDieTime(math.Rand(1.5, 2.5))
			p:SetVelocity(velocity)
			p:SetAirResistance(300)
			p:SetStartAlpha(255)
			p:SetEndAlpha(255)
			p:SetStartSize(math.random(15, 50) * self:GetScale())
			p:SetEndSize(0)
			p:SetColor(200, 200, 255)
		end
	end

	--Fire
	for i = 1, 5 do
		local velocity = Parent:GetAngles():Forward() * (self.Velocity * math.Rand(0, 0.75)) + (VectorRand() * math.Rand(300, 500))

		local p = self:GetEmitter():Add("effects/shipsplosion/smoke_00" .. math.random(1,4) .. "add", Parent:GetPos() - (Parent:GetAngles():Forward() * 120))
		p:SetDieTime(math.Rand(0.25, 0.5))
		p:SetVelocity(velocity)
		p:SetAirResistance(300)
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(math.random(25, 100) * self:GetScale())
		p:SetEndSize(0)
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(-30)
		p:SetColor(255, math.random(96, 196), math.random(32, 96))
	end
end

function C:SetScale(NewScale)
    BaseClass.SetScale(self, NewScale)
    self.TrailLength = NewScale * 3500
end

function C:Draw()
    local Parent = self:GetParent()

	render.SetMaterial(trailMat)
	render.DrawBeam(Parent:GetPos(), Parent:GetPos() + (Parent:GetAngles():Forward() * -self.CurTrailLength), 10 * self:GetScale(), 1, 0, Color(144, 196, 255, 255))

	render.SetMaterial(spriteFlareMat)
	render.DrawSprite(Parent:GetPos() - (Parent:GetAngles():Forward() * 120), self:GetScale() * 1000, self:GetScale() * 1000, Color(255, 240, 128))

	render.SetMaterial(spriteCoreMat)
	render.DrawSprite(Parent:GetPos() - (Parent:GetAngles():Forward() * 120), self:GetScale() * 350, self:GetScale() * 350, Color(255, 255, 255, 255))

end

function C:GetComponentClass()
    return "MACEffectComponent"
end

GM.class.registerClass("MACEffectComponent", C)