local GM = GM
local BaseClass = GM.class.getClass("EffectComponent")
local C = BaseClass:extends({
    Length = 0,
    MaxLength = 1000
})

-- We want to register ourselves as a networked component so we can be sent to the client
-- This MUST be called for our component if we set IsClientside to true!
local NetworkID = GM.Projectiles.RegisterNetworkedComponent("PulseBeamEffectComponent")

function C:Think()
    local Parent = self:GetParent()
    local Scale = self:GetScale()
    local NewScale = Scale - ((Scale * 15) * FrameTime())

    if NewScale <= 0.0001 then
        Parent:Remove()
        return
    end

    self:SetScale(NewScale)

    if SERVER then return end

    if self.MaxLength <= 0 then
        self.MaxLength = 40000
    end

    local Data = {}
    Data.start = Parent:GetPos()
    Data.endpos = Parent:GetPos() + (Parent:GetAngles():Forward() * self.MaxLength)
    Data.filter = {Parent:GetLauncherEntity()}
    local Trace = util.TraceLine(Data)

    if Trace.Hit then
        self.Length = Parent:GetPos():Distance(Trace.HitPos)
    else
        self.Length = self.MaxLength
    end
end

local bMats = {}
if CLIENT then
    bMats.Glow23 = GM.MaterialFromVMT(
    "bluelaser",
    [["UnlitTwoTexture"

    {
        "$baseTexture"  "effects/bluelaser1"
        "$Texture2" "effects/bluelaser1_smoke"
        "$nocull" "1"
        "$nodecal" "1"
        "$additive" "1"
        "$no_fullbright" 1
        "$model" "1"
        "$nofog" "1"


        "Proxies"
        {
            "TextureScroll"
            {
                "texturescrollvar" "$texture2transform"
                "texturescrollrate" 1
                "texturescrollangle" 0
            }

            "Sine"
            {
                "sineperiod"	.06
                "sinemin"	".3"
                "sinemax"	".5"
                "resultVar"	"$alpha"
            }
        }
    }]]
    )

    bMats.Glow24 = Material("effects/blueflare1")
    bMats.Glow24:SetMaterialInt("$selfillum", 10)
    bMats.Glow24:SetMaterialInt("$illumfactor",8)
    local lMats = {}
    lMats.Glow13 = Material("effects/blueblacklargebeam")
    lMats.Glow13:SetMaterialInt("$vertexalpha", 1)
    lMats.Glow13:SetMaterialInt("$vertexcolor", 1)
    lMats.Glow13:SetMaterialInt("$additive", 1)
    lMats.Glow13:SetMaterialInt("$nocull", 1)
    lMats.Glow13:SetMaterialInt("$selfillum", 10)
    lMats.Glow13:SetMaterialInt("$illumfactor",8)
    lMats.Glow23 = GM.MaterialFromVMT(
        "sc_blue_beam02",
        [["UnLitGeneric"
        {
            "$basetexture"		"effects/blueblacklargebeam"
            "$nocull" 1
            "$additive" 1
            "$vertexalpha" 1
            "$vertexcolor" 1
            "$illumfactor" 8
        }]]
    )
end

function C:Draw()
    local Forward = self:GetParent():GetAngles():Forward()
    local Start = self:GetParent():GetPos()
    local Scale = self:GetScale()
    local End = Start + Forward * self.Length

    local Size = math.random(200,400) * self:GetScale()
    local spriteStartOffset = (Start - End):Angle():Forward() * (25 * Scale)
    local spriteEndOffset = (Start - End):Angle():Forward() * (-25 * Scale)

    render.SetMaterial( bMats.Glow24 )

    render.DrawSprite(Start + spriteStartOffset, Size*2 , Size*2, self:GetColor())
    render.DrawSprite(Start + spriteStartOffset, Size, Size , Color(255, 255, 255, 255))
    render.DrawSprite(End + spriteEndOffset, Size*2 , Size*2, self:GetColor())
    render.DrawSprite(End + spriteEndOffset, Size, Size , Color(255, 255, 255, 255))

    render.SetMaterial( bMats.Glow23 )
    render.DrawBeam( End, Start, (100 + (15 * math.sin(CurTime() * (25 + math.Rand(1, 15))))) * Scale, 110, 0, Color( 0, 255, 0, 200 ) )

    render.SetMaterial( bMats.Glow24 )
    render.DrawBeam( End, Start, 100 * Scale, 0.5, 0.5, self:GetColor() )
    render.DrawBeam( End, Start, 100 * Scale, 0.5, 0.5, self:GetColor() )
    render.DrawBeam( End, Start, 100 * Scale, 0.5, 0.5, self:GetColor() )
    render.DrawBeam( End, Start, (50 + (25 * math.sin(CurTime() * 45))) * Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )
    render.DrawBeam( End, Start, 50 * Scale, 0.5, 0.5, Color( 255, 255, 255, 255 ) )
end

-- This function is used to send data to the client for replication.
function C:WriteCreationPacket()
    if not SERVER then return end
    BaseClass.WriteCreationPacket(self)

    net.WriteFloat(self.MaxLength)
end

-- This function reads the data sent to the client with the WriteCreationPacket function.
function C:ReadCreationPacket()
    if not CLIENT then return end
    BaseClass.ReadCreationPacket(self)

    self.MaxLength = net.ReadFloat()
end

function C:Serialize()
    local Data = BaseClass.Serialize(self) or {}

    Data.MaxLength = self.MaxLength

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)

    self.MaxLength = Data.MaxLength or 0
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "PulseBeamEffectComponent"
end

GM.class.registerClass("PulseBeamEffectComponent", C)