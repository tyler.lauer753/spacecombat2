local C = GM.class.getClass("DamageComponent"):extends({
    Radius = 500,
    Velocity = 10000,
    Falloff = 500,
    RemoveOnDetonate = true,
    Detonated = false
})

local BaseClass = GM.class.getClass("DamageComponent")

function C:init(Radius, Velocity, Falloff, Damage, RemoveOnDetonate)
    self.Radius = Radius or 500
    self.Velocity = Velocity or 10000
    self.Falloff = Falloff or 500

    if RemoveOnDetonate == false then
        self.RemoveOnDetonate = false
    end

    BaseClass.init(self, Damage)
end

function C:OnProjectileEvent(Event, Info)
    if Event == "Detonate" or Event == "Collision" then
        self:Detonate()
        return
    end

    BaseClass.OnProjectileEvent(self, Event, Info)
end

function C:Detonate()
    if not SERVER or self.Detonated then return end
    local Parent = self:GetParent()
    SC.Explode(Parent:GetPos(), self.Radius, self.Falloff, self.Velocity, self:GetDamage(), Parent:GetOwner(), Parent, false, {})
    self.Detonated = true
    if self.RemoveOnDetonate and IsValid(Parent) then
        Parent:FireNetworkedEvent("Collision", {
            Entity = nil,
            Fraction = 1,
            FractionLeftSolid = 1,
            Hit = true,
            HitBox = -1,
            HitGroup = HITGROUP_GENERIC,
            HitNoDraw = false,
            HitNonWorld = true,
            HitNormal = Parent:GetAngles():Forward(),
            HitPos = Parent:GetPos(),
            HitSky = true,
            HitTexture = "",
            HitWorld = true,
            MatType = 0,
            Normal = Parent:GetAngles():Forward(),
            PhysicsBone = 0,
            StartPos = Vector(0, 0, 0),
            SurfaceProps = 1,
            StartSolid = false,
            AllSolid = false
        })
        Parent:Remove()
    end
end

-- If this returns true then the C:Think function will be called. If it returns false, then it won't get called.
function C:ShouldThink()
    return false
end

-- If this returns true then the component will be networked to the client using C:Serialize and C:DeSerialize.
function C:IsClientside()
    return false
end

function C:Serialize()
    local Data = BaseClass.Serialize(self)

    Data.Radius = self.Radius
    Data.Velocity = self.Velocity
    Data.Falloff = self.Falloff
    Data.RemoveOnDetonate = self.RemoveOnDetonate

    return Data
end

function C:DeSerialize(Data)
    BaseClass.DeSerialize(self, Data)

    self.Radius = Data.Radius or 500
    self.Velocity = Data.Velocity or 10000
    self.Falloff = Data.Falloff or 500
    if Data.RemoveOnDetonate == false then
        self.RemoveOnDetonate = false
    else
        self.RemoveOnDetonate = true
    end
end

-- This should always return the name of the class!
function C:GetComponentClass()
    return "ExplosiveComponent"
end

GM.class.registerClass("ExplosiveComponent", C)