local GM = GM

local C = GM.class.getClass("Environment"):extends({
	name = "Space",
	gravity = 1/math.pow(10,30),
	atmosphere = 0,
	temperature = 14,
	pressure = 0,
	radius = 0,
	resources = {},
	entities = {},
    children = {},
	celestial = nil,
	habitable = false
})

function C:updateEntities()
	for _,v in pairs(self:GetEntities()) do
		if IsValid(v) then
            if v.GetEnvironment and v:GetEnvironment() ~= self then
				self:RemoveEntity(v)
			end
            self:updateEntity(v)
        else
            self:GetEntities()[_] = nil
		end
	end
end

function C:ContainsPosition(pos)
    return true --Space contains all positions, and is the root node of the environment graph
end

function C:SetHabitable(bool)
	self.habitable = bool

	self:SetName(bool and "Habitable Atmosphere" or "Space")
	self:SetGravity(bool and 1 or (1/math.pow(10,30)))
	self:SetTemperature(bool and 300 or 14)
	self:SetAtmosphere(bool and 1 or 0)
	self:SetRadius(0)
	self:SetPressure(self:GetGravity() * self:GetAtmosphere())
	self:SetNoclip(bool)
end

function C:GetHabitable()
	return self.habitable
end

function C:GetResTotal()
	return self.habitable and 1e30 or 0
end

function C:GetResource( r )
	return GM:NewResource(r, self.habitable and 1e30 or 0, self.habitable and 1e30 or 0)
end

function C:GetResourceAmount( r )
	return self.habitable and 1e30 or 0
end

function C:GetResources()
	local out = {}

	if self.habitable then
		out = {
			GM:NewResource("Oxygen", 1e30, 1e30),
			GM:NewResource("Water", 1e30, 1e30),
			GM:NewResource("Hydrogen", 1e30, 1e30),
			GM:NewResource("Nitrogen", 1e30, 1e30),
			GM:NewResource("Carbon Dioxide", 1e30, 1e30),
			GM:NewResource("Carbon Monoxide", 1e30, 1e30),
			GM:NewResource("Methane", 1e30, 1e30)
		}
	end

	return out
end

function C:AddResource( r, a )
	return
end

function C:SetResource( r, a )
	return
end

function C:IsHostile()
    if not self.habitable then return self:super('IsHostile') end

	return true, 0
end

function C:GetPlayerSpeedModifier(ply)
	if self.habitable then return 1 end
	return 1/1e30
end

function C:GetNoclip(ply)
	local adminnoclip = GetConVar("SB_AdminSpaceNoclip"):GetBool()
    local planetonly = GetConVar("SB_PlanetNoClipOnly"):GetBool()

    return self.habitable or not planetonly or (adminnoclip and ply:IsAdmin())
end

GM.class.registerClass("Space", C)
