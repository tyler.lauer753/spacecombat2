local GM = GM

local C = GM.class.getClass("HudComponent"):extends({
	color = Color(255,255,255,255),
	width = 0,
	height = 0,
	xalign = TEXT_ALIGN_LEFT,
	yalign = TEXT_ALIGN_LEFT,
	ftext = "",
	font = "SBHud" -- This is a custom font set in cl_fonts
})

function C:init(x, y, clr, ftext, xalign, yalign)
	self:super('init', x, y)
	self:setColor( clr )
	self:setText(ftext)
	self:setXAlign(xalign or self.xalign)
	self:setYAlign(yalign or self.yalign)
end

function C:getFont()
	return self.font
end

function C:setFont(f)
	if type(f) ~= "string" then error("Expected string, got "..type(f) ) return end
	self.font = f
end

function C:getText()
	return self.ftext
end

function C:setText(s)
	if type(s) ~= "string" then error("Exptected String, got "..type(s)) return end
	self.ftext = s
end

function C:getXAlign()
	return self.xalign
end

function C:setXAlign(a)
	if type(a) ~= "number" or a > 5 or a < 0 then error("Unexpected align type") return end
	self.xalign = a
	return
end

function C:getYAlign()
	return self.yalign
end

function C:setYAlign(a)
	if type(a) ~= "number" or a > 5 or a < 0 then error("Unexpected align type") return end
	self.yalign = a
	return
end

function C:getColor()
	return self.color
end

function C:setColor(c)
	if type(c) ~= "table" or not c.a or not c.r or not c.g or not c.b then error("Expected table ( Color ), got "..type(c)) return end
	self.color = c
	return
end

local function calcWidth(self)
	surface.SetFont(self:getFont())
	local w,h = surface.GetTextSize(self:getText())
	return w
end

local function calcHeight(self)
	surface.SetFont(self:getFont())
	local w,h = surface.GetTextSize(self:getText())
	return h
end

function C:getWidth()
	self.width = calcWidth(self)
	return self.width
end

function C:getHeight()
	self.height = calcHeight(self)
	return self.height
end

function C:setWidth()
	-- Nothing!
	-- We want to override default behaviour for get/set width
	return
end

function C:setHeight()
	-- Nothing!
	-- We want to override default behaviour for get/set height
	return
end

function C:render()
	self:super('render')
	if self:isVisible() then
		draw.SimpleText(self:getText(), self:getFont(), self:getX(), self:getY(), self:getColor(), self:getXAlign(), self:getYAlign())
	end
end

GM.class.registerClass("TextElement", C)