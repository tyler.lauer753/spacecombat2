--[[
	Space Combat Player Inventory - Q Menu Tab Container
	Author: Steve "Steeveeo" Green

	Note: This is just here so we can have an inventory panel in the
	Q menu. The InventoryPanel object is for use in any player-accessible
	inventory and is defined elsewhere.
]]--

local PANEL = {}

--[[---------------------------------------------------------
   Name: Init
-----------------------------------------------------------]]
function PANEL:Init()

	local pnl = vgui.Create( "InventoryPanel", self )
	pnl:SetTitle( "Player Inventory" )
	pnl:Dock( FILL )

end


vgui.Register( "InventoryMenu", PANEL, "Panel" )