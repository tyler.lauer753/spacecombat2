require("mysql")
include("shared.lua")

local GM = GM
local Admin = SC.Administration or {}
local PlayerMeta = FindMetaTable("Player")
local DB

local PlayerConnectedSQL = [[
    INSERT INTO sc2_users (steamid, name, first_connection_time, last_connection_time)
        VALUES (%s, "%s", %s, %s)
        ON DUPLICATE KEY UPDATE name="%s", last_connection_time=%s
]]

local GetPlayerPermissionsSQL = [[
    SELECT sc2_userpermissions.permissionid, sc2_permissions.name
        FROM (sc2_userpermissions
            INNER JOIN sc2_permissions
            ON sc2_userpermissions.permissionid=sc2_permissions.permissionid)
        WHERE sc2_userpermissions.steamid=%s;
]]

Admin.PermissionIDs = Admin.PermissionIDs or {}

function Admin.Ban(SteamID64, Length)
    local UnbanTime = os.time() + Length

    if Length == -1 then
        UnbanTime = -1
    end

    mysql.asyncquery(DB, "UPDATE sc2_users SET banned=1, unban_time="..UnbanTime.." WHERE steamid="..SteamID64)
end

function Admin.UnBan(SteamID64)
    mysql.asyncquery(DB, "UPDATE sc2_users SET banned=0, unban_time=0 WHERE steamid="..SteamID64)
end

function Admin.CheckForBan(SteamID64)
    local Data = mysql.query(DB, "SELECT banned, unban_time FROM sc2_users WHERE steamid="..SteamID64)

    if not Data or not Data[1] then
        return false, -1
    end

    return Data[1].banned == 1, Data[1].unban_time or -1
end

function Admin.GetPermissionID(Name)
    return Admin.PermissionIDs[Name]
end

function Admin.RemovePermissionFromDatabase(PermissionName, Transaction)
    local Query = "DELETE FROM `sc2_permissions` WHERE name=%s"
    Query = Format(Query, DB:escape(PermissionName))

    if Transaction then
        Transaction:addQuery(DB:query(Query))
    else
        mysql.asyncquery(DB, Query)
    end

    return true
end

function Admin.ExecuteCommand(Executor, Command, Arguments)
    -- Make sure the command actually exists
    if not Command or not Admin.Commands[Command] then
        if IsValid(Executor) then
            Executor:ChatPrint("[Space Combat Administration] - Invalid command `"..tostring(Command).."`!")
        end

        return false
    end

    local Args = Arguments
    if type(Arguments) == "string" then
        local Err
        Args, Err = Admin.ParseArguments(Arguments, Command)

        if not Args then
            if IsValid(Executor) then
                Executor:ChatPrint("[Space Combat Administration] - Invalid arguments!")
                Executor:ChatPrint(Err)
            end

            return false
        end
    end

    -- If there was an executor then check the permissions, otherwise assume it was called by the server
    if IsValid(Executor) and not Executor:HasPermissions(Admin.Commands[Command].Permissions) then
        Executor:ChatPrint("[Space Combat Administration] - You lack the permissions needed to use this command!")
        return false
    end

    -- Call the command
    Admin.Commands[Command].Callback(Executor, Args)

    return true
end

-- Network functions
util.AddNetworkString("SC.Admin.PermissionBroadcast")
function Admin.BroadcastPermissions(Player)
    if not IsValid(Player) then return end
    net.Start("SC.Admin.PermissionBroadcast")
    net.WriteString(Player:SteamID64())
    net.WriteTable(Admin.UserPermissions[Player:SteamID64()])
    net.Broadcast()
end

util.AddNetworkString("SC.Admin.RequestPermissions")
net.Receive("SC.Admin.RequestPermissions", function(Length, Player)
    net.Start("SC.Admin.RequestPermissions")
    net.WriteTable(Admin.UserPermissions)
    net.Send(Player)
end)

-- Player functions
function PlayerMeta:AddPermission(Permission, Transaction)
    if not IsValid(self) or not Admin.IsValidPermission(Permission) then return false end

    local ID64 = self:SteamID64()
    local Query = "INSERT IGNORE INTO `sc2_userpermissions` (steamid, permissionid) VALUES (%s, %d)"
    Query = Format(Query, ID64, Admin.GetPermissionID(Permission))

    Admin.UserPermissions[ID64][Permission] = true

    if Transaction then
        Transaction:addQuery(DB:query(Query))
    else
        mysql.asyncquery(DB, Query)
        Admin.BroadcastPermissions(self)
    end

    return true
end

function PlayerMeta:RemovePermission(Permission, Transaction)
    if not IsValid(self) or not Admin.IsValidPermission(Permission) then return false end

    local ID64 = self:SteamID64()
    local Query = "DELETE FROM `sc2_userpermissions` WHERE steamid=%s AND permissionid=%d"
    Query = Format(Query, ID64, Admin.GetPermissionID(Permission))

    Admin.UserPermissions[ID64][Permission] = nil

    if Transaction then
        Transaction:addQuery(DB:query(Query))
    else
        mysql.asyncquery(DB, Query)
        Admin.BroadcastPermissions(self)
    end

    return true
end

function PlayerMeta:AddPermissions(Permissions)
    local Transaction = DB:createTransaction()
    for i,k in pairs(Permissions) do
        self:AddPermission(k, Transaction)
    end
    Transaction:start()

    Admin.BroadcastPermissions(self)
end

function PlayerMeta:RemovePermissions(Permissions)
    local Transaction = DB:createTransaction()
    for i,k in pairs(Permissions) do
        self:RemovePermission(k, Transaction)
    end
    Transaction:start()

    Admin.BroadcastPermissions(self)
end

function PlayerMeta:ResetPermissions(Transaction)
    local Query = "DELETE FROM `sc2_userpermissions` WHERE steamid=%s"
    Query = Format(Query, self:SteamID64())

    if Transaction then
        Transaction:addQuery(DB:query(Query))
    else
        mysql.asyncquery(DB, Query)
    end

    return true
end

function PlayerMeta:Ban(Length, Kick)
    Admin.Ban(self:SteamID64(), Length)

    if Kick then
        self:Kick("You have been banned from the server.")
    end
end

-- Hooks
function Admin.OnMySQLConnected(SQL)
    DB = SQL

    mysql.query(SQL, [[
CREATE TABLE IF NOT EXISTS `sc2_permissions` (
  `permissionid` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(300) NULL,
  PRIMARY KEY (`permissionid`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
COMMENT = 'Used to store permissions for the Space Combat 2  administration system';

CREATE TABLE IF NOT EXISTS `sc2_users` (
  `steamid` BIGINT NOT NULL,
  `name` VARCHAR(45) NULL NOT NULL DEFAULT 'NoName',
  `first_connection_time` BIGINT NULL NOT NULL DEFAULT 0,
  `last_connection_time` BIGINT NULL NOT NULL DEFAULT 0,
  `time_connected` INT NULL NOT NULL DEFAULT 0,
  `banned` TINYINT NULL NOT NULL DEFAULT 0,
  `unban_time` BIGINT NULL NOT NULL DEFAULT 0,
  PRIMARY KEY (`steamid`))
COMMENT = 'Used to store information about users for the Space Combat 2 administration system';

CREATE TABLE IF NOT EXISTS `sc2_userpermissions` (
  `steamid` BIGINT NOT NULL,
  `permissionid` INT NOT NULL,
  UNIQUE INDEX `permissions_UNIQUE` (`steamid` ASC, `permissionid` ASC),
  INDEX `permissionid_idx` (`permissionid` ASC),
  CONSTRAINT `permissionid`
    FOREIGN KEY (`permissionid`)
    REFERENCES `sc2_permissions` (`permissionid`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `steamid`
    FOREIGN KEY (`steamid`)
    REFERENCES `sc2_users` (`steamid`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT = 'Used for the Space Combat 2 user permissions';
    ]])

    hook.Run("RegisterSC2Commands")

    do -- Update mysql information that might be outdated because we just connected
        local Transaction = SQL:createTransaction()

        -- Populate the permissions table in the database
        local Query = "INSERT INTO sc2_permissions (name, description) VALUES (\"%s\", \"%s\") ON DUPLICATE KEY UPDATE description=\"%s\";"
        for i, k in pairs(Admin.Permissions) do
            Transaction:addQuery(SQL:query(Format(Query, SQL:escape(i), SQL:escape(k), SQL:escape(k))))
        end

        -- Update player table
        for i,k in pairs(player.GetAll()) do
            -- Send info to database
            local Name = DB:escape(k:Nick())
            local SteamID = k:SteamID64()
            local Time = os.time()
            Transaction:addQuery(SQL:query(Format(PlayerConnectedSQL, SteamID, Name, Time, Time, Name, Time)))

            -- Get permissions from database
            local PermissionsQuery = SQL:query(Format(GetPlayerPermissionsSQL, SteamID))
            function PermissionsQuery:onSuccess(Data)
                for i,k in pairs(Data) do
                    Admin.UserPermissions[SteamID][k.name] = true
                end
            end
            Transaction:addQuery(PermissionsQuery)
        end
        Transaction:start()
    end

    do -- Update permission IDs
        local Query = "SELECT * FROM sc2_permissions"
        Query = DB:query(Query)
        function Query:onSuccess(Data)
            for _, Row in pairs(Data) do
                Admin.PermissionIDs[Row.name] = Row.permissionid
            end
        end

        Query:start()
    end
end
hook.Add("OnMySQLConnected", "SC.Admin.OnMySQLConnected", Admin.OnMySQLConnected)

hook.Add("PlayerInitialSpawn", "SC.Admin.InitialPlayerSpawn", function(Player)
    local SteamID = Player:SteamID64()
    local Time = os.time()

    Admin.UserPermissions[SteamID] = {}

    Player:SetNWInt("JoinTime", Time)
    Player:SetNWInt("SessionTime", 0)
    Player:SetNWInt("TotalTime", 0)

    for _, pl in pairs(player.GetAll()) do
		pl:ChatPrint({Color(0,255,0), Player:Nick(), " (", Player:SteamID(), ")", Color(255,255,255), " has joined the server!"})
	end

    if not DB then return end

    local Name = DB:escape(Player:Nick())
    if DB then
        mysql.asyncquery(DB, Format(PlayerConnectedSQL, SteamID, Name, Time, Time, Name, Time))

        local TimeQuery = DB:query("SELECT * FROM sc2_users WHERE steamid="..SteamID)
        function TimeQuery:onSuccess(Data)
            if IsValid(Player) and Data[1] and Data[1].time_connected then
                Player:SetNWInt("TotalTime", Data[1].time_connected)
            end
        end
        TimeQuery:start()

        local PermissionsQuery = DB:query(Format(GetPlayerPermissionsSQL, SteamID))
        function PermissionsQuery:onSuccess(Data)
            for i,k in pairs(Data) do
                Admin.UserPermissions[SteamID][k.name] = true
            end

            Admin.BroadcastPermissions(Player)
        end
        PermissionsQuery:start()
    end
end)

-- Keep track of how long users have been on the server
timer.Remove("SC.Admin.UpdatePlayerTime")
timer.Create("SC.Admin.UpdatePlayerTime", 60, 0, function()
    if not DB then return end
    local Transaction = DB:createTransaction()
    for i, k in pairs(player.GetAll()) do
        k:SetNWInt("SessionTime", os.time() - k:GetNWInt("JoinTime"))

        local TimeQuery = DB:query("UPDATE sc2_users SET time_connected="..(k:GetNWInt("TotalTime")+k:GetNWInt("SessionTime")).." WHERE steamid="..k:SteamID64())
        Transaction:addQuery(TimeQuery)
    end
    Transaction:start()
end)

-- Detect if players try to use commands
hook.Add("PlayerSay", "SC.Admin.PlayerSay", function(Player, Text)
    local LoweredText = Text:lower()
    local Command = LoweredText:sub(1, 1)

    if Command:find('/') or Command:find('!') then
        for i, k in pairs(Admin.Commands) do
            local LoweredCommand = i:lower()
            if LoweredCommand == LoweredText:sub(2, #LoweredCommand + 1) then
                print(Player, i, '"'..LoweredText:sub(#LoweredCommand + 3)..'"')
                Admin.ExecuteCommand(Player, i, LoweredText:sub(#LoweredCommand + 3))
                return ""
            end
        end
    end
end)

--
-- CheckPassword( steamid, networkid, server_password, password, name )
--
-- Called every time a non-localhost player joins the server. steamid is their 64bit
-- steamid. Return false and a reason to reject their join. Return true to allow
-- them to join.
--
function GM:CheckPassword( steamid, networkid, server_password, password, name )
	-- The server has sv_password set
	if server_password ~= "" then
		-- The joining clients password doesn't match sv_password
		if server_password ~= password then
			return false
		end
	end

    local Banned, UnbanTime = Admin.CheckForBan(steamid)
	if Banned then
        if UnbanTime ~= -1 and UnbanTime <= os.time() then
            Admin.UnBan(steamid)
            return true
        else
		    return false, "You are banned, please contact the server administrator for more information."
        end
	end

	-- Returning true means they're allowed to join the server
	return true
end

gameevent.Listen( "player_disconnect" )
hook.Add("player_disconnect", "player_disconnect_sc2", function( data )
	local name = data.name			-- Same as Player:Nick()
	local steamid = data.networkid	-- Same as Player:SteamID()
	local id = data.userid			-- Same as Player:UserID()
	local bot = data.bot			-- Same as Player:IsBot()
	local reason = data.reason		-- Text reason for disconnected such as "Kicked by console!", "Timed out!", etc...

    for _, pl in pairs(player.GetAll()) do
		pl:ChatPrint({Color(0,255,0), name, " (", steamid, ")", Color(255,255,255), " Disconnected. (Reason:", reason, ")"})
	end
end)

gameevent.Listen( "player_connect" )
hook.Add( "player_connect", "player_connect_sc2", function(data)
	local name = data.name			-- Same as Player:Nick()
	local steamid = data.networkid	-- Same as Player:SteamID()
	local ip = data.address			-- Same as Player:IPAddress()
	local id = data.userid			-- Same as Player:UserID()
	local bot = data.bot			-- Same as Player:IsBot()
	local index = data.index		-- Same as Player:EntIndex()

    for _, pl in pairs(player.GetAll()) do
		pl:ChatPrint({Color(0,255,0), name, " (", steamid, ")", Color(255,255,255), " is joining the server!"})
	end
end)

SC.Administration = SC.Administration or Admin