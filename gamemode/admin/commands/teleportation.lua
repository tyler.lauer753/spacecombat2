AddCSLuaFile()

local Admin = SC.Administration

hook.Add("RegisterSC2Commands", "RegisterTeleportationCommands", function()
    -- Register permissions
    Admin.RegisterPermission("TeleportSelf", "The ability for a user to teleport themselves.")
    Admin.RegisterPermission("TeleportOthers", "The ability for a user to teleport other users.")

    -- Register commands
    Admin.RegisterCommand("Teleport", "Teleports the user where they are looking.",
        -- Permissions
        {
            "TeleportSelf"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "optional_player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            local ToTeleport
            if IsValid(Arguments.Target) and Arguments.Target ~= Executor and Executor:HasPermission("TeleportOthers") then
                ToTeleport = Arguments.Target
            else
                ToTeleport = Executor
            end

            if IsValid(ToTeleport) then
                local Trace = {}
				Trace.start = Executor:GetShootPos()
				Trace.endpos = Executor:GetShootPos() + Executor:GetAimVector() * 99999999999
                Trace.filter = Executor

				local TraceResult = util.TraceLine(Trace)
				ToTeleport:SetPos(TraceResult.HitPos + TraceResult.HitNormal * Vector(35, 35, 1))
				ToTeleport:SetLocalVelocity(Vector(0, 0, 0))
            end
        end)

    Admin.RegisterCommand("Goto", "Teleports the user where they are looking.",
        -- Permissions
        {
            "TeleportSelf"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            if Executor:GetMoveType() == MOVETYPE_NOCLIP then
                Executor:SetPos(Arguments.Target:GetPos() + Arguments.Target:GetForward() * 50)
                return
            end

            if Executor:InVehicle() then
                Executor:ExitVehicle()
            end

            if not Executor:Alive() then
                Executor:ChatPrint("You must be alive to goto!")
                return
            end

            local TargetLocations = {}
            for i = 1, 360 do
                table.insert(TargetLocations, Arguments.Target:GetPos() + Vector(math.sin(i) * 50, math.cos(i) * 50, 37))
            end

            table.insert(TargetLocations, Arguments.Target:GetPos() + Vector(0, 0, 112))

            for k,v in pairs(TargetLocations) do
                local Trace = {}
                Trace.start = v
                Trace.endpos = v
                Trace.mins = Vector(-25, -25, -37)
                Trace.maxs = Vector(25, 25, 37)

                local TraceResult = util.TraceHull(Trace)
                if not TraceResult.Hit then
                    Executor:SetPos(v - Vector(0, 0, 37))
                    Executor:SetLocalVelocity(Vector(0, 0, 0))
                    Executor:SetEyeAngles((Arguments.Target:GetShootPos() - Executor:GetShootPos()):Angle())
                    return
                end
            end
        end)

    Admin.RegisterCommand("Bring", "Teleports the user where they are looking.",
        -- Permissions
        {
            "TeleportOthers"
        },

        -- Arguments
        {
            {
                Name = "Target",
                Type = "player"
            }
        },

        -- Callback
        function(Executor, Arguments)
            if Arguments.Target:GetMoveType() == MOVETYPE_NOCLIP then
                Arguments.Target:SetPos(Executor:GetPos() + Executor:GetForward() * 50)
                return
            end

            if Arguments.Target:InVehicle() then
                Arguments.Target:ExitVehicle()
            end

            if not Arguments.Target:Alive() then
                Executor:ChatPrint("Target must be alive to bring them!")
                return
            end

            local TargetLocations = {}
            for i = 1, 360 do
                table.insert(TargetLocations, Executor:GetPos() + Vector(math.sin(i) * 50, math.cos(i) * 50, 37))
            end

            table.insert(TargetLocations, Executor:GetPos() + Vector(0, 0, 112))

            for k,v in pairs(TargetLocations) do
                local Trace = {}
                Trace.start = v
                Trace.endpos = v
                Trace.mins = Vector(-25, -25, -37)
                Trace.maxs = Vector(25, 25, 37)

                local TraceResult = util.TraceHull(Trace)
                if not TraceResult.Hit then
                    Arguments.Target:SetPos(v - Vector(0, 0, 37))
                    Arguments.Target:SetLocalVelocity(Vector(0, 0, 0))
                    Arguments.Target:SetEyeAngles((Executor:GetShootPos() - Arguments.Target:GetShootPos()):Angle())
                    return
                end
            end
        end)
end)