local GM = GM

-- These files get sent to the client
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("diaspora.lua")
AddCSLuaFile("cl_help.lua")
AddCSLuaFile("cl_chat.lua")
AddCSLuaFile("cl_quiz.lua")
AddCSLuaFile("cl_hints.lua")
AddCSLuaFile("cl_init.lua")
AddCSLuaFile("cl_search_models.lua")
AddCSLuaFile("cl_spawnmenu.lua")
AddCSLuaFile("cl_worldtips.lua")
AddCSLuaFile("player_extension.lua")
AddCSLuaFile("gui/IconEditor.lua")
AddCSLuaFile("gui/inventoryinfopanel.lua")
AddCSLuaFile("gui/inventorypanel.lua")
AddCSLuaFile("gui/inventoryitem.lua")
AddCSLuaFile("maprotate/client/cl_maptimer.lua")
AddCSLuaFile("classes/send.lua")
AddCSLuaFile("sb/cl_main.lua")
AddCSLuaFile("spacecombat2/cl_convar.lua")

include("classes/send.lua")
include('shared.lua')
include('commands.lua')
include('player.lua')
include("quiz.lua")
include("announcements.lua")
include('spawnmenu/init.lua')
include("sb/sv_main.lua")
include("spacecombat2/sv_main.lua")
include("admin/init.lua")
include("maprotate/server/maptimer.lua")

--networking strings
util.AddNetworkString("AddToChatBox")
util.AddNetworkString("PlayerSay")
util.AddNetworkString("SetOverlayText")
util.AddNetworkString("SetOverlayData")
util.AddNetworkString("Ship_Shield_UMSG")
util.AddNetworkString("Ship_Core_UMSG")
util.AddNetworkString("Ship_Core_Update")
util.AddNetworkString("Ship_Core_Select")
util.AddNetworkString("Ship_Core_OpenGlobalStorage")
util.AddNetworkString("Ship_Core_Mods")
util.AddNetworkString("sc_refinery_ui")
util.AddNetworkString("sc_createeffect")
util.AddNetworkString("sc_module_reactor_ui")
util.AddNetworkString("SC.ClientMissingString")

--
-- Make BaseClass available
--
DEFINE_BASECLASS("gamemode_base")


--[[---------------------------------------------------------
   Name: gamemode:OnPhysgunFreeze(weapon, phys, ent, player)
   Desc: The physgun wants to freeze a prop
-----------------------------------------------------------]]
function GM:OnPhysgunFreeze(weapon, phys, ent, ply)

	-- Don't freeze persistent props (should already be froze)
	if (ent:GetPersistent()) then return false end

	BaseClass.OnPhysgunFreeze(self, weapon, phys, ent, ply)

	ply:SendHint("PhysgunUnfreeze", 0.3)
	ply:SuppressHint("PhysgunFreeze")

end


--[[---------------------------------------------------------
   Name: gamemode:OnPhysgunReload(weapon, player)
   Desc: The physgun wants to unfreeze
-----------------------------------------------------------]]
function GM:OnPhysgunReload(weapon, ply)

	local num = ply:PhysgunUnfreeze()

	if (num > 0) then
		ply:SendLua("GAMEMODE:UnfrozeObjects("..num..")")
	end

	ply:SuppressHint("PhysgunReload")

end


--[[---------------------------------------------------------
   Name: gamemode:PlayerShouldTakeDamage
   Return true if this player should take damage from this attacker
   Note: This is a shared function - the client will think they can
	 damage the players even though they can't. This just means the
	 prediction will show blood.
-----------------------------------------------------------]]
function GM:PlayerShouldTakeDamage(ply, attacker)

	-- The player should always take damage in single player..
	if (game.SinglePlayer()) then return true end

	-- Global godmode, players can't be damaged in any way
	if (cvars.Bool("sbox_godmode", false)) then return false end

	-- No player vs player damage
	if (attacker:IsValid() and attacker:IsPlayer()) then
		return cvars.Bool("sbox_playershurtplayers", true)
	end

	-- Default, let the player be hurt
	return true

end


--[[---------------------------------------------------------
   Show the search when f1 is pressed
-----------------------------------------------------------]]
function GM:ShowHelp(ply)

	ply:SendLua("hook.Run('StartSearch')")

end


--[[---------------------------------------------------------
   Desc: A ragdoll of an entity has been created
-----------------------------------------------------------]]
function GM:CreateEntityRagdoll(entity, ragdoll)

	-- Replace the entity with the ragdoll in cleanups etc
	undo.ReplaceEntity(entity, ragdoll)
	cleanup.ReplaceEntity(entity, ragdoll)

end


--[[---------------------------------------------------------
   Name: gamemode:PlayerUnfrozeObject()
-----------------------------------------------------------]]
function GM:PlayerUnfrozeObject(ply, entity, physobject)

	local effectdata = EffectData()
		effectdata:SetOrigin(physobject:GetPos())
		effectdata:SetEntity(entity)
	util.Effect("phys_unfreeze", effectdata, true, true)

end


--[[---------------------------------------------------------
   Name: gamemode:PlayerFrozeObject()
-----------------------------------------------------------]]
function GM:PlayerFrozeObject(ply, entity, physobject)

	if (DisablePropCreateEffect) then return end

	local effectdata = EffectData()
		effectdata:SetOrigin(physobject:GetPos())
		effectdata:SetEntity(entity)
	util.Effect("phys_freeze", effectdata, true, true)

end


--
-- Who can edit variables?
-- If you're writing prop protection or something, you'll
-- probably want to hook or override this function.
--
function GM:CanEditVariable(ent, ply, key, val, editor)

	-- Only allow admins to edit admin only variables!
	if (editor.AdminOnly) then
		return ply:IsAdmin()
	end

	-- This entity decides who can edit its variables
	if (isfunction(ent.CanEditVariables)) then
		return ent:CanEditVariables(ply)
	end

	-- default in sandbox is.. anyone can edit anything.
	return true

end

--GM.MakeEnt is basically a copy of WireLib.MakeWireEnt with our ownership stuff and without limit checks
function GM.MakeEnt(pl, Data, ...)
	Data.Class = scripted_ents.Get(Data.Class).ClassName

	local ent = ents.Create(Data.Class)
	if not IsValid(ent) then return false end

	duplicator.DoGeneric(ent, Data)
	ent:Spawn()
	ent:Activate()
	duplicator.DoGenericPhysics(ent, pl, Data) -- Is deprecated, but is the only way to access duplicator.EntityPhysics.Load (its local)

	ent.Owner = pl
	if ent.Setup then ent:Setup(...) end

	local phys = ent:GetPhysicsObject()
	if IsValid(phys) then
		if Data.frozen then phys:EnableMotion(false) end
		if Data.nocollide then phys:EnableCollisions(false) end
	end

	return ent
end

local function DoGrammar(Say)
		local FilterWords = {
				i = "I",
				u = "you",
				cuz = "because",
				im = "I'm",
				ur = "your",
				omg = "oh my god",
				omfg = "oh my fucking god",
				wtf = "what the fuck",
				tbh = "to be honest",
				wont = "won't",
				dont = "don't",
				cant = "can't",
				theyre = "they're",
				hax = "hacks",
				teh = "the",
				bbl = "be back later",
				brb = "be right back",
				bsod = "blue screen of death",
				hai = "yes",	--Get out, you weeb bastard. -Steeveeo, king of weebs --NEVER -Lt.Brandon
				bbiab = "be back in a bit",
				afaicr = "as far as I can recall",
				asap = "as soon as possible",
				atm = "at the moment",
				--leet = "leet", --dafaq is this sheet - Steeveeo
				cya = "see ya",
				ffs = "for fucks sake",
				gtg = "got to go",
				g2g = "got to go",
				haxor = "hacker",
				ty = "thank you",
				np = "no problem",
				wtg = "way to go",
				wth = "what the hell",
				zomg = "omg", --"oh my god",
				nub = "I am a noob", --"noob", --Lul
				wat = "what",

				--Steeveeo's Additions
				iirc = "if I recall correctly",
				btw = "by the way",

				-- Ninjr's lulzy additions.
				soon = "soon&trade;",

				-- Kanzuke is helping.
				eventually = "eventually&trade;"
			}

        Say = " "..Say.." "

		for K, V in pairs(FilterWords) do
			Say = string.Replace(Say, " "..K.." ", " "..V.." ")
		end

		Say = string.Trim(Say)

		local UpperText = string.upper(string.sub(Say, 0, 1))

		Say = UpperText..""..string.sub(Say, 2, string.len(Say))

		if not string.find(string.sub(Say, string.len(Say)), "%p") then
			Say = Say.."."
		end

		return Say
end

--playerspawn stuff
function GM:PlayerInitialSpawn(ply)
	self.BaseClass.PlayerInitialSpawn(self, ply)

	--This is here because players spawn before they're completely valid. You can thank Source logic for this one...
    timer.Simple(0.5, function() if IsValid(ply) then ply:KillSilent() end end)
end

--[[---------------------------------------------------------
   Called on the player's every spawn
-----------------------------------------------------------]]
function GM:PlayerSpawn(ply)
    player_manager.SetPlayerClass(ply, "player_sandbox")

	local spawners = ents.FindByClass("sc_player_start")
	for k,v in pairs(spawners) do
		if ply:getRace() == v.Race then
			ply:SetPos(v:GetPos())
			ply:SetAngles(v:GetAngles())
		end
	end

    self.BaseClass.PlayerSpawn(self, ply)
end

--rpchat
local meta = FindMetaTable("Player")
local FuncPrintMessage = meta.PrintMessage

function meta:ChatPrint(msg,tab)
	self:PrintMessage(HUD_PRINTTALK,msg,tab)
end

function meta:PrintMessage(typ, msg, tab)
	if type(msg) ~= "table" and typ ~= HUD_PRINTNOTIFY and typ ~= HUD_PRINTTALK then
		FuncPrintMessage(self, typ, msg)
		return
	end

	if not tab then tab = "All" end
	if type(msg) ~= "table" then msg = {msg} end

	net.Start("AddToChatBox")
		net.WriteString(tab)
		net.WriteTable(msg)
	net.Send(self)
end

function DoSay(ply, text, isTeam)
	if IsValid(ply) then
		local Say = DoGrammar(text)
		hook.Run("PostPlayerSay", ply, Say, isTeam)

		if isTeam then
            local Faction = ply:Team()
            local FactionName = team.GetName(Faction)
			for i,k in pairs(player.GetAll()) do
				if k:Team() == Faction then
					k:ChatPrint({ply, " ["..FactionName.."]: "..Say}, "Faction")
				end
			end
		else
			ErrorNoHalt(ply:Name()..": "..Say.."\n")

			for i,k in pairs(player.GetAll()) do k:ChatPrint({ply, ": "..Say}, "All") end
		end
	end
end

net.Receive("PlayerSay", function(len, ply)
	local str = net.ReadString()
	local tab = net.ReadString()
	local isTeam = (net.ReadBit() == 1) or (tab == "Faction")
	local say = hook.Run("PlayerSay", ply, str, isTeam, tab)

	if string.len(say) > 0 then
		DoSay(ply, say, isTeam)
	end
end)

local HoverHeads = {}
concommand.Add("start_chatting",function(ply, cmd, args)
    if not HoverHeads[ply] then
        local ent = ents.Create("prop_physics")
        HoverHeads[ply] = ent
        ent:SetModel("models/extras/info_speech.mdl")
        ent:SetMoveType(MOVETYPE_NONE)
        ent:SetNotSolid(1)
        ent:AddEffects( EF_ITEM_BLINK )
        ent:AddEffects( EF_NOSHADOW )
        ent:SetParent(ply)
        ent:SetPos(ply:GetPos() + Vector(0,0,100))
        ent:SetAngles(Angle(0,0,0))
	end
end
)

concommand.Add("stop_chatting", function(ply, cmd, args)
    if HoverHeads[ply] and HoverHeads[ply]:IsValid() then
        HoverHeads[ply]:Remove()
        HoverHeads[ply] = nil
    end
end
)

hook.Add("Tick","SpinMeRightRound", function()
    for k,v in pairs(HoverHeads) do
        if (v:IsValid()) then
            v:SetAngles(v:GetAngles() + Angle(0,3,0))
        end
    end
end)