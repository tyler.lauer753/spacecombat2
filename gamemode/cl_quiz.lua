Quiz = {}

local QuestionPanel
local Questions = {}
local Choices = {}
local TryCreateQuestionPanel = true
local KickPlayerOnFailure = true
local KickWaitTime = 10

net.Receive("SC.GetQuizQuestions", function()
    Questions = net.ReadTable()
	KickPlayerOnFailure = net.ReadBool()
    KickWaitTime = net.ReadInt(8)
    local PlayerNeedsQuiz = net.ReadBool()

	if TryCreateQuestionPanel and PlayerNeedsQuiz then
		TryCreateQuestionPanel = false
		Quiz.CreateQuestionPanel()
	end
end)

net.Receive("SC.CheckQuiz", function()
    local Failed = net.ReadBool()

	if Failed then
		Quiz.Incorrect()
	else
		Quiz.EndQuiz()
	end
end)

function Quiz.UpdateQuestions()
	net.Start("SC.GetQuizQuestions")
	net.SendToServer()
end
hook.Add("SC.OnPlayerCreationCompleted", "SC.UpdateQuizQuestions", Quiz.UpdateQuestions)

function Quiz.CheckQuiz()
	net.Start("SC.CheckQuiz")
	net.WriteTable(Choices)
	net.SendToServer()
end

function Quiz.EndQuiz()
	QuestionPanel:Remove()
	gui.EnableScreenClicker(false)
end

function Quiz.Incorrect()
	local warn = vgui.Create("DFrame")
	warn:SetTitle("Cannot Continue")
	warn:SetPos(ScrW() / 2 - 100, ScrH() / 2 - 45)
	warn:SetSize(200, 90)

	local Label = vgui.Create("DLabel", warn)

	if KickPlayerOnFailure then
		Label:SetText("Incorrect answer(s).\n\nYou will be disconnected in "..tostring(KickWaitTime).." seconds.")
	else
		Label:SetText("Incorrect answer(s).\n\nPlease try again!")
	end

	Label:SetFont("DefaultSmall")
	Label:SizeToContents()
	Label:SetPos(15, 35)

	warn:SetVisible(true)
	warn:MakePopup()
end

function Quiz.CreateQuestionPanel()
	if not (Questions and table.Count(Questions) > 0) then
		Quiz.UpdateQuestions()
		TryCreateQuestionPanel = true

		return
	end

	if IsValid(LocalPlayer()) then
		gui.EnableScreenClicker(true)

		QuestionPanel = vgui.Create("DFrame")
		QuestionPanel:SetTitle("Questions")
		QuestionPanel:SetSize(600, table.Count(Questions) * 35)
		QuestionPanel:SetDraggable(false)
		QuestionPanel:SetSizable(false)
		QuestionPanel:SetBackgroundBlur(true)
		QuestionPanel:ShowCloseButton(false)
		QuestionPanel:Center()
		QuestionPanel:MakePopup()

		local function AddButton(Text, X, Y, Width, Height)
			local Button = vgui.Create("DButton", QuestionPanel)
			Button:SetPos(X, Y)
			Button:SetSize(Width, Height)
			Button:SetText(Text)
			return Button
		end

		local h = 10
		surface.SetFont("DefaultSmall")

		for Question, Answers in pairs(Questions) do
			local xw, xh = surface.GetTextSize(FormatLine(Question, "DefaultSmall", QuestionPanel:GetWide() * .55))
			h = h + xh + 10

			local Label = vgui.Create("DLabel", QuestionPanel)
			Label:SetText(FormatLine(Question, "DefaultSmall", QuestionPanel:GetWide() * .55))
			Label:SetFont("DefaultSmall")
			Label:SizeToContents()
			Label:SetPos(10, h)

			local but1 = AddButton(Answers[1], QuestionPanel:GetWide() * .62, h, surface.GetTextSize(Answers[1]) + 20, 16)
			local but2 = AddButton(Answers[2], QuestionPanel:GetWide() * .62 + surface.GetTextSize(Answers[1]) + 40, h, surface.GetTextSize(Answers[2]) + 20, 16)

			function OnClicked(self)
				self:SetColor(Color(30, 180, 30, 255))
				self.OtherButton:SetColor(Color(90, 90, 90, 255))

				Choices[self.QID] = self.opt
			end

			but1.DoClick = OnClicked
			but2.DoClick = OnClicked

			but1.QID = Question
			but1.opt = Answers[1]
			but1.OtherButton = but2

			but2.QID = Question
			but2.opt = Answers[2]
			but2.OtherButton = but1
		end

		local cont = AddButton("Continue", QuestionPanel:GetWide() * .62, h + 25, 200, 16)
		cont.DoClick = Quiz.CheckQuiz
	end
end
