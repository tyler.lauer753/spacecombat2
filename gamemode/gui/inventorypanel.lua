--[[
	Space Combat Inventory Control Panel
	Author: Steve "Steeveeo" Green
	
	For use in any player-accessible inventory, like
	own pockets and global storage.
]]--

local PANEL = {}

PANEL.ItemList = {}
PANEL.CurSelectedItem = nil

PANEL.TitleText = "Inventory"
PANEL.TitleLabel = nil

PANEL.Capacity = 400.0
PANEL.CapacityUsed = 0.0
PANEL.Mass = 0.0
PANEL.InfoLabel = nil

PANEL.ResourcePanel = nil

PANEL.DropItemButton = nil
PANEL.DestroyItemButton = nil

--[[---------------------------------------------------------
   Initialize Inventory Panel
-----------------------------------------------------------]]
function PANEL:Init()
	
	--Title Text
	self.TitleLabel = vgui.Create( "DLabel", self )
	self.TitleLabel:SetFont( "DermaLarge" )
	self.TitleLabel:SetText( self.TitleText )
	self.TitleLabel:SizeToContents()
	self.TitleLabel:Dock( TOP )
	
	--Inventory Information
	self.InfoLabel = vgui.Create( "DLabel", self )
	
	local info = "Capacity: " .. self.CapacityUsed .. "L / " .. self.Capacity .. "L\n"
	info = info .. "Mass: " .. self.Mass .. " kg"
	
	self.InfoLabel:SetText(info)
	self.InfoLabel:SizeToContents()
	self.InfoLabel:Dock( TOP )
	self.InfoLabel:DockMargin( 0, 15, 0, 0 )
	
	--Resource List Scrollpane
	self.ResourcePanel = vgui.Create( "DScrollPanel", self )
	self.ResourcePanel:Dock( FILL )
	self.ResourcePanel:DockMargin( 0, 5, 0, 2 )
	self.ResourcePanel:SetBackgroundColor( Color(255, 255, 255, 255) )
	self.ResourcePanel:SetPaintBackground( true )
	
	--Populate with test data
	--[[
	for i = 1, 30 do
		local res = vgui.Create( "InventoryItem", self.ResourcePanel )
		res:Dock( TOP )
		res:DockMargin( 2, 1, 2, 1 )
		
		res.InventoryPanel = self
		
		table.insert(self.ItemList, res)
	end]]--

	----------------------------------------
	--BEGIN COPYPASTA TEST DATA GENERATION
	----------------------------------------
	
	for k, v in pairs(GAMEMODE:GetResourcesOfType("Cargo")) do
		local res = vgui.Create( "InventoryItem", self.ResourcePanel )
		
		local fakeResourceInfo = {}
		fakeResourceInfo.Name = v:GetName()
		fakeResourceInfo.Amount = math.Rand(1, 1000000)
		
		res:Dock( TOP )
		res:DockMargin( 2, 1, 2, 1 )
		res:LoadFromResource( fakeResourceInfo )
		res.InventoryPanel = self
		table.insert(self.ItemList, res)
	end
	
	local res = vgui.Create( "InventoryItem", self.ResourcePanel )
	local fakeResourceInfo = {}
	fakeResourceInfo.Name = "Oxygen"
	fakeResourceInfo.Amount = 1000.54
	res:Dock( TOP )
	res:DockMargin( 2, 1, 2, 1 )
	res:LoadFromResource( fakeResourceInfo )
	res.InventoryPanel = self
	table.insert(self.ItemList, res)
	
	res = vgui.Create( "InventoryItem", self.ResourcePanel )
	fakeResourceInfo = {}
	fakeResourceInfo.Name = "Water" --"Polonium Nitrate"
	fakeResourceInfo.Amount = 5.0000006
	res:Dock( TOP )
	res:DockMargin( 2, 1, 2, 1 )
	res:LoadFromResource( fakeResourceInfo )
	res.InventoryPanel = self
	table.insert(self.ItemList, res)
	
	res = vgui.Create( "InventoryItem", self.ResourcePanel )
	fakeResourceInfo = {}
	fakeResourceInfo.Name = "Lifesupport Canister"
	fakeResourceInfo.Amount = 900
	res:Dock( TOP )
	res:DockMargin( 2, 1, 2, 1 )
	res:LoadFromResource( fakeResourceInfo )
	res.InventoryPanel = self
	table.insert(self.ItemList, res)
	
	res = vgui.Create( "InventoryItem", self.ResourcePanel )
	fakeResourceInfo = {}
	fakeResourceInfo.Name = "Uranium"
	fakeResourceInfo.Amount = 0.5
	res:Dock( TOP )
	res:DockMargin( 2, 1, 2, 1 )
	res:LoadFromResource( fakeResourceInfo )
	res.InventoryPanel = self
	table.insert(self.ItemList, res)
	
	res = vgui.Create( "InventoryItem", self.ResourcePanel )
	fakeResourceInfo = {}
	fakeResourceInfo.Name = "Empty Canister" --"Nubium Generator Core"
	fakeResourceInfo.Amount = 5
	res:Dock( TOP )
	res:DockMargin( 2, 1, 2, 1 )
	res:LoadFromResource( fakeResourceInfo )
	res.InventoryPanel = self
	table.insert(self.ItemList, res)
	
	---END COPYPASTA TEST DATA GENERATION---
	
	
	self.ResourcePanel:InvalidateLayout()
	
	--Salvage/Destroy Button
	self.DestroyItemButton = vgui.Create( "DButton", self )
	self.DestroyItemButton:SetText("Salvage/Destroy Selected Item")
	self.DestroyItemButton:Dock( BOTTOM )
	
	--Drop Button
	self.DropItemButton = vgui.Create( "DButton", self )
	self.DropItemButton:SetText("Drop Selected Item")
	self.DropItemButton:Dock( BOTTOM )
end


--[[---------------------------------------------------------
   Change Settings
-----------------------------------------------------------]]
function PANEL:SetTitle( title )
	self.TitleText = title
	self.TitleLabel:SetText( self.TitleText )
	self.TitleLabel:SizeToContents()
end

function PANEL:DisableDropButton( toggle )
	self.DropItemButton:SetDisabled( toggle )
end

function PANEL:DisableDestroyButton( toggle )
	self.DestroyItemButton:SetDisabled( toggle )
end


--[[---------------------------------------------------------
   InventoryItem Selection
-----------------------------------------------------------]]
function PANEL:SetSelected( item )
	--Deselect if another item is already selected
	if self.CurSelectedItem and self.CurSelectedItem ~= item then
		self.CurSelectedItem:Deselect()
	end
	
	--Select this one (since we do the selection event on InventoryItem, we only need to update the tracker)
	self.CurSelectedItem = item
end



vgui.Register( "InventoryPanel", PANEL, "Panel" )