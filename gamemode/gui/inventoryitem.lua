--[[
	Space Combat Inventory Item Object
	Author: Steve "Steeveeo" Green

	Fills up the scrollpanel on InventoryPanel, represents
	some sort of contained resource/item.
]]--

local PANEL = {}

--Panel Settings/Materials
local GradientUpMat = Material( "gui/gradient_up" )
local GradientDownMat = Material( "gui/gradient_down" )

local PanelHeight = 85
local SubPanelHeight = 20

PANEL.IsSelected = false


--Element Pointers
PANEL.InventoryPanel = nil --Pointer back to the InventoryPanel that spawned this
PANEL.RightClickMenu = nil

PANEL.ItemIcon = nil
PANEL.ItemLabel = nil
PANEL.ItemAmountLabel = nil

PANEL.ItemVolumeLabel = nil
PANEL.ItemMassLabel = nil

PANEL.ItemInfoPage = nil
PANEL.ItemInfoPopout = nil


--Default Element Values
PANEL.IconPath = "models/props_interiors/pot01a.mdl"
PANEL.ItemName = "[PLACEHOLDER ITEM NAME]"
PANEL.ItemAmount = 1337 --Hurr Durr
PANEL.ItemMassPerUnit = 1
PANEL.ItemVolumePerUnit = 1


--[[---------------------------------------------------------
   Initialize
-----------------------------------------------------------]]
function PANEL:Init()
	self:SetMouseInputEnabled( true )

	--Fill horizontal, clip vertical
	self:SetSize( 0, PanelHeight )
	
	--Top Panel Section (doing this in two DPanels so we can use Docks without issue)
	local panelTop = vgui.Create( "DPanel", self )
	panelTop:SetSize( 0, PanelHeight - SubPanelHeight )
	panelTop:Dock( TOP )
	panelTop:SetPaintBackground( false )
	
	--Bottom Panel Section
	local panelBottom = vgui.Create( "DPanel", self )
	panelBottom:SetSize( 0, SubPanelHeight )
	panelBottom:Dock( BOTTOM )
	panelBottom:SetPaintBackground( false )
	
	--Item Icon Backing
	local backPanel = vgui.Create( "DPanel", panelTop )
	local size = PanelHeight - SubPanelHeight - 2
	backPanel:SetSize( size, size )
	backPanel:Dock( LEFT )
	backPanel:DockMargin( 1, 1, 1, 1 )
	backPanel:SetPaintBackground( true )
	backPanel:SetBackgroundColor( Color(0, 0, 0, 255) )
	
	--Item Icon (SpawnIcon for now, should probably be switched when I can figure out how to make a Material panel read from spawnicons/models/)
	--	Can also be switched for Material if someone feels like coming up with icons for each resource.
	self.ItemIcon = vgui.Create( "SpawnIcon", backPanel )
	self.ItemIcon:SetModel( self.IconPath )
	self.ItemIcon:Dock( FILL )
	self.ItemIcon:DockMargin( 5, 5, 5, 5 )
	
	--Resource Title
	self.ItemLabel = vgui.Create( "DLabel", panelTop )
	self.ItemLabel:SetFont( "DermaLarge" )
	self.ItemLabel:SetText( self.ItemName )
	self.ItemLabel:SizeToContents()
	self.ItemLabel:Dock( LEFT )
	self.ItemLabel:DockMargin( 5, 0, 0, 0 )
	
	--Resource Amount Label
	self.ItemAmountLabel = vgui.Create( "DLabel", panelTop )
	self.ItemAmountLabel:SetFont( "DermaLarge" )
	self.ItemAmountLabel:SetText( self.ItemAmount )
	self.ItemAmountLabel:SizeToContents()
	self.ItemAmountLabel:Dock( RIGHT )
	self.ItemAmountLabel:DockMargin( 0, 0, 5, 0 )
	
	--Resource Volume Label
	self.ItemVolumeLabel = vgui.Create( "DLabel", panelBottom )
	self.ItemVolumeLabel:SetText( "Volume: 1000000000.00L" )
	self.ItemVolumeLabel:SizeToContents()
	self.ItemVolumeLabel:Dock( LEFT )
	self.ItemVolumeLabel:DockMargin( 2, 0, 0, 0 )
	
	--Resource Mass Label
	self.ItemMassLabel = vgui.Create( "DLabel", panelBottom )
	self.ItemMassLabel:SetText( "Mass: 1000000000.00kg" )
	self.ItemMassLabel:SizeToContents()
	self.ItemMassLabel:Dock( LEFT )
	self.ItemMassLabel:DockMargin( 10, 0, 0, 0 )
	
	--The below, oddly enough, lets us turn mouse input on for the entire panel, so yay for that
	panelTop:SetMouseInputEnabled( false )
	panelBottom:SetMouseInputEnabled( false )
	backPanel:SetMouseInputEnabled( false )
	self.ItemIcon:SetMouseInputEnabled( false )
	self.ItemLabel:SetMouseInputEnabled( false )
	self.ItemAmountLabel:SetMouseInputEnabled( false )
	self.ItemVolumeLabel:SetMouseInputEnabled( false )
	self.ItemMassLabel:SetMouseInputEnabled( false )
end


--[[---------------------------------------------------------
   Accessors/Settings
-----------------------------------------------------------]]

function PANEL:SetItemIcon( modelPath )
	self.IconPath = modelPath
	self.ItemIcon:SetModel( self.IconPath )
end

function PANEL:GetItemIcon()
	return self.IconPath
end


function PANEL:SetItemName( name )
	self.ItemName = name

	self.ItemLabel:SetText( self.ItemName )
	self.ItemLabel:SizeToContents()
end

function PANEL:GetItemName()
	return self.ItemName
end


function PANEL:SetAmount( amount )
	self.ItemAmount = amount
	
	self.ItemAmountLabel:SetText( string.format("x%.2f", self.ItemAmount) )
	self.ItemAmountLabel:SizeToContents()
	
	self.ItemVolumeLabel:SetText( string.format("Volume: %.2fL", self.ItemAmount * self.ItemVolumePerUnit) )
	self.ItemVolumeLabel:SizeToContents()
	
	self.ItemMassLabel:SetText( string.format("Mass: %.2fkg", self.ItemAmount * self.ItemMassPerUnit) )
	self.ItemMassLabel:SizeToContents()
end

function PANEL:GetAmount()
	return self.ItemAmount
end


function PANEL:SetVolumePerUnit( volume )
	self.ItemVolumePerUnit = volume
	
	self.ItemVolumeLabel:SetText( string.format("Volume: %.2fL", self.ItemAmount * self.ItemVolumePerUnit) )
	self.ItemVolumeLabel:SizeToContents()
end

function PANEL:GetItemVolumePerUnit()
	return self.ItemVolumePerUnit
end


function PANEL:SetMassPerUnit( mass )
	self.ItemMassPerUnit = mass
	
	self.ItemMassLabel:SetText( string.format("Mass: %.2fkg", self.ItemAmount * self.ItemMassPerUnit) )
	self.ItemMassLabel:SizeToContents()
end

function PANEL:GetMassPerUnit()
	return self.ItemMassPerUnit
end



function PANEL:LoadFromResource( resource )
	local resourceInfo = GAMEMODE:GetResourceInfoTable( resource.Name )
	
	self:SetItemName( resource.Name )
	self:SetAmount( resource.Amount )
	
	self:SetVolumePerUnit( resourceInfo.Volume )
	self:SetMassPerUnit( resourceInfo.Mass )
	self:SetItemIcon( resourceInfo.Icon )
end

--[[---------------------------------------------------------
   Input Controls
-----------------------------------------------------------]]

function PANEL:OnMousePressed( keyCode )
	if keyCode == MOUSE_LEFT then
		self:Select( true )
	elseif keyCode == MOUSE_RIGHT then
		self:OpenMenu()
		
		--Close info panel
		if IsValid(self.ItemInfoPage) then
			self.ItemInfoPage:Remove()
		end
	end
end

function PANEL:OnCursorEntered()
	if not IsValid(self.RightClickMenu) then
		self:OpenItemInfo( gui.MousePos() )
	end
end

function PANEL:OnCursorExited()
	if IsValid(self.ItemInfoPage) then
		self.ItemInfoPage:Remove()
	end
end

function PANEL:Select()
	self.IsSelected = true
	
	--Report back to ScrollPanel so we can either deselect others or (TODO) multiselect
	self.InventoryPanel:SetSelected( self )
end

function PANEL:Deselect()
	self.IsSelected = false
end


--[[---------------------------------------------------------
   Right Click Menu Functionality
-----------------------------------------------------------]]

--Open Shit
function PANEL:OpenMenu()
	self.RightClickMenu = vgui.Create( "DMenu" )
	
	--Open Information Page
	local infoOption = self.RightClickMenu:AddOption( "Compare", function()
		self:OpenItemInfoPopup()
	end)
	infoOption:SetIcon( "icon16/information.png" )
	
	--Drop This Item
	local dropOption = self.RightClickMenu:AddOption( "Drop", function()
		--self:DropItem()
	end)
	dropOption:SetIcon( "icon16/arrow_down.png" )
	
	--Salvage This Item
	local salvageOption = self.RightClickMenu:AddOption( "Salvage", function()
		--self:SalvageItem()
	end)
	salvageOption:SetIcon( "icon16/cog_go.png" )
	
	--Destroy This Item
	local destroyOption = self.RightClickMenu:AddOption( "Destroy", function()
		--self:DestroyItem()
	end)
	destroyOption:SetIcon( "icon16/cross.png" )
	
	self.RightClickMenu:Open()
end

--Open hover-over Information Page
function PANEL:OpenItemInfo()
	if not IsValid(self.ItemInfoPage) then
		self.ItemInfoPage = vgui.Create( "InventoryInfoPanel" )
		
		local resource = {}
		resource.Name = self:GetItemName()
		resource.Amount = self:GetAmount()
		
		self.ItemInfoPage:LoadFromResource( resource )
		
		self.ItemInfoPage:SetDrawOnTop( true )
	end
end

--Open Information Page in a Frame instead of a hover-over Panel
function PANEL:OpenItemInfoPopup()
	if not IsValid(self.ItemInfoPopout) then
		self.ItemInfoPopout = vgui.Create( "DFrame" )
		self.ItemInfoPopout:SetDeleteOnClose( true )
		self.ItemInfoPopout:SetScreenLock( true ) --Keep within boundaries of screen
		self.ItemInfoPopout:SetPos( gui.MousePos() )
		self.ItemInfoPopout:SetTitle( "Item Information" )
		
		local infoPanel = vgui.Create( "InventoryInfoPanel", self.ItemInfoPopout )
		infoPanel:SetPos( 3, 24 )
		infoPanel:SetCursorLock( false )
		
		local resource = {}
		resource.Name = self:GetItemName()
		resource.Amount = self:GetAmount()
		
		infoPanel:LoadFromResource( resource )
	
		self.ItemInfoPopout:MakePopup()
		
		--Throwing a timer at this for the simple reason that RichText elements
		--take more than 1 frame to figure out how many lines they have...
		timer.Simple( 0.05, function()
			self.ItemInfoPopout:SizeToChildren( true, true )
		end)
	end
end


--[[---------------------------------------------------------
   Paint
-----------------------------------------------------------]]
function PANEL:Paint( w, h )
	
	--Draw Backing Rect
	surface.SetDrawColor( Color(64, 64, 64, 255) )
	surface.DrawRect( 0, 0, w, h )
	
	--Selection Glow
	if self.IsSelected then
		surface.SetDrawColor( Color(84, 144, 96, 255) )
	else
		surface.SetDrawColor( Color(96, 96, 96, 255) )
	end
	
	--Top Bevel
	surface.SetMaterial( GradientUpMat )
	surface.DrawTexturedRect( 0, 0, w, h - SubPanelHeight )
	
	--Bottom Bevel
	surface.SetMaterial( GradientDownMat )
	surface.DrawTexturedRect( 0, h - SubPanelHeight, w, SubPanelHeight )
	
	--Separator
	surface.SetDrawColor( Color(0, 0, 0, 255) )
	surface.DrawLine( 0, h - SubPanelHeight, w, h - SubPanelHeight )
	
end



vgui.Register( "InventoryItem", PANEL, "DPanel" )