--[[

SUI Scoreboard v2.6 by .Z. Nexus is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
----------------------------------------------------------------------------------------------------------------------------
Copyright (c) 2014 .Z. Nexus <http://www.nexusbr.net> <http://steamcommunity.com/profiles/76561197983103320>

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en_US.
----------------------------------------------------------------------------------------------------------------------------
This Addon is based on the original SUI Scoreboard v2 developed by suicidal.banana.
Copyright only on the code that I wrote, my implementation and fixes and etc, The Initial version (v2) code still is from suicidal.banana.
----------------------------------------------------------------------------------------------------------------------------

$Id$
Version 2.6.2 - 12-06-2014 05:33 PM(UTC -03:00)

]]--

--[[==========================
	DIASPORA SCOREBOARD
	EDIT BY STEEVEEO
===========================]]--

include("player_row.lua")
include("player_frame.lua")

-- Get In-Use Skin
SKIN = derma.GetNamedSkin("Diaspora")

local textColor = Color(128, 128, 128, 255)
local texGradient = surface.GetTextureID("gui/center_gradient")
local diaLogo = Material("gui/diaspora/logo_alpha.png", "noclamp")

local function ColorCmp(c1, c2)
    if not c1 or not c2 then
        return false
    end

    return c1.r == c2.r and c1.g == c2.g and c1.b == c2.b and c1.a == c2.a
end

local PANEL = { }

--- Init
function PANEL:Init()
    Scoreboard.vgui = self
    self.Hostname = vgui.Create("DLabel", self)
    self.Hostname:SetText(GetHostName())

    self.SuiSc = vgui.Create("DLabel", self)
    self.SuiSc:SetText("SUI Scoreboard v2.6 by .Z. Nexus - Diaspora Edit by Steeveeo")
    self.SuiSc:SetCursor("hand")
    self.SuiSc.DoClick = function() gui.OpenURL("http://steamcommunity.com/profiles/76561197983103320") end
    self.SuiSc:SetMouseInputEnabled(true)

    self.Description = vgui.Create("DLabel", self)
    self.Description:SetText(GAMEMODE.Name .. " - " .. GAMEMODE.Author)

    self.PlayerFrame = vgui.Create("suiplayerframe", self)

    self.PlayerRows = { }

    self:UpdateScoreboard()

    -- Update the scoreboard every 1 second
    timer.Create("Scoreboard.vguiUpdater", 1, 0, self.UpdateScoreboard)

    self.lblPing = vgui.Create("DLabel", self)
    self.lblPing:SetText("Ping")

    self.lblKills = vgui.Create("DLabel", self)
    self.lblKills:SetText("Kills")

    self.lblDeaths = vgui.Create("DLabel", self)
    self.lblDeaths:SetText("Deaths")

    self.lblHealth = vgui.Create("DLabel", self)
    self.lblHealth:SetText("Health")

    self.lblArmor = vgui.Create("DLabel", self)
    self.lblArmor:SetText("Armor")

    self.lblHours = vgui.Create("DLabel", self)
    self.lblHours:SetText("Time Connected")

    self.lblTeam = vgui.Create("DLabel", self)
    self.lblTeam:SetText("Faction")

    self.lblMute = vgui.Create("DImageButton", self)
end

--- AddPlayerRow
function PANEL:AddPlayerRow(ply)
    local button = vgui.Create("suiscoreplayerrow", self.PlayerFrame:GetCanvas())
    button:SetPlayer(ply)
    self.PlayerRows[ply] = button
end

--- GetPlayerRow
function PANEL:GetPlayerRow(ply)
    return self.PlayerRows[ply]
end

--- Paint
function PANEL:Paint(w, h)
    draw.RoundedBox(10, 0, 0, self:GetWide(), self:GetTall(), Color(30, 0, 0, 150))
    surface.SetTexture(texGradient)
    surface.SetDrawColor(Color(50, 0, 0, 150))
    surface.DrawTexturedRect(0, 0, self:GetWide(), self:GetTall())

    -- Inner Box
    draw.RoundedBox(6, 15, self.Description.y - 8, self:GetWide() -30, self:GetTall() - self.Description.y - 6, Color(0, 0, 0, 200))
    surface.SetTexture(texGradient)
    surface.SetDrawColor(Color(0, 0, 0, 150))
    surface.DrawTexturedRect(15, self.Description.y - 8, self:GetWide() -30, self:GetTall() - self.Description.y - 8)

    -- Sub Header
    draw.RoundedBox(6, 108, self.Description.y - 4, self:GetWide() -128, self.Description:GetTall() + 8, Color(75, 75, 75, 75))
    surface.SetTexture(texGradient)
    surface.SetDrawColor(0, 0, 0, 50)
    surface.DrawTexturedRect(108, self.Description.y - 4, self:GetWide() -128, self.Description:GetTall() + 8)

    -- Logo
    draw.RoundedBox(8, 24, 12, 80, 80, Color(0, 0, 0, 255))
    surface.SetMaterial(diaLogo)
    surface.SetDrawColor(255, 255, 255, 225)
    surface.DrawTexturedRect(26, 10, 78, 82)
end

--- PerformLayout
function PANEL:PerformLayout()
    self:SetSize(ScrW() * 0.75, ScrH() * 0.65)

    self:SetPos((ScrW() - self:GetWide()) / 2,(ScrH() - self:GetTall()) / 2)

    self.Hostname:SizeToContents()
    self.Hostname:SetPos(115, 17)

    self.SuiSc:SizeToContents()
    self.SuiSc:SetPos((self:GetWide() - self.SuiSc:GetWide()),(self:GetTall() - 15))

    self.Description:SizeToContents()
    self.Description:SetPos(115, 60)
    self.Description:SetColor(Color(255, 255, 255, 255))

    self.PlayerFrame:SetPos(5, self.Description.y + self.Description:GetTall() + 20)
    self.PlayerFrame:SetSize(self:GetWide() -10, self:GetTall() - self.PlayerFrame.y - 20)

    local y = 0

    local PlayerSorted = { }

    for k, v in pairs(self.PlayerRows) do
        table.insert(PlayerSorted, v)
    end

    table.sort(PlayerSorted, function(a, b) return a:HigherOrLower(b) end)

    for k, v in ipairs(PlayerSorted) do
        v:SetPos(0, y)
        v:SetSize(self.PlayerFrame:GetWide(), v:GetTall())

        self.PlayerFrame:GetCanvas():SetSize(self.PlayerFrame:GetCanvas():GetWide(), y + v:GetTall())
        y = y + v:GetTall() + 1
    end

    self.Hostname:SetText(GetGlobalString("ServerName", "Garry's Mod 13"))

    self.lblPing:SizeToContents()
    self.lblKills:SizeToContents()
    self.lblDeaths:SizeToContents()
    self.lblHealth:SizeToContents()
    self.lblArmor:SizeToContents()
    self.lblHours:SizeToContents()
    self.lblTeam:SizeToContents()

    local COLUMN_SIZE = 45

    self.lblPing:SetPos(self:GetWide() - COLUMN_SIZE * 2 - self.lblPing:GetWide() / 2, self.PlayerFrame.y - self.lblPing:GetTall() -3)
    self.lblDeaths:SetPos(self:GetWide() - COLUMN_SIZE * 3.4 - self.lblDeaths:GetWide() / 2, self.PlayerFrame.y - self.lblPing:GetTall() -3)
    self.lblKills:SetPos(self:GetWide() - COLUMN_SIZE * 4.4 - self.lblKills:GetWide() / 2, self.PlayerFrame.y - self.lblPing:GetTall() -3)
    self.lblHealth:SetPos(self:GetWide() - COLUMN_SIZE * 6.4 - self.lblKills:GetWide() / 2, self.PlayerFrame.y - self.lblPing:GetTall() -3)
    self.lblArmor:SetPos(self:GetWide() - COLUMN_SIZE * 5.4 - self.lblDeaths:GetWide() / 2, self.PlayerFrame.y - self.lblPing:GetTall() -3)
    self.lblHours:SetPos(self:GetWide() - COLUMN_SIZE * 10.2 - self.lblKills:GetWide() / 2, self.PlayerFrame.y - self.lblPing:GetTall() -3)
    self.lblTeam:SetPos(self:GetWide() - COLUMN_SIZE * 16.2 - self.lblKills:GetWide() / 2, self.PlayerFrame.y - self.lblPing:GetTall() -3)
end

--- ApplySchemeSettings
function PANEL:ApplySchemeSettings()
    self.Hostname:SetFont("suiscoreboardheader")
    self.Description:SetFont("suiscoreboardsubtitle")
    self.SuiSc:SetFont("suiscoreboardsuisctext")

    tColor = team.GetColor(LocalPlayer():Team())

    self.Hostname:SetFGColor(Color(tColor.r, tColor.g, tColor.b, 255))
    self.Description:SetFGColor(Color(255, 255, 255, 255))

    self.SuiSc:SetFGColor(Color(200, 200, 200, 200))

    self.lblPing:SetFont("DefaultSmall")
    self.lblKills:SetFont("DefaultSmall")
    self.lblDeaths:SetFont("DefaultSmall")
    self.lblTeam:SetFont("DefaultSmall")
    self.lblHealth:SetFont("DefaultSmall")
    self.lblArmor:SetFont("DefaultSmall")
    self.lblHours:SetFont("DefaultSmall")

    self.lblPing:SetColor(textColor)
    self.lblDeaths:SetColor(textColor)
    self.lblKills:SetColor(textColor)
    self.lblHealth:SetColor(textColor)
    self.lblArmor:SetColor(textColor)
    self.lblHours:SetColor(textColor)
    self.lblTeam:SetColor(textColor)

    self.lblPing:SetTextColor(textColor)
    self.lblDeaths:SetTextColor(textColor)
    self.lblKills:SetTextColor(textColor)
    self.lblHealth:SetTextColor(textColor)
    self.lblArmor:SetTextColor(textColor)
    self.lblHours:SetTextColor(textColor)
    self.lblTeam:SetTextColor(textColor)

    self.lblPing:SetFGColor(textColor)
    self.lblDeaths:SetFGColor(textColor)
    self.lblKills:SetFGColor(textColor)
    self.lblHealth:SetFGColor(textColor)
    self.lblArmor:SetFGColor(textColor)
    self.lblHours:SetFGColor(textColor)
    self.lblTeam:SetFGColor(textColor)
end

--- UpdateScoreboard
function PANEL:UpdateScoreboard(force)
    if self then
        if not force and not self:IsVisible() then
            return false
        end

        for k, v in pairs(self.PlayerRows) do
            if not k:IsValid() then
                v:Remove()
                self.PlayerRows[k] = nil
            end
        end

        local PlayerList = player.GetAll()
        for id, pl in pairs(PlayerList) do
            if not self:GetPlayerRow(pl) then
                self:AddPlayerRow(pl)
            end
        end

        -- Always invalidate the layout so the order gets updated
        self:InvalidateLayout()
    end
end

vgui.Register("suiscoreboard", PANEL, "Panel")