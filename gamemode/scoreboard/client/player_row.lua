--[[

SUI Scoreboard v2.6 by .Z. Nexus is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
----------------------------------------------------------------------------------------------------------------------------
Copyright (c) 2014 .Z. Nexus <http://www.nexusbr.net> <http://steamcommunity.com/profiles/76561197983103320>

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en_US.
----------------------------------------------------------------------------------------------------------------------------
This Addon is based on the original SUI Scoreboard v2 developed by suicidal.banana.
Copyright only on the code that I wrote, my implementation and fixes and etc, The Initial version (v2) code still is from suicidal.banana.
----------------------------------------------------------------------------------------------------------------------------

$Id$
Version 2.6.2 - 12-06-2014 05:33 PM(UTC -03:00)

]]--

include("player_infocard.lua")

local texGradient = surface.GetTextureID("gui/center_gradient")

local texRatings = { }
texRatings['none'] = surface.GetTextureID("gui/silkicons/user")
texRatings['builder'] = surface.GetTextureID("gui/silkicons/wrench")
texRatings['helpful'] = surface.GetTextureID("gui/info")
texRatings['warrior'] = surface.GetTextureID("gui/diaspora/ship")
texRatings['positive'] = surface.GetTextureID("gui/silkicons/emoticon_smile")
texRatings['negative'] = surface.GetTextureID("gui/silkicons/exclamation")

surface.GetTextureID("gui/silkicons/emoticon_smile")
local PANEL = { }

--- Paint
function PANEL:Paint(w, h)

    local color = Color(100, 100, 100, 255)

    if self.Armed then
        color = Color(125, 125, 125, 255)
    end

    if self.Selected then
        color = Color(125, 125, 125, 255)
    end

    if self.Player:IsValid() then
        if self.Player:Team() == TEAM_CONNECTING then
            color = Color(25, 25, 25, 200)
        elseif self.Player:IsValid() then
            if self.Player:Team() == TEAM_UNASSIGNED then
                color = Color(25, 25, 25, 200)
            else
                tcolor = team.GetColor(self.Player:Team())
                color = Color(tcolor.r / 4, tcolor.g / 4, tcolor.b / 4, 200)
            end
        end

        if self.Player == LocalPlayer() then
            tcolor = team.GetColor(self.Player:Team())
            color = Color(tcolor.r / 4, tcolor.g / 4, tcolor.b / 4, 200)
        end
    end

    draw.RoundedBox(4, 18, 0, self:GetWide() -36, self.Size, color)

    surface.SetTexture(texGradient)
    surface.SetDrawColor(0, 0, 0, 150)
    surface.DrawTexturedRect(0, 0, self:GetWide() -36, self.Size)

    return true
end

--- SetPlayer
function PANEL:SetPlayer(ply)
    self.Player = ply
    self.infoCard:SetPlayer(ply)
    self:UpdatePlayerData()
    self.imgAvatar:SetPlayer(ply)
end

--- CheckRating
function PANEL:CheckRating(name, count)
    if self.Player:GetNetworkedInt("Rating." .. name, 0) > count then
        count = self.Player:GetNetworkedInt("Rating." .. name, 0)
        self.texRating = texRatings[name]
    end

    return count
end

--- UpdatePlayerData
function PANEL:UpdatePlayerData()
    if not self.Player:IsValid() then
        return false
    end

    self.lblName:SetText(self.Player:Nick())
    self.lblTeam:SetText(Scoreboard.getGroup(self.Player))
    self.lblRank:SetText(string.upper(Scoreboard.getRank(self.Player)))

    self.modelIcon.Model = self.Player:GetModel()
    self.modelIcon:SetModel(self.modelIcon.Model)

    self.lblHours:SetText(Scoreboard.formatTime(Scoreboard.getPlayerTime(self.Player)))
    self.lblHealth:SetText(self.Player:Health())
    self.lblArmor:SetText(self.Player:Armor())
    self.lblFrags:SetText(self.Player:Frags())
    self.lblDeaths:SetText(self.Player:Deaths())
    self.lblPing:SetText(self.Player:Ping())

    -- Change the icon of the mute button based on state
    if self.Muted == nil or self.Muted ~= self.Player:IsMuted() then
        self.Muted = self.Player:IsMuted()
        if self.Muted then
            self.lblMute:SetImage("icon32/muted.png")
        else
            self.lblMute:SetImage("icon32/unmuted.png")
        end

        self.lblMute.DoClick = function() self.Player:SetMuted(not self.Muted) end
    end

    -- Work out what icon to draw
    self.texRating = surface.GetTextureID("gui/silkicons/emoticon_smile")

    self.texRating = texRatings['none']
    local count = 0

    count = self:CheckRating('builder', count)
    count = self:CheckRating('helpful', count)
    count = self:CheckRating('warrior', count)
    count = self:CheckRating('positive', count)
    count = self:CheckRating('negative', count)
end

--- Int
function PANEL:Init()
    self.Size = 40
    self:OpenInfo(false)
    self.infoCard = vgui.Create("suiscoreplayerinfocard", self)

    self.lblName = vgui.Create("DLabel", self)
    self.lblTeam = vgui.Create("DLabel", self)
    self.lblRank = vgui.Create("DLabel", self)
    self.lblHours = vgui.Create("DLabel", self)
    self.lblHealth = vgui.Create("DLabel", self)
    self.lblDeaths = vgui.Create("DLabel", self)
    self.lblArmor = vgui.Create("DLabel", self)
    self.lblFrags = vgui.Create("DLabel", self)
    self.lblPing = vgui.Create("DLabel", self)
    self.lblMute = vgui.Create("DImageButton", self)
    self.modelIcon = vgui.Create("SpawnIcon", self)
    self.imgAvatar = vgui.Create("AvatarImage", self)
    self.lblAvatarFix = vgui.Create("DLabel", self)
    self.lblAvatarFix:SetText("")
    self.lblAvatarFix:SetCursor("hand")
    self.lblAvatarFix.DoClick = function() self.Player:ShowProfile() end

    -- If you don't do this it'll block your clicks
    self.lblName:SetMouseInputEnabled(false)
    self.lblTeam:SetMouseInputEnabled(false)
    self.lblRank:SetMouseInputEnabled(false)
    self.lblHours:SetMouseInputEnabled(false)
    self.lblHealth:SetMouseInputEnabled(false)
    self.lblArmor:SetMouseInputEnabled(false)
    self.lblFrags:SetMouseInputEnabled(false)
    self.lblDeaths:SetMouseInputEnabled(false)
    self.lblPing:SetMouseInputEnabled(false)
    self.modelIcon:SetMouseInputEnabled(false)
    self.imgAvatar:SetMouseInputEnabled(false)
    self.lblMute:SetMouseInputEnabled(true)
    self.lblAvatarFix:SetMouseInputEnabled(true)
end

--- ApplySchemeSettings
function PANEL:ApplySchemeSettings()
    self.lblName:SetFont("suiscoreboardplayername")
    self.lblTeam:SetFont("suiscoreboardplayername")
    self.lblRank:SetFont("suiscoreboardplayerrank")
    self.lblHours:SetFont("suiscoreboardplayername")
    self.lblHealth:SetFont("suiscoreboardplayername")
    self.lblArmor:SetFont("suiscoreboardplayername")
    self.lblFrags:SetFont("suiscoreboardplayername")
    self.lblDeaths:SetFont("suiscoreboardplayername")
    self.lblPing:SetFont("suiscoreboardplayername")
    self.lblAvatarFix:SetFont("suiscoreboardplayername")

    local namecolor = Color(255, 255, 255, 255)

    self.lblName:SetColor(namecolor)
    self.lblTeam:SetColor(namecolor)
    self.lblRank:SetColor(namecolor)
    self.lblHours:SetColor(namecolor)
    self.lblHealth:SetColor(namecolor)
    self.lblArmor:SetColor(namecolor)
    self.lblFrags:SetColor(namecolor)
    self.lblDeaths:SetColor(namecolor)
    self.lblPing:SetColor(namecolor)
    self.lblAvatarFix:SetColor(namecolor)

    self.lblName:SetFGColor(namecolor)
    self.lblTeam:SetFGColor(namecolor)
    self.lblHours:SetFGColor(namecolor)
    self.lblHealth:SetFGColor(namecolor)
    self.lblArmor:SetFGColor(namecolor)
    self.lblFrags:SetFGColor(namecolor)
    self.lblDeaths:SetFGColor(namecolor)
    self.lblPing:SetFGColor(namecolor)
    self.lblAvatarFix:SetFGColor(namecolor)
end

--- DoClick
function PANEL:DoClick()
    if self.Open then
        surface.PlaySound("ui/buttonclickrelease.wav")
    else
        surface.PlaySound("ui/buttonclick.wav")
    end

    self:OpenInfo(not self.Open)
end

--- OpenInfo
function PANEL:OpenInfo(bool)
    if bool then
        self.TargetSize = 96
    else
        self.TargetSize = 40
    end

    self.Open = bool
end

--- Think
function PANEL:Think()
    if self.Size ~= self.TargetSize then
        self.Size = math.Approach(self.Size, self.TargetSize,(math.abs(self.Size - self.TargetSize) + 1) * 10 * FrameTime())
        self:PerformLayout()
        Scoreboard.vgui:InvalidateLayout()
    end

    if not self.PlayerUpdate or self.PlayerUpdate < CurTime() then
        self.PlayerUpdate = CurTime() + 0.5
        self:UpdatePlayerData()
    end
end

--- PerformLayout
function PANEL:PerformLayout()
    self:SetSize(self:GetWide(), self.Size)

    self.modelIcon:SetPos(21, 4)
    self.modelIcon:SetSize(32, 32)

    self.lblName:SizeToContents()
    self.lblName:SetPos(92, 3)
    self.lblRank:SizeToContents()
    self.lblRank:SetPos(92, 19)
    self.lblRank:SetSize(string.len(self.lblRank:GetText()) * 20, 25)
    self.lblTeam:SizeToContents()
    self.lblMute:SetSize(32, 32)
    self.lblHours:SizeToContents()

    self.imgAvatar:SetPos(57, 4)
    self.imgAvatar:SetSize(32, 32)
    self.lblAvatarFix:SetPos(57, 4)
    self.lblAvatarFix:SetSize(32, 32)

    local COLUMN_SIZE = 45

    self.lblMute:SetPos(self:GetWide() - COLUMN_SIZE - 8, 0)
    self.lblPing:SetPos(self:GetWide() - COLUMN_SIZE * 2, 0)
    self.lblDeaths:SetPos(self:GetWide() - COLUMN_SIZE * 3.4, 0)
    self.lblFrags:SetPos(self:GetWide() - COLUMN_SIZE * 4.4, 0)
    self.lblArmor:SetPos(self:GetWide() - COLUMN_SIZE * 5.4, 0)
    self.lblHealth:SetPos(self:GetWide() - COLUMN_SIZE * 6.4, 0)
    self.lblHours:SetPos(self:GetWide() - COLUMN_SIZE * 10.3, 0)
    self.lblTeam:SetPos(self:GetWide() - COLUMN_SIZE * 16.3, 0)

    if self.Open or self.Size ~= self.TargetSize then
        self.infoCard:SetVisible(true)
        self.infoCard:SetPos(18, self.modelIcon:GetTall() + 12)
        self.infoCard:SetSize(self:GetWide() -36, self:GetTall() - self.lblName:GetTall() + 5)
    else
        self.infoCard:SetVisible(false)
    end
end

local function stringCmp(str1, str2)
    local chars1 = string.Explode("", str1)
    local chars2 = string.Explode("", str2)

    local count = math.max(#chars1, #chars2)

    for i = 1, count do
        local c1 = chars1[i]
        local c2 = chars2[i]

        -- Something nil, return
        if i > #chars1 then return false end
        if i > #chars2 then return true end

        -- Convert to number
        local c1byte = string.byte(c1)
        local c2byte = string.byte(c2)

        -- Compare
        if c1byte ~= c2byte then
            return(c1byte > c2byte)
        end
    end

    return false
end

--- HigherOrLower
function PANEL:HigherOrLower(row)
    if SC ~= nil then
        -- Push joiners to the bottom
        if self.Player:Team() == TEAM_CONNECTING then
            return true
        end

        -- Compare Factions
        local thisFaction = self.Player:GetNWString("faction", "Civilian")
        local thatFaction = row.Player:GetNWString("faction", "Civilian")

        return stringCmp(thisFaction, thatFaction)
    else
        if self.Player:Team() == TEAM_CONNECTING then
            return false
        end

        if self.Player:Team() ~= row.Player:Team() then
            return self.Player:Team() < row.Player:Team()
        end

        if self.Player:Frags() == row.Player:Frags() then
            return self.Player:Deaths() < row.Player:Deaths()
        end

        return self.Player:Frags() > row.Player:Frags()
    end
end

vgui.Register("suiscoreplayerrow", PANEL, "Button")