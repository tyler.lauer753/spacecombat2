--[[

SUI Scoreboard v2.6 by .Z. Nexus is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
----------------------------------------------------------------------------------------------------------------------------
Copyright (c) 2014 .Z. Nexus <http://www.nexusbr.net> <http://steamcommunity.com/profiles/76561197983103320>

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/deed.en_US.
----------------------------------------------------------------------------------------------------------------------------
This Addon is based on the original SUI Scoreboard v2 developed by suicidal.banana.
Copyright only on the code that I wrote, my implementation and fixes and etc, The Initial version (v2) code still is from suicidal.banana.
----------------------------------------------------------------------------------------------------------------------------

$Id$
Version 2.6.2 - 12-06-2014 05:33 PM(UTC -03:00)

]]--

include("admin_buttons.lua")
include("vote_button.lua")

local PANEL = { }

--- Init
function PANEL:Init()
    self.InfoLabels = { }
    self.InfoLabels[1] = { }
    self.InfoLabels[2] = { }
    self.InfoLabels[3] = { }

    self.btnKick = vgui.Create("suiplayerkickbutton", self)
    self.btnBan = vgui.Create("suiplayerbanbutton", self)
    self.btnPBan = vgui.Create("suiplayerpermbanbutton", self)

    self.VoteButtons = { }

    self.VoteButtons[5] = vgui.Create("suispawnmenuvotebutton", self)
    self.VoteButtons[5]:SetUp("silkicons/wrench", "builder", "This player is good at building!")

    self.VoteButtons[4] = vgui.Create("suispawnmenuvotebutton", self)
    self.VoteButtons[4]:SetUp("info", "helpful", "This player is very helpful!")

    self.VoteButtons[3] = vgui.Create("suispawnmenuvotebutton", self)
    self.VoteButtons[3]:SetUp("diaspora/ship", "warrior", "This player is a worthy opponent!")

    self.VoteButtons[2] = vgui.Create("suispawnmenuvotebutton", self)
    self.VoteButtons[2]:SetUp("silkicons/emoticon_smile", "positive", "I like this player!")

    self.VoteButtons[1] = vgui.Create("suispawnmenuvotebutton", self)
    self.VoteButtons[1]:SetUp("silkicons/exclamation", "negative", "This player is a mingebag!")
end

--- SetInfo
function PANEL:SetInfo(column, k, v)
    if not v or v == "" then v = "N/A" end

    if not self.InfoLabels[column][k] then
        self.InfoLabels[column][k] = { }
        self.InfoLabels[column][k].Key = vgui.Create("DLabel", self)
        self.InfoLabels[column][k].Value = vgui.Create("DLabel", self)
        self.InfoLabels[column][k].Key:SetText(k)
        self.InfoLabels[column][k].Key:SetColor(Color(255, 255, 255, 255))
        self.InfoLabels[column][k].Key:SetFont("suiscoreboardcardinfo")
        self:InvalidateLayout()
    end

    self.InfoLabels[column][k].Value:SetText(v)
    self.InfoLabels[column][k].Value:SetColor(Color(255, 255, 255, 255))
    self.InfoLabels[column][k].Value:SetFont("suiscoreboardcardinfo")
    return true
end

--- SetPlayer
function PANEL:SetPlayer(ply)
    self.Player = ply
    self:UpdatePlayerData()
end

--- UpdatePlayerData
function PANEL:UpdatePlayerData()
    if not self.Player then return end
    if not self.Player:IsValid() then return end

    self:SetInfo(1, "Props:", self.Player:GetCount("props"))
    self:SetInfo(1, "Ships:", self.Player:GetCount("sc_core"))
    self:SetInfo(1, "Weapons:", self.Player:GetCount("sc_weapon_base"))

    self:SetInfo(2, "Vehicles:", self.Player:GetCount("vehicles"))
    self:SetInfo(2, "Detail Props:", self.Player:GetCount("detail_props"))
    self:SetInfo(2, "Expression2s:", self.Player:GetCount("wire_expressions"))

    self:InvalidateLayout()
end

--- ApplySchemeSettings
function PANEL:ApplySchemeSettings()
    for _k, column in pairs(self.InfoLabels) do
        for k, v in pairs(column) do
            v.Key:SetFGColor(30, 30, 30, 255)
            v.Value:SetFGColor(255, 255, 255, 255)
        end
    end
end

--- Think
function PANEL:Think()
    if self.PlayerUpdate and self.PlayerUpdate > CurTime() then return end
    self.PlayerUpdate = CurTime() + 0.25

    self:UpdatePlayerData()
end

--- PerformLayout
function PANEL:PerformLayout()
    local x = 5

    for column, column in pairs(self.InfoLabels) do

        local y = 0
        local RightMost = 0

        for k, v in pairs(column) do
            v.Key:SetPos(x, y)
            v.Key:SizeToContents()

            v.Value:SetPos(x + 70, y)
            v.Value:SizeToContents()

            y = y + v.Key:GetTall() + 2

            RightMost = math.max(RightMost, v.Value.x + v.Value:GetWide())
        end

        if x < 100 then
            x = x + 205
        else
            x = x + 115
        end
    end

    if not LocalPlayer():IsAdmin() then
        self.btnKick:SetVisible(false)
        self.btnBan:SetVisible(false)
        self.btnPBan:SetVisible(false)
    else
        self.btnKick:SetVisible(true)
        self.btnBan:SetVisible(true)
        self.btnPBan:SetVisible(true)

        self.btnKick:SetPos(self:GetWide() -275, 0)
        self.btnKick:SetSize(46, 20)

        self.btnBan:SetPos(self:GetWide() -225, 0)
        self.btnBan:SetSize(46, 20)

        self.btnPBan:SetPos(self:GetWide() -175, 0)
        self.btnPBan:SetSize(46, 20)

        self.btnKick.DoClick = function() Scoreboard.kick(self.Player) end
        self.btnPBan.DoClick = function() Scoreboard.pBan(self.Player) end
        self.btnBan.DoClick = function() Scoreboard.ban(self.Player) end
    end

    for k, v in ipairs(self.VoteButtons) do
        v:InvalidateLayout()
        if k < 6 then
            v:SetPos(self:GetWide() - k * 25, 0)
        elseif k < 11 then
            v:SetPos(self:GetWide() -(k - 5) * 25, 36)
        else
            v:SetPos(self:GetWide() -(k - 10) * 25, 72)
        end
        v:SetSize(20, 32)
    end
end

--- Paint
function PANEL:Paint(w, h)
    return true
end

vgui.Register("suiscoreplayerinfocard", PANEL, "Panel")