require("lip")

local Quiz = {}

Quiz.ShouldKickPlayerOnFailure = true
Quiz.KickWaitTime = 10

Quiz.Questions = {}
Quiz.Questions["Is parenting required?"] = {"Yes", "No"}
Quiz.Questions["Is violence on spawn allowed?"] = {"Yes", "No"}
Quiz.Questions["Can you attack players who aren't on spawn?"] = {"Yes", "No"}
Quiz.Questions["Should you expect to be banned if you're a dick?"] = {"Yes", "No"}
Quiz.Questions["What should you hit if you want help?"] = {"Chat key", "F2"}
Quiz.Questions["How do you know someone is an admin?"] = {"Scoreboard", "Asking in chat"}

Quiz.Answers = {}
Quiz.Answers["Is parenting required?"] = "Yes"
Quiz.Answers["Is violence on spawn allowed?"] = "No"
Quiz.Answers["Can you attack players who aren't on spawn?"] = "Yes"
Quiz.Answers["Should you expect to be banned if you're a dick?"] = "Yes"
Quiz.Answers["What should you hit if you want help?"] = "F2"
Quiz.Answers["How do you know someone is an admin?"] = "Scoreboard"

if not sql.TableExists("quiz") then
	local query = "CREATE TABLE quiz (unique_id varchar(255), taken int)"
	local result = sql.Query(query)
	if not sql.TableExists("quiz") then
		SC.Error("Something went wrong with the quiz query!\n")
		SC.Error(sql.LastError(result) .. "\n")
	end
end

hook.Add("InitPostEntity", "LoadQuizConfiguration", function()
    if file.Exists(SC.DataFolder.."/config/quiz.txt", "DATA") then
        local NewData = lip.load(SC.DataFolder.."/config/quiz.txt")

        if NewData and table.Count(NewData) > 0 and NewData.Answers and NewData.Settings then
            Quiz.Answers = NewData.Answers
            Quiz.ShouldKickPlayerOnFailure = NewData.Settings.ShouldKickPlayerOnFailure
            Quiz.KickWaitTime = NewData.Settings.KickWaitTime
            Quiz.Questions = {}

            for section, data in pairs(NewData) do
                if string.lower(section:sub(0, 10)) == "questions/" then
                    Quiz.Questions[section:sub(11)] = data
                end
            end

            if table.Count(Quiz.Questions) ~= table.Count(Quiz.Answers) then
                SC.Error("Bad number of quiz answers detected! Users will not be able to finish the quiz successfully!")
            end
        else
            SC.Error("Malformed quiz configuration file detected!")
        end
    else
        local SaveData = {}

        SaveData["Settings"] = {
            ["ShouldKickPlayerOnFailure"] = Quiz.ShouldKickPlayerOnFailure,
            ["KickWaitTime"] = Quiz.KickWaitTime
        }

        SaveData["Answers"] = Quiz.Answers

        for i,k in pairs(Quiz.Questions) do
            SaveData["Questions/"..i] = k
        end

        file.CreateDir(SC.DataFolder.."/config/")
        lip.save(SC.DataFolder.."/config/quiz.txt", SaveData)
    end
end)

net.Receive("SC.GetQuizQuestions", function(len, ply)
    local NeedsQuiz = true
    if sql.Query("SELECT * FROM quiz WHERE unique_id='"..ply:SteamID64().."'") then
        NeedsQuiz = false
    end

    net.Start("SC.GetQuizQuestions")
    net.WriteTable(Quiz.Questions)
    net.WriteBool(Quiz.ShouldKickPlayerOnFailure)
    net.WriteInt(Quiz.KickWaitTime, 8)
    net.WriteBool(NeedsQuiz)
    net.Send(ply)
end)

net.Receive("SC.CheckQuiz", function(len, ply)
    local Choices = net.ReadTable()
    local Failed = not table.Compare(Choices, Quiz.Answers)

    if Quiz.ShouldKickPlayerOnFailure and Failed then
        timer.Simple(Quiz.KickWaitTime, function()
            if IsValid(ply) then
                ply:Kick("You failed the quiz! Please try again later!")
            end
        end)
    end

    if not Failed then
        sql.Query("INSERT INTO quiz ('unique_id','taken') VALUES ('"..ply:SteamID64().."','1')")
    end

    net.Start("SC.CheckQuiz")
    net.WriteBool(Failed)
    net.Send(ply)
end)

util.AddNetworkString("SC.GetQuizQuestions")
util.AddNetworkString("SC.CheckQuiz")