local GM = GM
local class = GM.class
local const = GM.constants
local internal = GM.internal

local os = os
local tonumber = tonumber
local tostring = tostring
local table = table
local pairs = pairs
local error = error
local string = string
local Color = Color
local math = math
local smooth = GM.util.smoother

--Settings
if not ConVarExists("sc_hud_showTimeElement") then
	CreateConVar("sc_hud_showTimeElement", 1, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
end
local drawTimeBar = GetConVarNumber("sc_hud_showTimeElement") ~= 0

--== HUD SYSTEM FUNCTIONS ==--
internal.hud = {}

function GM:registerHUDComponent(name, component)
	if not component.is_A or not component:is_A( GM.class.getClass("HudComponent") ) then error("Component is not a HudComponent. Failed to register, "..name) return end
	internal.hud[name] = component
end

function GM:getHudComponentByName(name)
	return internal.hud[name]
end

function GM:getHudComponentByClass(classname)
	local ret = {}
	for k, v in pairs(internal.hud) do
		if v.is_A and v:is_A(classname) then
		   table.insert(ret, v)
		end
	end
	return ret
end

--==========================--


-------------------------
--------------------------
-------------------------

-- Locals for the panels so we can reference them throughout cl_hud
local generated = false

local statPanel, health, health_txt, armor, armor_txt -- Stats Panel & Children
local lsBar_Temp, lsBar_O2, lsBar_N, lsBar_CO2, lsBar_Temptxt, lsBar_O2txt, lsBar_Ntxt, lsBar_CO2txt -- Lifesupport Stuff
local atmoLabel, atmoWarning --Atmosphere "HOSTILE" warning labels
local suitPercent, suitLabel, suitTime --Suit Display
local secClock, minClock, hrClock, fpsInd -- Clock & FPS
local secLabel, minLabel, hrLabel, fpsLabel -- Some labels for Clock & FPS
local secN, minN, hrN, fpsN -- Numbers for the Clock & FPS
local ammoPanel, ammo, ammo_txt, alt -- Ammo Panel & Children
local infoPanel, info -- Information Panel & Children

local wepTable = {}

local function calcAmmo()

	local ammo, maxAmmo, secondaryAmmo
	local wep = LocalPlayer():GetActiveWeapon()

	if wep:IsValid() then
		ammo = wep:Clip1()

		if ammo ~= nil and ammo > 0 then

			-- Add Wep max ammo if it doesn't exist. Otherwise update ammo amount if we have something bigger
			if not wepTable[ wep:GetClass() ] or ammo > wepTable[ wep:GetClass() ] then
				wepTable[ wep:GetClass() ] = ammo or 0
			end
		end

		secondaryAmmo = wep:GetSecondaryAmmoType() and LocalPlayer():GetAmmoCount( wep:GetSecondaryAmmoType()) or 0
		maxAmmo = wepTable[ wep:GetClass() ]
	end

	return ammo, maxAmmo, secondaryAmmo
end

local function genComponents()
	if not generated then

		---
		--- Stats Panel - Bottom Left
		---

		statPanel = GM.class.getClass("HudPanel"):new( 0, 0, 0, 0, Color(20, 20, 20, 150), true)
		statPanel:setPadding(10, 10)
		statPanel:setAutoSize(false) --It really hates HudRadialIndicators and I don't care to fix it right now.
		statPanel:setWidth(292)
		statPanel:setHeight(105)

		local think = statPanel.think
		statPanel.think = function(self)
			think(self)

			self:setPos(20, ScrH() - 20 - self:getHeight())
		end

		health = GM.class.getClass("HudBarIndicator"):new(0, 0, 110, 10, 0, 100, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(70, 0, 0, 255), false)
		health_txt = GM.class.getClass("TextElement"):new(health:getX(), health:getY() + health:getHeight(), Color(255, 255, 255, 255), "Health")
		health:setSmoothFactor(0.3)

		local think = health.think
		health.think = function(self)
			self:setTarget(LocalPlayer():Health())

			think(self)
		end

		local think = health_txt.think
		health_txt.think = function(self)
			think(self)

			self:setText(string.format("Health: %s", math.Round(LocalPlayer():Health())) .. "%")
		end

		armor = GM.class.getClass("HudBarIndicator"):new(0, health_txt:getY() + health_txt:getHeight() + 5, 110, 10, 0, 100, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(70, 0, 0, 255), false)
		armor_txt = GM.class.getClass("TextElement"):new(armor:getX(), armor:getY() + armor:getHeight(), Color(255, 255, 255, 255), "Armor")
		armor:setSmoothFactor(0.3)

		local think = armor.think
		armor.think = function(self)
			self:setTarget(LocalPlayer():Armor())

			think(self)
		end

		local think = armor_txt.think
		armor_txt.think = function(self)
			think(self)

			self:setText(string.format("Armor: %s", math.Round(LocalPlayer():Armor())) .. "%" )
		end

		-- LS Bars

		lsBar_Temp = GM.class.getClass("HudBarIndicator"):new(health:getWidth() + 7, 0, 12, 80, 0, 1, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(70, 0, 0, 255), true)
		lsBar_Temp:setSmoothFactor(0.3)

		lsBar_Temptxt = GM.class.getClass("TextElement"):new(lsBar_Temp:getX() + (lsBar_Temp:getWidth() / 2), lsBar_Temp:getY() + lsBar_Temp:getHeight(), Color(255, 255, 255, 255), "°K")
		lsBar_Temptxt:setFont("HudHintTextSmall")
		lsBar_Temptxt:setXAlign(TEXT_ALIGN_CENTER)

		lsBar_O2 = GM.class.getClass("HudBarIndicator"):new(lsBar_Temp:getX() + lsBar_Temp:getWidth() + 7, 0, 14, 80, 0, 1, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(0, 144, 0, 255), true)
		lsBar_O2:setSmoothFactor(0.3)

		lsBar_O2txt = GM.class.getClass("TextElement"):new(lsBar_O2:getX() + (lsBar_O2:getWidth() / 2), lsBar_O2:getY() + lsBar_O2:getHeight(), Color(255, 255, 255, 255), "O2")
		lsBar_O2txt:setFont("HudHintTextSmall")
		lsBar_O2txt:setXAlign(TEXT_ALIGN_CENTER)

		lsBar_CO2 = GM.class.getClass("HudBarIndicator"):new(lsBar_O2:getX() + lsBar_O2:getWidth() + 3, 0, 14, 80, 0, 1, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(128, 128, 0, 255), true)
		lsBar_CO2:setSmoothFactor(0.3)

		lsBar_CO2txt = GM.class.getClass("TextElement"):new(lsBar_CO2:getX() + (lsBar_CO2:getWidth() / 2), lsBar_CO2:getY() + lsBar_CO2:getHeight(), Color(255, 255, 255, 255), "CO2")
		lsBar_CO2txt:setFont("HudHintTextSmall")
		lsBar_CO2txt:setXAlign(TEXT_ALIGN_CENTER)

		lsBar_N = GM.class.getClass("HudBarIndicator"):new(lsBar_CO2:getX() + lsBar_CO2:getWidth() + 3, 0, 14, 80, 0, 1, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(0, 0, 255, 255), true)
		lsBar_N:setSmoothFactor(0.3)

		lsBar_Ntxt = GM.class.getClass("TextElement"):new(lsBar_N:getX() + (lsBar_N:getWidth() / 2), lsBar_N:getY() + lsBar_N:getHeight(), Color(255, 255, 255, 255), "N")
		lsBar_Ntxt:setFont("HudHintTextSmall")
		lsBar_Ntxt:setXAlign(TEXT_ALIGN_CENTER)

		local think = lsBar_O2.think
		lsBar_O2.think = function(self)
			if LocalPlayer().Atmo_O2Percent ~= nil then
				self:setTarget(LocalPlayer().Atmo_O2Percent)
			end

			think(self)
		end

		local think = lsBar_CO2.think
		lsBar_CO2.think = function(self)
			if LocalPlayer().Atmo_CO2Percent ~= nil then
				self:setTarget(LocalPlayer().Atmo_CO2Percent)
			end

			think(self)
		end

		local think = lsBar_N.think
		lsBar_N.think = function(self)
			if LocalPlayer().Atmo_NPercent ~= nil then
				self:setTarget(LocalPlayer().Atmo_NPercent)
			end

			think(self)
		end

		-- End LS Bars

		-- Suit Display

		suitPercent = GM.class.getClass("HudRadialIndicator"):new(lsBar_CO2:getX() + lsBar_CO2:getWidth() + 65, lsBar_Temp:getY() + (lsBar_Temp:getHeight() / 2), 79, 79, 0, 0, Color(0, 0, 0, 150), Color(0, 0, 50, 80), true, Color(0, 0, 50, 255))
		suitPercent:setMaxValue(1)
		suitPercent:setSmoothFactor(0.5)

		suitLabel = GM.class.getClass("TextElement"):new(suitPercent:getX(), suitPercent:getY() + (suitPercent:getHeight() / 2) + 1, Color(255, 255, 255, 255), "Suit Capacity")
		suitLabel:setFont("HudHintTextSmall")
		suitLabel:setXAlign(TEXT_ALIGN_CENTER)

		suitTime = GM.class.getClass("TextElement"):new(suitPercent:getX(), suitPercent:getY() - 6, Color(255, 255, 255, 255), "00:00")
		suitTime:setXAlign(TEXT_ALIGN_CENTER)

		local think = suitPercent.think
		suitPercent.think = function(self)
			if LocalPlayer().IntakeAmount ~= nil then
				self:setTarget(LocalPlayer().IntakeAmount / LocalPlayer().IntakeMaxAmount)
			end
			think(self)
		end

		local think = suitTime.think
        local oldamount = 0
        local delta = 0
        local nextupdate = CurTime()
		suitTime.think = function(self)
			if CurTime() > nextupdate and LocalPlayer().IntakeAmount ~= nil then
                local newamount = LocalPlayer().IntakeAmount or 0
                local maxamount = LocalPlayer().IntakeMaxAmount or 0
                local outamount = LocalPlayer().OuttakeAmount or 0
                delta = newamount - oldamount

                if math.abs(delta) > 0.001 then
                    local color = delta > 0 and Color(0, 50, 0) or Color(50, 0, 0)
				    local time = string.FormattedTime((delta > 0 and math.min(outamount, maxamount - newamount) or newamount) / math.abs(delta))
                    suitPercent:setCenterColor(color)
				    self:setText(string.format("%02i:%02i", time.m, time.s))
                else
                    local time = string.FormattedTime(newamount)
				    self:setText(string.format("%02i:%02i", time.m, time.s))
                    suitPercent:setCenterColor(Color(0, 0, 50))
                end

                oldamount = newamount
                nextupdate = CurTime() + 1
			end
			think(self)
		end

		-- End Suit Display

		-- Atmosphere Warning

		atmoLabel = GM.class.getClass("TextElement"):new(0, armor_txt:getY() + 17, Color(255, 255, 255, 255), "Atmosphere:")
		atmoLabel:setFont("HudHintTextSmall")

		atmoWarning = GM.class.getClass("TextElement"):new(health:getX() + (health:getWidth() / 2), atmoLabel:getY() + 7, Color(0, 255, 0, 255), "SAFE")
		atmoWarning:setFont("DermaLarge")
		atmoWarning:setXAlign(TEXT_ALIGN_CENTER)

		local think = atmoWarning.think
		atmoWarning.think = function(self)
			if LocalPlayer().Atmo_O2Percent ~= nil and LocalPlayer().Atmo_Temp ~= nil then
				if LocalPlayer().Atmo_O2Percent > 0.05 and LocalPlayer().Atmo_Temp >= 283 and LocalPlayer().Atmo_Temp <= 308 then
					atmoWarning:setText("SAFE")
					atmoWarning:setColor(Color(0, 255, 0, 255))
				else
					atmoWarning:setText("HOSTILE")
					atmoWarning:setColor(Color(255, 0, 0, 255))
				end
			else
				self:setText("") --Invalid state
			end
			think(self)
		end

		-- End Atmosphere Warning

		statPanel:addChild(health)
		statPanel:addChild(health_txt)
		statPanel:addChild(armor)
		statPanel:addChild(armor_txt)
		statPanel:addChild(lsBar_Temp)
		statPanel:addChild(lsBar_Temptxt)
		statPanel:addChild(lsBar_O2)
		statPanel:addChild(lsBar_O2txt)
		statPanel:addChild(lsBar_CO2)
		statPanel:addChild(lsBar_CO2txt)
		statPanel:addChild(lsBar_N)
		statPanel:addChild(lsBar_Ntxt)

		statPanel:addChild(atmoLabel)
		statPanel:addChild(atmoWarning)

		statPanel:addChild(suitPercent)
		statPanel:addChild(suitLabel)
		statPanel:addChild(suitTime)


		---
		--- End of StatsPanel
		---

		---
		--- Clock & FPS - Bottom
		---
		if drawTimeBar then
			-- Seconds
			secClock = GM.class.getClass("HudRadialIndicator"):new(0, 0, 60, 60, 0, 0, Color(0, 0, 0, 200), Color(50, 0, 0, 80), false, Color(50, 0, 0, 255))
			secClock:setMaxValue(60) -- 60 seconds in a minute hurr durr
			secClock:setPos(math.Round(ScrW() / 2 + secClock:getRadius() + secClock:getRadius() * 2 + 5), ScrH() - math.Round(secClock:getHeight() / 2 + 10))
			secClock:setSmoothFactor(0.5)

			local think = secClock.think
			secClock.think = function(self)
				self:setTarget(tonumber(os.date("%S")))
				think(self)
			end

			-- Minutes
			minClock = GM.class.getClass("HudRadialIndicator"):new(0, 0, 60, 60, 0, 0, Color(0, 0, 0, 200), Color(50, 0, 0, 80), false, Color(50, 0, 0, 255))
			minClock:setMaxValue(60) -- 60 seconds in a minute hurr durr
			minClock:setPos(secClock:getX() - secClock:getRadius() - minClock:getRadius() - 10, secClock:getY())
			minClock:setSmoothFactor(0.6)

			local think = minClock.think
			minClock.think = function(self)
				self:setTarget(tonumber(os.date("%M") + os.date("%S") / 60))
				think(self)
			end

			-- Hours
			hrClock = GM.class.getClass("HudRadialIndicator"):new(0, 0, 60, 60, 0, 0, Color(0, 0, 0, 200), Color(50, 0, 0, 80), false, Color(50, 0, 0, 255))
			hrClock:setMaxValue(12)
			hrClock:setPipSize(1.8)
			hrClock:setPos(minClock:getX() - minClock:getRadius() - hrClock:getRadius() - 10, minClock:getY())
			hrClock:setSmoothFactor(0.6)

			local think = hrClock.think
			hrClock.think = function(self)
				self:setTarget(tonumber((os.date("%H") % 12) + (os.date("%M")/60)))
				think(self)
			end

			-- FPS
			fpsInd = GM.class.getClass("HudRadialIndicator"):new(0, 0, 60, 60, 0, 0, Color(0, 0, 0, 200), Color(50, 0, 0, 80), true, Color(50, 0, 0, 255))
			fpsInd:setMaxValue(math.max(GetConVarNumber("fps_max") or 300, 30))
            fpsInd:setPos(hrClock:getX() - hrClock:getRadius() - fpsInd:getRadius() - 10, hrClock:getY())
            fpsInd:setTarget(60)

            local think = fpsInd.think
            fpsInd.think = function(self)
				self:setTarget(math.ceil((self:getTarget() + 1 / RealFrameTime()) / 2))
				think(self)
			end

			---
			--- LABELS
			---

			secLabel = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "Seconds")
			minLabel = GM.class.getClass("TextElement"):new(0 ,0, Color(255, 255, 255, 255), "Minutes")
			hrLabel = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "Hours")
			fpsLabel = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "FPS")

			secN = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "00")
			minN = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "00")
			hrN = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "00")
			fpsN = GM.class.getClass("TextElement"):new(0, 0, Color(255, 255, 255, 255), "00")

			secLabel:setPos(secClock:getX() - secLabel:getWidth() / 2, secClock:getY() - secClock:getRadius() - secLabel:getHeight())
			minLabel:setPos(minClock:getX() - minLabel:getWidth() / 2, minClock:getY() - minClock:getRadius() - minLabel:getHeight())
			hrLabel:setPos(hrClock:getX() - hrLabel:getWidth() / 2, hrClock:getY() - hrClock:getRadius() - hrLabel:getHeight())
			fpsLabel:setPos(fpsInd:getX() - fpsLabel:getWidth() / 2, fpsInd:getY() - fpsInd:getRadius() - fpsLabel:getHeight())

			secN:setPos(secClock:getX() - secN:getHeight() / 2, secClock:getY() - secN:getHeight() / 2)
			minN:setPos(minClock:getX() - minN:getWidth() / 2, minClock:getY() - minN:getHeight() / 2)
			hrN:setPos(hrClock:getX() - hrN:getWidth() / 2, hrClock:getY() - hrN:getHeight() / 2)

			local think = secN.think
			secN.think = function(self)
				local val = ""
				local time = math.floor(secClock:getValue())
				if time < 10 then
					val = "0" .. tostring(time)
				else
					val = tostring(time)
				end

				self:setText(val)
				--secN:setPos(secClock:getX() - secN:getHeight() / 2, secClock:getY() - secN:getHeight() / 2)
				think(self)
			end

			local think = minN.think
			minN.think = function(self)
				local val = ""
				local time = math.floor(minClock:getValue())
				if time < 10 then
					val = "0" .. tostring(time)
				else
					val = tostring(time)
				end

				self:setText(val)

				--minN:setPos(minClock:getX() - minN:getWidth() / 2, minClock:getY() - minN:getHeight() / 2)
				think(self)
			end

			local think = hrN.think
			hrN.think = function(self)
				local val = ""
				local time = math.floor(os.date("%H")) -- To display 24 hour time, whilst the dial is repeated by 12
				if time < 10 then
					val = "0" .. tostring(time)
				else
					val = tostring(time)
				end

				self:setText(val)

				--hrN:setPos(hrClock:getX() - hrN:getWidth() / 2, hrClock:getY() - hrN:getHeight() / 2)
				think(self)
			end

			local think = fpsN.think
			fpsN.think = function(self)
				self:setText(tostring(math.floor(fpsInd:getValue())))
				fpsN:setPos(fpsInd:getX() - fpsN:getWidth() / 2, fpsInd:getY() - fpsN:getHeight() / 2)
				think(self)
			end

			---
			--- END OF LABELS
			---

		end
		---
		--- End of FPS & Clock
		---

		---
		--- Ammo Panel - Bottom Right
		---

		ammoPanel = GM.class.getClass("HudPanel"):new(0, 0, 0, 0, Color(20, 20, 20, 150), true)
		ammoPanel:setPadding(10, 10)

		local think = ammoPanel.think
		ammoPanel.think = function(self)
			think(self)

			self:setPos(ScrW() - 30 - self:getWidth(), ScrH() - 30 - self:getHeight())

			local ammo, maxAmmo, _ = calcAmmo()
			if ammo and maxAmmo and maxAmmo > 0 then
				self:setVisible(true)
			else
				self:setVisible(false)
			end

		end

		ammo = GM.class.getClass("HudBarIndicator"):new(0, 0, 120, 10, 0, 100, Color(70, 0, 0, 255), Color(50, 0, 0, 20), Color(70, 0, 0, 255), false)
		ammo_txt = GM.class.getClass("TextElement"):new(ammo:getX(), ammo:getY() + ammo:getHeight(), Color(255, 255, 255, 255), "Ammo")
		ammo:setSmoothFactor(0.3)

		local think = ammo.think
		ammo.think = function(self)
			local ammo, maxAmmo, _ = calcAmmo()

			if ammo and ammo >= 0 and maxAmmo and maxAmmo > 0 then
				self:setMaxValue(maxAmmo)
				self:setTarget(ammo)
			end

			think(self)
		end

		local think = ammo_txt.think
		ammo_txt.think = function(self)
			think(self)

			local ammo, maxAmmo, _ = calcAmmo()
			self:setText( string.format("Ammo: %s/%s", ammo, maxAmmo) )
		end

		alt = GM.class.getClass("TextElement"):new(ammo_txt:getX(), ammo_txt:getY() + ammo_txt:getHeight(), Color(255, 255, 255, 255), "Alt")

		local think = alt.think
		alt.think = function(self)
			think(self)

			local _, _, secondaryAmmo = calcAmmo()
			self:setText(string.format("Alt: %s", secondaryAmmo))
		end

		ammoPanel:addChild(ammo):addChild(ammo_txt):addChild(alt)
		---
		---	End of Ammo Panel
		---

		---
		--- Register components
		---

		GM:registerHUDComponent("StatPanel", statPanel) -- Optional, allows external lua scripts to call up the HUD elements for hooking etc
		GM:registerHUDComponent("AmmoPanel", ammoPanel)

		if drawTimeBar then
			GM:registerHUDComponent("fpsInd", fpsInd)
			GM:registerHUDComponent("secClock", secClock)
			GM:registerHUDComponent("minClock", minClock)
			GM:registerHUDComponent("hrClock", hrClock)
		end

		-- Don't bother with clock as it's not in a frame, and won't be used for anything other than defined here.

		---
		--- End Component Registration
		---

		generated = true
	end
end

function GM:HUDPaint()
	--HUD Invalidation (Update if any numbers change)
	if drawTimeBar ~= (GetConVarNumber("sc_hud_showTimeElement") ~= 0) then
		generated = false
		drawTimeBar = GetConVarNumber("sc_hud_showTimeElement") ~= 0
	end

	genComponents()

	if LocalPlayer():Alive() and GetConVarNumber("cl_drawhud") ~= 0 then

		-----
		-- Player Status Panel
		-----

		-- Temperature Gauge Tomfoolery
		---		Graph assumes min 200K and max 400K and an 80 pixel bar, because a range of 25 degrees of habitability is a bit tiny (283 - 308K)
		surface.SetTexture(0)
		surface.SetDrawColor(Color(0, 196, 255, 255)) --Cold
		surface.DrawRect(lsBar_Temp:getX(), lsBar_Temp:getY(), lsBar_Temp:getWidth(), lsBar_Temp:getHeight())
		surface.SetTexture(0)
		surface.SetDrawColor(Color(255, 0, 0, 255)) --Hot
		surface.DrawRect(lsBar_Temp:getX(), lsBar_Temp:getY(), lsBar_Temp:getWidth(), 36)
		surface.SetTexture(0)
		surface.SetDrawColor(Color(0, 255, 0, 255)) --Goldilocks
		surface.DrawRect(lsBar_Temp:getX(), lsBar_Temp:getY() + 36, lsBar_Temp:getWidth(), 10)
		surface.SetTexture(0)
		surface.SetDrawColor(Color(0, 0, 0, 255)) --Goldilocks Highlight
		surface.DrawOutlinedRect(lsBar_Temp:getX(), lsBar_Temp:getY() + 35, lsBar_Temp:getWidth(), 12)

		-- Main Panel
		statPanel:render()

		--Temperature Indicator

		if LocalPlayer().Atmo_Temp ~= nil then
			local adjustedTemp = math.Clamp(LocalPlayer().Atmo_Temp, 200, 400) - 200
			surface.SetTexture(0)
			surface.SetDrawColor(Color(255, 255, 255, 255))
			surface.DrawRect(lsBar_Temp:getX() - 3, lsBar_Temp:getY() + (79 - (79 * (adjustedTemp / 200))), lsBar_Temp:getWidth() + 6, 2)
		end

		-- AmmoPanel
		ammoPanel:render()

		if drawTimeBar then
			-- Clock [ ORDER IMPORTANT ]
			secClock:render()
			minClock:render()
			hrClock:render()

			-- FPS meter
			fpsInd:render()

			-- Labels [ ORDER NOT IMPORTANT ]
			secLabel:render()
			minLabel:render()
			hrLabel:render()
			fpsLabel:render()

			-- Numbers [ ORDER NOT IMPORTANT ]
			secN:render()
			minN:render()
			hrN:render()
			fpsN:render()
		end

	    self:PaintWorldTips()

	    -- Draw all of the default stuff
	    self.BaseClass.HUDPaint(self)

	    --self:PaintNotes()
	end

end