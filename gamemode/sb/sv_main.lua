local GM = GM
local BaseClass = GM:getBaseClass()
local EnvironmentsInitialized = false

util.AddNetworkString("SpaceCombat2-UpdatePlayerGravity")

include("sv_convar.lua")
include("sh_main.lua")

AddCSLuaFile("sh_main.lua")
AddCSLuaFile("sh_generators.lua")
AddCSLuaFile("cl_convar.lua")
AddCSLuaFile("cl_fonts.lua")
AddCSLuaFile("cl_main.lua")
AddCSLuaFile("cl_hud.lua")

function GM:PlayerNoClip(ply)
    if not IsValid(ply) then return false end
    if ply:InVehicle() then return false end
    local noclip = GetConVar("SB_NoClip"):GetBool()
    if not noclip then
        return false
    end

    if ply.GetEnvironment then
        return ply.GetEnvironment():GetNoclip(ply)
    end

	return GM:getSpace():GetNoclip(ply)
end

---
--- Environment Stuffs
---

---
-- Planet data structure
--
-- Case01 -> type of planet, eg planet/cube/planet_color
-- Case02 -> planet radius
-- Case03 -> planet gravity
-- Case04 -> planet atmosphere
-- Case05 -> planet night temperature
-- Case06 -> planet temperature
-- Case07 -> color_id
-- Case08 -> bloom_id
-- Case16 -> process sb1 flags
---

---
-- Planet2 data structure
--
-- Case01 -> type of planet, eg planet/cube/planet_color
-- Case02 -> planet radius
-- Case03 -> planet gravity
-- Case04 -> planet atmosphere
-- Case05 -> planet pressure (ignore)
-- Case06 -> planet night temperature
-- Case07 -> planet temperature
-- Case08 -> process sb3 flags
-- Case09 -> oxygen percentage
-- Case10 -> co2 percentage
-- Case11 -> nitrogen percentage
-- Case12 -> hydrogen percentage
-- Case13 -> planet name
-- Case14 -> unknown
-- Case15 -> color_id
-- Case16 -> bloom_id
---

---
-- Star data structure
--
-- Case01 -> type of planet, eg planet/cube/planet_color
--
-- radius -> 512
-- gravity -> 0 (so you don't fall when inside of it)
-- nighttemperature -> 10000
-- temperature -> 10000
---

---
-- Star2 data structure
--
-- Case01 -> type of planet, eg planet/cube/planet_color
-- Case02 -> star radius
-- Case03 -> star night temperature
-- Case04 -> unknown
-- Case05 -> star temperature
-- Case06 -> star name
---

local function getKey( key )
	if type(key) ~= "string" then error("Expected String, got",type(key)) return key end
	if string.find(key,"Case") and tonumber( string.sub(key, 5) ) then
		return tonumber( string.sub(key,5) ) or key
	else
		return key
	end
end

local function Extract_Bit(bit, field)
    if not bit or not field then return false end
    if ((field <= 7) and (bit <= 4)) then
        if (field >= 4) then
            field = field - 4
            if (bit == 4) then return true end
        end
        if (field >= 2) then
            field = field - 2
            if (bit == 2) then return true end
        end
        if (field >= 1) then
            if (bit == 1) then return true end
        end
    end
    return false
end

local function spawnEnvironment( v )

	local obj = GM.class.getClass("Celestial"):new()
	local env
	local ent
	local r

	--PrintTable( v )

	local type = string.lower(v[1])

	if type == "planet" or type == "planet2" or type == "cube" then

		if type == "planet2" or type == "cube" then v[6] = v[7] end -- Override night temp as norm temp

		r = tonumber(v[2])
        local vol = (4 / 3) * math.pi * (r ^ 3)
        local restotal = vol * 0.00157
        local Resources = {}

        if type == "planet" then
            local habitat = Extract_Bit(1, tonumber(v[16]))
            Resources["Oxygen"] = (habitat and 21 or 0.1) / 100 * restotal
            Resources["Carbon Dioxide"] = (habitat and 0.45 or 91.5) / 100 * restotal
            Resources["Nitrogen"] = (habitat and 60 or 3.5) / 100 * restotal
            Resources["Hydrogen"] = (habitat and 0.55 or 0) / 100 * restotal
            Resources["Carbon Monoxide"] = 0.045 * restotal
            Resources["Methane"] = 0.035 * restotal
            Resources["Water"] = (habitat and 0.275 or 0.15) * restotal
        else
            Resources["Oxygen"] = (tonumber(v[9]) or 0) / 100 * restotal * 0.845
            Resources["Carbon Dioxide"] = (tonumber(v[10]) or 0) / 100 * restotal * 0.845
            Resources["Nitrogen"] = (tonumber(v[11]) or 0) / 100 * restotal * 0.845
            Resources["Hydrogen"] = (tonumber(v[12]) or 0) / 100 * restotal * 0.845
            -- TODO: Distribute these dynamically based on planet temperatures and other resources
            Resources["Carbon Monoxide"] = 0.045 * restotal
            Resources["Methane"] = 0.035 * restotal
            Resources["Water"] = 0.275 * restotal
        end

        for i,k in pairs(Resources) do
          if k > 0 then
              Resources[i] = GM:NewResource(i, k, restotal)
          else
              Resources[i] = nil
          end
        end

        env = GM.class.getClass("Environment"):new( v[3], v[6], v[4], r, Resources, ((type == "planet2" or type == "cube") and v[13] or "Planet"), (type == "cube") and "cube" or "planet") -- grav, temp, atmos, radius, resources, name, type

	elseif type == "star" then

		r = 512
        local vol = (4 / 3) * math.pi * (r ^ 3)
        local restotal = vol * 0.03
        local Resources = {}
		Resources["Hydrogen"] = GM:NewResource("Hydrogen", restotal, restotal)

		env = GM.class.getClass("Environment"):new( 0, 10000, 0, r, Resources, "Star", "star") -- grav, temp, atmos, radius, resources, name
		env:SetPressure(5000)

	elseif type == "star2" then

		r = tonumber(v[2])

        local vol = (4 / 3) * math.pi * (r ^ 3)
        local restotal = vol * 0.03
        local Resources = {}
		Resources["Hydrogen"] = GM:NewResource("Hydrogen", restotal, restotal)

		env = GM.class.getClass("Environment"):new( 0, v[5], 0, r, Resources, (string.len(v[6] or "") > 0 and v[6] or "Star"), "star")
		env:SetPressure(5000)

	end

	if env and obj and r then
		obj:SetEnvironment(env) -- Bind Environment IMMEDIATELY!

		ent = ents.Create("sc_planet")
		ent:SetPos( v.ent:GetPos())
		ent:SetAngles( v.ent:GetAngles() )
		ent:Spawn()

        ent:SetMoveType( MOVETYPE_NONE ) -- We don't want these planets to move
		ent:SetSolid( SOLID_NONE ) -- We want people to be able to pass through it...

    if type == "cube" then
        ent:PhysicsInitBox(Vector(-r,-r,-r), Vector(r,r,r))
        ent:SetCollisionBounds(Vector(-r,-r,-r), Vector(r,r,r))
    else
        ent:PhysicsInitSphere(r)
        ent:SetCollisionBounds( Vector(-r,-r,-r), Vector(r,r,r) )
    end

    ent:SetTrigger( true )
		ent:DrawShadow( false ) -- That would be bad.

		local phys = ent:GetPhysicsObject()
		if phys:IsValid() then
        phys:EnableMotion(false) -- DON'T MOVE!
        phys:EnableGravity(false)
        phys:EnableDrag(false)
        phys:Wake()
		end

		ent:SetNotSolid(true) -- It's not solid ofc

		obj:setEntity(ent) -- Bind the entity to the celestial
	end
end

-- The direction of the sun, obviously
local SunDir = Vector(0,0,-1)
function GM:GetSunDir()
    return SunDir
end

function GM:UpdateSunDir()
    local Suns = ents.FindByClass("env_sun")
    if #Suns > 0 then
        local EnvSun = Suns[1]
        local Values = EnvSun:GetKeyValues()
        local NewSunDir

        -- Check if Garry has modified the sun
        if Values["sun_dir"] then
            NewSunDir = -Values["sun_dir"]

        -- See if the sun has been setup correctly if we don't have a direction yet
        else
            if Values["use_angles"] and Values["use_angles"] ~= 0 then
                NewSunDir = EnvSun:GetAngles()
                NewSunDir.p = -(Values["pitch"] ~= 0 and Values["pitch"] or NewSunDir.p)
                NewSunDir = NewSunDir:Forward()
            else
                local Target = ents.FindByName("sun_target")
                if #Target > 0 and IsValid(Target[1]) then
                    Target = Target[1]
                    NewSunDir = (Target:GetPos() - EnvSun:GetPos()):Normalize()
                end
            end
        end

        -- Some maps don't setup their suns correctly at all, so we'll just use the angles of the entity by default
        if not NewSunDir then
            NewSunDir = EnvSun:GetAngles()
            NewSunDir.p = -(Values["pitch"] ~= 0 and Values["pitch"] or NewSunDir.p)
            NewSunDir = NewSunDir:Forward()
        end

        SunDir = NewSunDir
    end

	--no sun found, so just set a default angle
	if not SunDir or SunDir == Vector(0,0,0) then SunDir = Vector(0,0,-1) end
end

--[[
--
-- GM:InitPostEntity( )
-- Called once all map entities have spawned.
--
 ]]

local env_data = {}

function GM:CreateMapEnvironments()
    -- Make sure this doesn't get called more than once on a gamemode reload
    if EnvironmentsInitialized then
        return
    end

    --Register sun
    GM:UpdateSunDir()

	--Special Map Cases
	if game.GetMap() == "gm_interplaneteryfunk" or game.GetMap() == "gm_interplanetary" then
		--This map is all one planet, but was made before cube atmos were a thing,
		--ignore all planet cases and make our own.

		--Settings
		local data = {}
		data[1] = "cube"
		data[2] = "15344"
		data[3] = "1"
		data[4] = "1"
		data[5] = "1"
		data[6] = "289"
		data[7] = "289"
		data[9] = "21"
		data[10] = "0.1"
		data[11] = "78"
		data[12] = "0.9"
		data[13] = "Earth"

		--Placeholder Entity for atmo to spawn on
		local ent = ents.Create("prop_physics")
		ent:SetPos(Vector(0, 0, -15618))
		ent:SetModel("models/props_lab/huladoll.mdl")
		ent:Spawn()

		--Spawn Environment
		data["ent"] = ent
		spawnEnvironment(data)

		--Cleanup
		ent:Remove()

	--Normal Maps
	else
        local ents = ents.FindByClass("logic_case")

        for _, ent in pairs(ents) do
            local tbl = { ent = ent }
            local vals = ent:GetKeyValues()
            for k, v in pairs(vals) do
                tbl[ getKey( k ) ] = v
            end
            table.insert(env_data, tbl )
        end

        for k,v in pairs( env_data ) do
            spawnEnvironment( v )
        end
	end

    --We need to rebuild the environment tree after we setup the atmospheres
    GM.EnvironmentTree = {[GM:getSpace()] = {}}

    local environments = {}
    for I=1, #GM.AtmosphereTable do
        table.insert(environments, GM.AtmosphereTable[I])
    end
    for I=1, #GM.StarTable do
        table.insert(environments, GM.StarTable[I])
    end

    --We want the smallest atmospheres to go last, because they're generally going to be inside of the bigger ones
    table.sort(environments, function(a, b) return a:GetRadius() > b:GetRadius() end)

    --Loop over the environments and build the tree
    for i,k in pairs(environments) do
        --We need a position, so we can only do this on environments with a valid celestial entity
        if k:GetCelestial() and IsValid(k:GetCelestial():getEntity()) then
            --Get the atmospheres at the position
            local final, all = GM:GetAtmosphereAtPoint(k:GetCelestial():getEntity():GetPos())
            local children = GM.EnvironmentTree

            --Loop over all of the atmospheres at the point
            for I=1, #all do
                children = children[all[I]]
            end

            --Once we reach the lowest level, add the new children table
            children[k] = {}
        end
    end

    --Temporary fix, this should really be handled in the other loop. Tried putting it in the other loop originally, but it didn't work.
    local function setchildren(parent, children)
        parent:setChildren(children)
        for i,k in pairs(children) do
            i:setParent(parent)
            setchildren(i, k)
        end
    end
    setchildren(GM:getSpace(), GM.EnvironmentTree[GM:getSpace()])

    -- Check if any environments were added, if there aren't any then
    if table.Count(GM.EnvironmentTree[GM:getSpace()]) == 0 then
        GM:getSpace():SetHabitable(true)
    end

    EnvironmentsInitialized = true
end

hook.Remove("InitPostEntity", "SC.LoadMapEnvironments")
hook.Add("InitPostEntity", "SC.LoadMapEnvironments", function()
    GM:CreateMapEnvironments()
end)

hook.Remove("OnReloaded", "SC.ReloadMapEnvironments")
hook.Add("OnReloaded", "SC.ReloadMapEnvironments", function()
    GM:CreateMapEnvironments()
end)

function GM:OnEnterEnvironment(env, ent)
	if env:GetName() ~= "" then
		print(ent, "Entering: ",env:GetName(),"\n")
	end
end

function GM:OnLeaveEnvironment(env, ent)
	if env:GetName() ~= "" then
		print(ent, "Leaving: ",env:GetName(),"\n")
	end
end

local space = GM.class.getClass("Space"):new()
local function spacethink()
    space:Think() -- Because it's not bound to a celestial :D
end
timer.Remove("SC2.SpaceThink")
timer.Create("SC2.SpaceThink", 1, 0, spacethink)

function GM:getSpace()
    return space
end

-- A way to find what environment is at a point that does not rely on touch checks and
-- therefore must loop over the environment tree. Will return the last environment in
-- the tree that has the point, and all atmospheres that contained the point.
function GM:CheckEnvironments(point, tree, atmospheres)
    if not point or not tree or not next(tree) then return end
	for atmo,children in pairs(tree) do
		if atmo:ContainsPosition(point) then
            SC.Error("Environment tree contains point.", 2)

            if atmospheres then table.insert(atmospheres, atmo) end

            local child
            if next(children) ~= nil then
                SC.Error("Environment has children, checking if point is inside of them...", 2)
                child = self:CheckEnvironments(point, children, atmospheres)
                SC.Error("Finished checking children.", 2)
            end

			return child or atmo
		end
	end

    SC.Error("Environment tree does not contain point.", 2)
end

-- A wrapper for GM:CheckEnvironment that goes over all environments on the map and returns all atmospheres that had the point
function GM:GetAtmosphereAtPoint(point)
    local atmospheres = {}
    local ret = self:CheckEnvironments(point, GM.EnvironmentTree, atmospheres)

    if not ret then
        SC.Error("Unable to find environment for position "..tostring(point).."! What happened!?", 5)

        -- TODO: remove debug code once the logic error is fixed
        print(ret)
        PrintTable(atmospheres or {})
    end

	return ret, atmospheres
end

local function SetupEntityEnvironment(e)
    if not IsValid(e) then return end

    if not EnvironmentsInitialized then
        timer.Simple(1, function()
            SetupEntityEnvironment(e)
        end)

        return
    end

	if not e.GetEnvironment then
		--Planets should be in their own atmospheres, so ignore them.
		if e:GetClass() ~= "sc_planet" then
			--This hook is called just after ents.Create, usually before ent:SetPos, so wait.
			timer.Simple(0.1, function()
				if not IsValid(e) then return end
				local atmo = GM:GetAtmosphereAtPoint(e:GetPos())

                --Check if this is valid, it CAN be invalid at the start of a map before things are fully initialized
                if atmo then
				    atmo:SetEnvironment(e, atmo)
                else
                    GM:getSpace():SetEnvironment(e, GM:getSpace())
                end
			end)
		end
	end
end

function GM:OnEntityCreated(e)
    SetupEntityEnvironment(e)
end

function GM:SetPlayerSpeed(ply, walk, run, override) --Adding the override parameter to prevent fighting over wtf the speed is, things outside of the gamemode should set this to true to enable their override and false to disable it
    if not ply.speedoverride or type(override) == "boolean" then
        ply.speedoverride = override
        BaseClass.SetPlayerSpeed(self,ply,walk,run)
    end
end

-- These functions are used by environments to figure out what speed players should move at
function GM:SetPlayerSpeedModifier(ply, walk, run)
    if IsValid(ply) and walk ~= nil and run ~= nil then
        ply.walkmodifier = walk
        ply.runmodifier = run
    end
end

function GM:GetPlayerSpeedModifier(ply)
    return ply.walkmodifier or 1, ply.runmodifier or 1
end

-- Synchronize gravity to the client when it changes
function GM:UpdatePlayerGravity(ply, gravity)
    if IsValid(ply) and gravity ~= nil then
        ply:SetGravity(gravity)
        net.Start("SpaceCombat2-UpdatePlayerGravity")
        net.WriteFloat(gravity)
        net.Send(ply)
    end
end

--Effect code pulled from Space Combat 2 for usage in SB4
function GM:CreateEffect(name, tab)
	SC.CreateEffect(name, tab)
end