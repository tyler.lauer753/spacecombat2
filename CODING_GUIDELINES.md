# Coding Guidelines

- `PascalCase`, never `camelCase`
- 4 spaces should be used for indenting, not tabs.
- End of line should should be Unix style (`\n`), not Windows style (`\r\n`)
- Use standard Lua operators, not C-style GLua operators

Example:

```c++
    GLua (Bad):
    // Do Something
    while (!Done) do
        /*
         * Some Big Comment
         */
        if (SomeThing != OtherThing) then
            DoSomeOtherThing();
            continue;
        end

        DoThing();
    end
```

```Lua
    Lua (Good):
    -- Do Something
    while not Done do
        --[[
            Some Big Comment
        ]]
        if SomeThing ~= OtherThing then
            DoSomeOtherThing()
        else
            DoThing()
        end
    end
```

- Avoid using parentheses unless required
  - Exceptions may be made if it significantly improves readability
- Function calls have no space before the parentheses
- No spaces are left inside the parentheses
- A space after each comma, but without space before
- All operators must have one space before and one after, with the exception of the concatenation operator `..`.
- The concatenation operator should have no spaces before or after it
- There should not be more than one contiguous blank line
- There should be no empty comments
