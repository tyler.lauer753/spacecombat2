--Override limits
--[[----------------
	Put specific classes in then FilterOverrides table, it will let you override limits set in other places.
]]------------------
local FilterOverrides = {}

--Per class overrides
FilterOverrides["ship_core"] = 10
FilterOverrides["sc_module"] = 30
FilterOverrides["mining_drill_rig"] = 2
FilterOverrides["phys_constraint"] = 4096

--We have a limiter in here that does a much better job than this function, so let's overwrite it.
local pmeta = FindMetaTable("Player")
pmeta.CheckLimit = function(str) return true end
pmeta.AddCount = function(str, ent) return end

--this is a duplicate of player.AddCount(), but is not prone to meta-related errors
function IncCount(ply, str, ent)
	local key = ply:SteamID64()
	g_SBoxObjects[ key ] = g_SBoxObjects[ key ] or {}
	g_SBoxObjects[ key ][ str ] = g_SBoxObjects[ key ][ str ] or {}

	local tab = g_SBoxObjects[ key ][ str ]

	table.insert(tab, ent)

	-- Update count (for client)
	ply:GetCount(str)

	ent:CallOnRemove("GetCountUpdate", function(ent, ply, str) ply:GetCount(str, 1) end, ply, str)
end

--This function should be able to handle just about everything that's thrown at it.
function SC.Filter(ent)

	if not IsValid(ent) then return false end

	local cls = ent:GetClass()
	if ent.warhead or ent:IsWeapon() or ent:IsPlayer() then return false end
	if string.Right(cls, 6) == "_brush" or string.Left(cls, 5) == "func_" or string.Left(cls, 8) == "trigger_" then return false end

	local ename = ent:GetName()
	if not ename or ename == "" then ename = ent.PrintName end
	if not ename or ename == "" then ename = cls end

	local founder = NULL
	local cv = cls

	--Check for a limit override
	if table.HasValue(table.GetKeys(FilterOverrides), cls) then
		cv = cls
		CreateConVar("sbox_max"..cv, FilterOverrides[cls], {FCVAR_NOTIFY, FCVAR_ARCHIVE})
		ename = cls

	--class name is not the only one. gmod uses a different set (custom names, ugh)
	elseif string.Left(cls, 5) == "gmod_" then
		--remove gmod_ and remove 's' at the end of the string if it's there
		cv = string.gsub(cls, "gmod_", "")
		if not ConVarExists("sbox_max"..cv) then
			--not all the default convars are plural.
			if string.Right(cv, 1) ~= "s" then
				cv = cv.."s"
			end
		end
		ename = cv
	elseif string.Left(cls, 12) == "prop_physics" then
		--this also counts parented props
		cv = "props"
		ename = cv
	elseif string.Left(cls, 9) == "sc_weapon" then
		--Special things for weapons
		cv = "sc_weapon"
		ename = cv
    elseif string.Left( cls, 4 ) == "npc_" then
        cv = "npcs"
		ename = cv
    elseif string.Left(cls, 5) == "item_" then
        cv = "items"
		ename = cv
	end

	--sadly, some addons don't use sbox_max or any such system at all.
	if ConVarExists("sbox_max"..cv) then
		if CPPI and ent.CPPIGetOwner then
			founder = ent:CPPIGetOwner()
		end

		if not IsValid(founder) then
			--this is the system that garrysmod uses by default, so let's just dig into it a little
			if ent.GetPlayer then founder = ent:GetPlayer() end
			if not IsValid(founder) then founder = ent.Owner end --McBuild ownership
			if not IsValid(founder) then founder = ent.SPL end --SBEP's ownership
			if not IsValid(founder) and ent.GetOwner then founder = ent:GetOwner() end --Sometimes
		end

		--Only works if the player is still on the server. If they've disconnected immediately after spawning something, this won't work.
		if founder and founder:IsPlayer() then
			--just do a little check to see if the player isn't trying to spawn something they're not allowed to.
			if not game.SinglePlayer() then
				if ent.AdminOnly or (ent.Spawnable == false and ent.AdminSpawnable == true) then
					--note: if both spawnables are false, that may mean the entity is just created by an stool and therefore still legitimate. however, alot of illegitimate entities do not have spawnable settings... ... not sure how to check for these, yet.
					if not founder:IsAdmin() and not founder:IsSuperAdmin() then
						ent:Remove()
						founder:PrintMessage(HUD_PRINTCENTER, "You are not an Admin, you may not spawn this ["..ename.."] !")
						return false
					end
				end

				-- Check for the old rank system and throw a depreciation message
				-- TODO: Remove this once all entities are converted
				if ent.RequiredRank then
					local Message = "Entity ["..ename.."] is using depreciated permission checks, please report this!"
					founder:PrintMessage(HUD_PRINTCENTER, Message)
					ErrorNoHalt(Message.."\n")
				end

				-- Remove the entity if the user doesn't have the required permissions
				if ent.RequiredPermissions and founder.HasPermissions and not founder:HasPermissions(ent.RequiredPermissions) then
					founder:PrintMessage(HUD_PRINTCENTER, "Access to ["..ename.."] is restricted!")
					ent:Remove()
					return false
				end
			end

			--plus the 1 we're spawning right now
			if (founder:GetCount(cls) + 1) > GetConVar("sbox_max"..cv):GetInt() then
				ent:Remove()
				founder:PrintMessage(HUD_PRINTCENTER, "Limit reached for ["..ename.."] !")
				return false
			end

			IncCount(founder,cls,ent) --tell gmod's lua that this player has spawned something and now owns it
		else
			if founder == NULL and founder ~= game.GetWorld() then
				SC.Error("Limiter: Unable to derive ownership for <"..tostring(ent).." ["..ename.."]>.\n", 0)
				return false
			end
		end
	else
		SC.Error("Limiter: <"..tostring(ent).." ["..ename.."]> was missing an associated sbox_max convar.\n", 1)
		-- So... create one here then ?
		-- maybe base limit on init's file size?
		CreateConVar("sbox_max"..cv, 20, {FCVAR_NOTIFY, FCVAR_ARCHIVE})
		SC.Error("Limiter: Created ConVar[".."sbox_max"..cv.."] with a limit of 20.\n", 1)
		SC.Filter(ent) --check filter again?
		return false
	end

	--this must come LAST. otherwise, ship weapon launchers will break.
    if ent.SC_Immune or ent.SC_Health or ent.CDSIgnore then	return false end

	SC.Spawned(ent)
end

local function EntCreated(ent)
	timer.Simple(0.25, function() SC.Filter(ent) end)
end
hook.Add("OnEntityCreated", "SC.EntCreated", EntCreated)