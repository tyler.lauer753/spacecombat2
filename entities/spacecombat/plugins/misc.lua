AddCSLuaFile()

-- Utility/Other functions
function SC.ValidVector( v ) -- Just a little utilitarian function.
	return v ~= nil and v ~= vector_origin
end

--Bitwise flag functions
function SC.TestFlag(set, flag)
  return set % (2*flag) >= flag
end

function SC.SetFlag(set, flag)
  if set % (2*flag) >= flag then
    return set
  end
  return set + flag
end

function SC.ClearFlag(set, flag)
  if set % (2*flag) >= flag then
    return set - flag
  end
  return set
end

--SC.MakeEnt is basically a copy of WireLib.MakeWireEnt with our ownership stuff and without limit checks
function SC.MakeEnt( pl, Data, ... )
	Data.Class = scripted_ents.Get(Data.Class).ClassName

	local ent = ents.Create( Data.Class )
	if not IsValid(ent) then return false end

	duplicator.DoGeneric( ent, Data )
	ent:Spawn()
	ent:Activate()
	duplicator.DoGenericPhysics( ent, pl, Data ) -- Is deprecated, but is the only way to access duplicator.EntityPhysics.Load (its local)

	ent.Owner = pl
	if ent.Setup then ent:Setup(...) end

	local phys = ent:GetPhysicsObject()
	if IsValid(phys) then
		if Data.frozen then phys:EnableMotion(false) end
		if Data.nocollide then phys:EnableCollisions(false) end
	end

	return ent
end

--This is a copy of the WireLib.ClassAlias function, I needed it for something and I don't want to rely on wiremod for everything incase it breaks.
function SC.ClassAlias(realclass, alias)
	scripted_ents.Alias(alias, realclass)
	-- Hack for Advdupe2, since scripted_ents.GetList() does not respect aliases
	hook.Add("Initialize", "Rename_"..alias, function()
		local tab = scripted_ents.GetStored(realclass).t -- Grab the registered entity
		scripted_ents.Register(tab, alias) -- Set alias to be defined as this ENT
		tab.ClassName = realclass -- scripted_ents.Register changes this to your argument, lets change it back
	end)
end

-- Messages that can easily be disabled.
function SC.Print(mess, level)
    level = level or 0
	if SC.Debug and SC.DebugLevel <= level then
		print(mess)
	end
	return
end

function SC.Msg(mess, level)
    level = level or 0
	if SC.Debug and SC.DebugLevel <= level then
		Msg(mess.."\n")
	end
	return
end

function SC.Error(mess, level)
    level = level or 0
	if SC.Debug and SC.DebugLevel <= level then
		ErrorNoHalt(mess.."\n")
	end
	return
end

function SC.GetCvarBool(cvar) --Mainly just to save space
	return GetConVar(cvar):GetBool()
end

--####  Math Stuff  ####--
function cubic_curve( a, b, c, d, X )
	return( (a*(X^3)) + (b*(X^2)) + (c*(X)) + d )
end

function SC.StringToVector(str)
	str = string.Trim(str) --Weird shit happens if you don't trim strings -Lt.Brandon
	local vecDims = string.Explode(",", str)
	local vector = Vector(tonumber(vecDims[1]), tonumber(vecDims[2]), tonumber(vecDims[3]))

	return vector
end

--Literally just string.Explode with a call to string.Trim for sanity's sake
function SC.StringToTable(str)
	str = string.Trim(str)
	local outTable = string.Explode(",", str)

	return outTable
end

hook.Add("OnToolCreated", "AddSC2LSGenerators", function(tool, tab)
    if tool ~= "sb4_generators" then return end

    local new = {}
    new[2] = {Name = "Resource Scanner", Model = "models/cerus/modbridge/misc/accessories/acc_radar1.mdl", EntityClass = "mining_scanner", EntityDescription = "A fancy radar that retrieves information about the resources inside of crystals and asteroids."}
end)

function AuthGarbageCollector( ply, sid, uid )
	collectgarbage("collect")
end
hook.Add( "PlayerAuthed", "AuthGarbageCollector", AuthGarbageCollector)

--This is a garbage collector. Nothing cleans this table, and it's important, and can get really huge. ~Dubby
NextSBObjCleaning = CurTime() + 300
hook.Add("Think", "g_SBoxObjects Cleaner", function()
	if CurTime() > NextSBObjCleaning then
		NextSBObjCleaning = CurTime() + 300
		local count = 0
		for uid, tbl in pairs( g_SBoxObjects ) do
			for class, enttbl in pairs( tbl ) do
				for id, ent in pairs( enttbl ) do
					if ent == NULL then g_SBoxObjects[uid][class][id] = nil count = count + 1 end
				end
			end
		end
		if count > 0 then
			ErrorNoHalt( "Limiter: Found and removed "..tostring(count).." invalid object(s) from the player module's table.\n" )
		end
	end
end)