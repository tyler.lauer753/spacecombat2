Spawners contain the behavior code for a feature. They are passed a
feature's data table whenever anything but "Setup" is called, and
therefore you may control all of the features with that Spawner type
on the map with one instance.

A Spawner requires the following functions to not cause errors:

SPAWNER:Setup()						--Called when the Spawner loads (only during Mining Plugin Enable).

SPAWNER:Initialize(feature)			--Called when a new feature is created. Do initial seeding here.

SPAWNER:LoadFromFile(name, data)	--Called when a Map Features file contains this type. Data contains the
									--INI flags for this feature. Initialize is called after this
									--automatically. Return Feature table when successful, nil otherwise.

SPAWNER:Cleanup(feature)			--Called when a feature gets removed for whatever reason.

SPAWNER:Think(feature)				--Called whenever a feature is set to "think" next, usually when it's
									--next supposed to spawn something.