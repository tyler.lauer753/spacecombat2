--[[
Space Combat Storage and Resourcing - Created by Lt.Brandon

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

local Entity = FindMetaTable("Entity")

function Entity:SetupSCStorage(Resources, Minable, Salvagable)
	--Setup the resources table
	self.Resources = Resources or {}

	--Allow resources to be taken and added
	function self:SupplySCResources(items)
		--Loop through all of the stuff and add items where possible
		for res, amount in pairs(items) do
			if self.Resources[res] then
				--Its in the table, lets add whatever we can.
				self.Resources[res] = self.Resources[res] + amount
			else
				--Make a new entry in the table.
				self.Resources[res] = amount
			end
		end
	end

	function self:GetSCResource(name)
		if not name or not self.Resources[name] then return 0 end

		return self.Resources[name]
	end

	function self:ConsumeSCResources(items, force)
		--Copy the hold table, lets us do this without utterly breaking everything.
		local hold = table.Copy(self.Resources)

		--Loop through all of the stuff and nuke items where possible
		for res, amount in pairs(items) do
			--Can we kill things?
			if hold[res] and ((hold[res] - amount >= 0) or force) then
				--Its in the table, lets kill shit!
				hold[res] = math.Max(hold[res] - amount, 0)
			else
				--Item didn't exist or there wasn't enough, evacuate area!
				return false
			end
		end

		self.Resources = hold
		return true
	end

	--Are we a minable object?
	if Minable then
		--Don't override any default function
		if self.MiningSequence == nil then
			--Mining Function, returns table of mined elements
			function self:MiningSequence(damage)
				--Convert damage types into mining effectiveness
				local resourceCount = 0
                if type(damage) == "table" then
                    resourceCount = (damage.RoidMiner + damage.MineralMiner)
                else
                    resourceCount = damage
                end

				local Receive = {}
				for i,k in pairs(self.Resources) do
					--Should we even take this resource?
					math.randomseed(SysTime() + math.random(-10000, 1000))
					local rand = math.random(0, 100)

					if rand > 60 then
					    --Generate a random number for each resource
					    Receive[i] = math.Round(math.min(k, resourceCount * math.random(0.7, 1.2)))

					    --Tick down internal resources
					    k = k - Receive[i]

					    self.Resources[i] = k
					    if self.Resources[i] <= 0 then self.Resources[i] = nil end
                    end
				end

				--Call hook if exists
				if self.OnMined ~= nil then
					self:OnMined(Receive)
				end

				return Receive
			end
		end
	end

	--Are we a salvagable object?
	if Salvagable then
		--Salvaging Function, returns table of salvaged elements
		function self:Salvage(Multiplier)
			local Receive = {}

			--Seed the random generator
			math.randomseed(os.time() + math.random(-1000, 1000))

			--Should they get salvage this time around?
			local can = (math.floor(math.random(0,(10/(Multiplier or 1)))) == 1)

			if can then
				for i,k in pairs(self.Resources) do
					if math.random(1, #self.Resources) == 1 then
						--How much should we give them?
						local amount = math.ceil(k * math.random(0.02, 0.1))

						--Hand over the salvage
						Receive[i] = amount

						--Remove resources from the wreck
						k = k - amount
						if k <= 0 then k = nil end

						self.Resources[i] = k
					end
				end
			end

			return Receive
		end
	end
end