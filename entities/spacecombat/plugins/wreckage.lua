--[[
Space Combat Ship Wrecks - Created by Lt.Brandon and Steeveeo

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

--Convars
CreateConVar( "sc_wreckage_enable", 1, {FCVAR_NOTIFY, FCVAR_ARCHIVE} ) --Enable the spawning of wreckage
CreateConVar( "sc_wreckage_simple", 0, {FCVAR_NOTIFY, FCVAR_ARCHIVE} ) --Use old spawning method of spawning a single container of resources
CreateConVar( "sc_wreckage_in_atmo", 0, {FCVAR_NOTIFY, FCVAR_ARCHIVE} ) --Allow spawning of wreckage debris inside an atmosphere
CreateConVar( "sc_wreckage_collision_enable", 0, {FCVAR_NOTIFY, FCVAR_ARCHIVE} ) --Set if debris can be ran into
CreateConVar( "sc_wreckage_debris_max", 15, {FCVAR_NOTIFY, FCVAR_ARCHIVE} ) --Maximum number of parts that can be spawned as wreckage when a ship explodes
CreateConVar( "sc_wreckage_size_min", 500, {FCVAR_NOTIFY, FCVAR_ARCHIVE} ) --Minimum prop size that can be made into debris

SC.Salvaging = {}
SC.Salvaging.Drops = {
	"Electronic Circuitry",
	"Scrap Metal",
	"Contaminated Coolant",
	"Radioactive Materials",
	"Nubium Generator Core",
	"Damaged Shield Emitter",
	"Plasma Conduits",
	"Destroyed Consoles"
}
SC.Salvaging.Models = {
	"models/mandrac/energy_cell/large_cell.mdl",
	"models/mandrac/energy_cell/medium_cell.mdl",
	"models/mandrac/ore_container/ore_medium.mdl",
	"models/mandrac/ore_container/ore_large.mdl",
	"models/mandrac/water_storage/water_storage_large.mdl",
	"models/mandrac/water_storage/water_storage_small.mdl"
}

--This function actually spawns a wreck
local function SpawnWreck(core, replace, resources)
    --Spawn the wreck, set its position and angles
	local wreck = ents.Create("sc_wreck")

    if replace ~= nil and IsValid(replace) then
        wreck:SetPos(replace:GetPos())
        wreck:SetModel(replace:GetModel())
		wreck:SetSkin(replace:GetSkin())
		wreck:SetMaterial(replace:GetMaterial())

		--Randomize angle slightly
        wreck:SetAngles(replace:GetAngles() + Angle(math.Rand(-15, 15), math.Rand(-15, 15), math.Rand(-15, 15)))

		--Darken color to simulate scorching
		local col = replace:GetColor()
		wreck:SetColor(Color(col.r / 1.5, col.g / 1.5, col.b / 1.5, 255))
    else
	    wreck:SetPos(core:GetPos())
	    wreck:SetAngles(core:GetAngles())
	    wreck:SetModel(table.Random(SC.Salvaging.Models))
    end

	wreck:Spawn()
	wreck:Activate()

	--Toggle physics
	local canCollide = GetConVarNumber("sc_wreckage_collision_enable")
	local physObj = wreck:GetPhysicsObject()
	if IsValid(physObj) then
		if canCollide == 0 then
			physObj:EnableCollisions(false)
		end
	end

	--Setup the wreck for salvaging
	wreck:SetupSCStorage(resources, false, true)

	--The end
	return wreck
end

--This is used to split the resources across multiple wrecks, replace with something more advanced later on to distribute it based on prop volume
local function DivideTable(tbl, divisor)
    local ret = {}
    for i,k in pairs(tbl) do
        ret[i] = k / divisor
    end
    return ret
end

--Create Wreck function, for all your shipwreck needs!
function SC.CreateWreck(core, multiplier, tbl)
	--Only run if wreckages are enabled
	if GetConVarNumber("sc_wreckage_enable") == 0 then return end

	--Is this even a valid entity? Can't use normal IsValid because fucking garry, but this should be close enough.
	if core == nil then return NULL end

    --This is used to control the total amount of resources, bigger ships should have more loot and smaller ones should have less.
    local multiplier = multiplier or 1

	--Which spawning method to use?
	if GetConVarNumber("sc_wreckage_simple") ~= 0 then
		--What resources should be left over from the explosion?
		local Resources = {}
		local hold = core:GetSCResources()
		if table.Count(hold) > 0 then
			for i,k in pairs(hold) do
				local amount = k:getAmount()
				if amount > 5000 and (amount > 60000 or math.floor(math.random(0,5)) == 1) then
					Resources[i] = math.floor(math.Clamp(math.random(1000, amount)/math.random(1, 10), 1000, amount))
				end
			end
		end

		--Add a random assortment of salvaging drops
		for i,k in pairs(SC.Salvaging.Drops) do
			if (math.floor(math.random(0,5)) == 1) then
				Resources[k] = math.ceil(math.random(10, 500)/math.random(1, 10)) * multiplier
			end
		end

		if tbl then
			local res = DivideTable(Resources, table.Count(tbl))
			for i,k in pairs(tbl) do
				SpawnWreck(core, k, res)
			end
		else
			SpawnWreck(core, nil, Resources)
		end


	--Complex Wreckage Spawning
	else
		local minSize = GetConVarNumber("sc_wreckage_size_min")
		local maxBits = GetConVarNumber("sc_wreckage_debris_max")
		local collisions = GetConVarNumber("sc_wreckage_collision_enable")
		local inAtmo = GetConVarNumber("sc_wreckage_in_atmo")

		minSize = minSize * minSize

		--Tally up all the bits that fit all requirements
		local availableBits = {}
		local holds = {}
		for _, part in pairs(core.ConWeldTable) do
			if IsValid(part) then
				--Props are standard wreckage with standard loot table
				if part:GetClass() == "prop_physics" then
					if part:GetColor().a > 200 then
						--Check environment
						if inAtmo ~= 0 or (not part.GetEnvironment and true or part:GetEnvironment() == GAMEMODE:getSpace()) then
							--Check size
							local size = part:OBBMins():DistToSqr(part:OBBMaxs())
							if size >= minSize then
								table.insert(availableBits, part)
							end
						end
					end

				--This is where the stored shit is held
				elseif part:GetClass() == "sc_storage" and part:GetStorageType() == "Cargo" then
					--Check environment
					if inAtmo ~= 0 or (not part.GetEnvironment and true or part:GetEnvironment() == GAMEMODE:getSpace()) then
						table.insert(holds, part)
					end
				end
			end
		end

		--Select which bits to become actual bits (reserve a slot for a ship hold)
		local debris = {}
		for i = 2, math.min(maxBits, #availableBits) do
			local bit, key = table.Random(availableBits)
			table.insert(debris, bit)

			table.remove(availableBits, key)
		end

		--Make the actual bits
		for _, bit in pairs(debris) do
			local Resources = {}

			--Base resources
			Resources["Iron"] = math.ceil(math.Rand(0.1, 3) * bit:BoundingRadius())
			Resources["Tritanium"] = math.ceil(math.Rand(0.5, 10) * bit:BoundingRadius())

			--Add a random assortment of salvaging drops
			for i,k in pairs(SC.Salvaging.Drops) do

				if (math.floor(math.random(0,5)) == 1) then
					Resources[k] = math.ceil(((math.random(1, 750)/GAMEMODE:NewResource(k,0,0):GetSize()) * multiplier * math.random(0.5,2)) / #debris)
				end
			end

			SpawnWreck(core, bit, Resources)
		end

		--Spawn the loot crate
		if #holds > 0 then
			local Resources = {}
			local hold = core:GetStorageOfType("Cargo"):GetStored()
			if table.Count(hold) > 0 then
				for i,k in pairs(hold) do
					local amount = k:GetAmount()

                    if amount > 1 then
					    Resources[i] = math.floor(math.random(1, amount))
                    end
				end
			end
			SpawnWreck(core, table.Random(holds), Resources)
		end
	end
end