--[[
Space Combat Constraint Library - Created by Lt.Brandon

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

--We need to wait a bit to do this, because garry's mod doesn't load the gamemode right away -Lt.Brandon
timer.Simple(1, function()
	function GAMEMODE:OnEntityConstrained(ent1, ent2, type)
		if IsValid(ent1) and ent1.OnConstrained then
			ent1.OnConstrained(ent1, ent2, type)
		end

		if IsValid(ent2) and ent2.OnConstrained then
			ent2.OnConstrained(ent2, ent1, type)
		end
	end

	--Constraint library overrides to make this stuff actually get called
	local oldconstraint = table.Copy(constraint)

	function constraint.Weld(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "weld")

		return oldconstraint.Weld(ent1, ent2, ...)
	end

	function constraint.NoCollide(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "nocollide")

		return oldconstraint.NoCollide(ent1, ent2, ...)
	end

	function constraint.Axis(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "axis")

		return oldconstraint.Axis(ent1, ent2, ...)
	end

	function constraint.Ballsocket(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "ballsocket")

		return oldconstraint.Ballsocket(ent1, ent2, ...)
	end

	function constraint.AdvBallsocket(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "advballsocket")

		return oldconstraint.AdvBallsocket(ent1, ent2, ...)
	end

	function constraint.Motor(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "motor")

		return oldconstraint.Motor(ent1, ent2, ...)
	end

	function constraint.Hydraulic(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "hydraulic")

		return oldconstraint.Hydraulic(ent1, ent2, ...)
	end

	function constraint.Muscle(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "muscle")

		return oldconstraint.Muscle(ent1, ent2, ...)
	end

	function constraint.Pulley(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "pulley")

		return oldconstraint.Pulley(ent1, ent2, ...)
	end

	function constraint.Rope(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "rope")

		return oldconstraint.Rope(ent1, ent2, ...)
	end

	function constraint.Slider(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "slider")

		return oldconstraint.Slider(ent1, ent2, ...)
	end

	function constraint.Winch(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "winch")

		return oldconstraint.Winch(ent1, ent2, ...)
	end

	function constraint.Elastic(ent1, ent2, ...)
		hook.Run("OnEntityConstrained", ent1, ent2, "elastic")

		return oldconstraint.Elastic(ent1, ent2, ...)
	end

	function GAMEMODE:OnEntityUnconstrained(ent1, ent2, type)
		if IsValid(ent1) and ent1.OnUnconstrained then
			ent1.OnUnconstrained(ent1, ent2, type)
		end

		if IsValid(ent2) and ent2.OnUnconstrained then
			ent2.OnUnconstrained(ent2, ent1, type)
		end
	end

	local function SetPhysicsCollisions( Ent, b )
		if (not Ent or not Ent:IsValid() or not Ent:GetPhysicsObject()) then return end

		Ent:GetPhysicsObject():EnableCollisions( b )
	end

	function constraint.RemoveConstraints( Ent, Type )
		if ( not Ent:GetTable().Constraints ) then return end

		local c = Ent:GetTable().Constraints
		i = 0

		for k, v in pairs( c ) do
			if ( not v:IsValid() ) then
				c[ k ] = nil
			elseif (  v:GetTable().Type == Type ) then
				local ent1, ent2 = v:GetTable().Ent1, v:GetTable().Ent2

				-- Make sure physics collisions are on!
				-- If we don't the unconstrained objects will fall through the world forever.
				SetPhysicsCollisions( ent1, true )
				SetPhysicsCollisions( ent2, true )

				hook.Run("OnEntityUnconstrained", ent1, ent2, string.lower(Type))

				c[ k ] = nil
				v:Remove()

				i = i + 1
			end
		end

		if (table.Count(c) == 0) then
			-- Update the network var and clear the constraints table.
			Ent:IsConstrained()
		end

		local  bool = i ~= 0
		return bool, i
	end

	function constraint.RemoveAll( Ent )
		if ( not Ent:GetTable().Constraints ) then return end

		local c = Ent:GetTable().Constraints
		local i = 0
		for k, v in pairs( c ) do
			if ( v:IsValid() ) then
				local ent1, ent2 = v:GetTable().Ent1, v:GetTable().Ent2

				-- Make sure physics collisions are on!
				-- If we don't the unconstrained objects will fall through the world forever.
				SetPhysicsCollisions( ent1, true )
				SetPhysicsCollisions( ent2, true )

				hook.Run("OnEntityUnconstrained", ent1, ent2, string.lower(v.Type))

				v:Remove()
				i = i + 1
			end
		end

		-- Update the network var and clear the constraints table.
		Ent:IsConstrained()

		local  bool = i ~= 0
		return bool, i
	end
	constraint.RemoveAll_NoHook = oldconstraint.RemoveAll

	--Extra constraint commands
	function constraint.GetAllWeldedEntities( ent, ResultTable ) --Modded constraint.GetAllConstrainedEntities to find only welded ents

		local ResultTable = ResultTable or {}

		if ( not ent ) then return end
		if ( not ent:IsValid() ) then return end
		if ( ResultTable[ ent ] ) then return end

		ResultTable[ ent ] = ent

		local ConTable = constraint.GetTable( ent )

		for k, con in ipairs( ConTable ) do

			for EntNum, Ent in pairs( con.Entity ) do
				if (con.Type == "Weld") or (con.Type == "Axis") or (con.Type == "Ballsocket") or (con.Type == "Hydraulic") then
					constraint.GetAllWeldedEntities( Ent.Entity, ResultTable )
				end
			end

		end

		return ResultTable

	end

	function constraint.GetAllConstrainedEntities_B( ent, ResultTable ) --Modded to filter out grabbers

		local ResultTable = ResultTable or {}

		if ( not ent ) then return end
		if ( not ent:IsValid() ) then return end
		if ( ResultTable[ ent ] ) then return end

		ResultTable[ ent ] = ent

		local ConTable = constraint.GetTable( ent )

		for k, con in ipairs( ConTable ) do

			for EntNum, Ent in pairs( con.Entity ) do
				if con.Type ~= ""  then
					constraint.GetAllWeldedEntities( Ent.Entity, ResultTable )
				end
			end

		end

		return ResultTable

	end
end)