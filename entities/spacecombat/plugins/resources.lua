--[[
Space Combat Resources - Created by Lt.Brandon

TODO: Move this file out of plugins, it's not something that should be disabled!
]]--

AddCSLuaFile()

--Global table
SC.Resources = {}
SC.Resources.Resources = {} --Best name, really.

--Global functions
function SC.Resources:Register(name)
    if self.Resources[name] ~= nil then return end

    --GAMEMODE:AddResourceType(name, 1, "Cargo") --I am unsure as to what all is using this file at the moment, so we'll just take this away as sh_main.lua handles all the resources now. -Steeveeo
    self.Resources[name] = true
end

function SC.Resources:RegisterTable(tbl)
    for i,k in pairs(tbl) do
        self:Register(k)
    end
end

function SC.Resources:IsSCResource(name)
    if self.Resources[name] then return true end
    return false
end

function SC.Resources:GetSCResources()
    return table.Copy(self.Resources)
end


-- Not only is this not where this should be, this entire file is deprecated.
-- I've moved it here temporarily due to the fact that its using the functions here, and we need this in a shared file.
-- I did not want to make the entire mining plugin shared because that is a really huge file to make shared just for some resource definitions.
--Register resources
hook.Add("InitPostEntity", "SCResourcesInit", function()
    local MiningOres = {"Veldspar", "Iron", "Ice", "Tritanium", "Titanite", "Triidium", "Trinium", "Tronium", "Polonium Nitrate", "Tylium", "Nubium", "Solid Nubium", "Anubium", "Vibranium", "Veridium", "Voltarium"}
    for i,k in pairs(MiningOres) do
        SC.Resources:Register(k)
    end
end)

-- This is from wreckage.lua for the same reasons as above. Move back after proper runtime resource support is added.
--[[Register SC2 resources and recipes
hook.Add("InitPostEntity", "SCSalvageInit", function()
    GAMEMODE:AddResourceType("Electronic Circuitry", 3, "Cargo")
    GAMEMODE:AddResourceType("Scrap Metal", 1, "Cargo")
    GAMEMODE:AddResourceType("Contaminated Coolant", 0.1, "Cargo")
    GAMEMODE:AddResourceType("Radioactive Materials", 0.5, "Cargo")
    GAMEMODE:AddResourceType("Nubium Generator Core", 15, "Cargo")
    GAMEMODE:AddResourceType("Damaged Shield Emitter", 7, "Cargo")
    GAMEMODE:AddResourceType("Plasma Conduits", 2, "Cargo")
    GAMEMODE:AddResourceType("Destroyed Consoles", 5, "Cargo")
end)
]]--