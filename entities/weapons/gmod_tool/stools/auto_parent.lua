TOOL.Category		= "Constraints"
TOOL.Name		= "Auto-Parenter"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.ClientConVar["hullweight"] = 500
TOOL.ClientConVar["vehweight"] = 5000
TOOL.ClientConVar["parweight"] = 50000
TOOL.Information = {
	"left",
	"reload"
}

if CLIENT then
    language.Add( "tool.auto_parent.name", "Auto-Parenter Tool" )
    language.Add( "tool.auto_parent.desc", "Just click on the largest prop in the ship!" )
    language.Add( "tool.auto_parent.left", "Parents the Contraption to the selected Prop.")
    language.Add( "tool.auto_parent.reload", "Reset the tool to its default settings.")
end

-- Constraint types to remove
local ConstraintTypes = {
    "Weld",
    "Elastic",
    "Axis",
    "Rope",
    "Slider",
    "Winch",
    "Hydraulic",
    "Keepupright",
    "Motor",
    "Muscle",
    "Pulley",
    "Ballsocket",
    "AdvBallsocket"
}

-- Entities can have a table on them with the following format:
--  ENTITY.SCAutoParenter = {
--      ["DisableConstraintRemoval"] = false,
--      ["IgnoreSpecialConstraints"] = false,
--      ["IgnoredConstraints"] = {}, -- This is only used if IgnoreSpecialConstraints is true. Fill with constraint types from the ConstraintTypes table
--      ["DisableWeighting"] = false,
--      ["DisableWelding"] = false,
--      ["DisableParenting"] = false
--  }

local function doshit(self, ent, ply, const)
	for i,k in pairs(const) do
		--get the physics
		local kphys = k:GetPhysicsObject()
		
		--freeze the contraption and nullify all collisions(we put these back later!)
		kphys:EnableCollisions(false)
		kphys:EnableMotion(false)
		kphys:Sleep()
		
		--unconstrain everything
        if not k.SCAutoParenter or not (k.SCAutoParenter.DisableConstraintRemoval or k.SCAutoParenter.IgnoreSpecialConstraints) then
		    constraint.RemoveAll(k)
        elseif k.SCAutoParenter.IgnoreSpecialConstraints and k.SCAutoParenter.IgnoredConstraints ~= nil then
            for _,c in pairs(ConstraintTypes) do
                if not table.HasValue(k.SCAutoParenter.IgnoredConstraints, c) then
                    constraint.RemoveConstraints(k, c)
                end
            end
        end
	end
	
	--this part needs to be delayed a bit sadly
	timer.Simple(0.1, function()
		for i,k in pairs(const) do
			if IsValid(k) then
				--get the physics
				local kphys = k:GetPhysicsObject()
				
				--make sure we don't do this to the parent
				if k ~= ent then
					--find out if we want to ignore anything
					local doweight, doweld, doparent = true, true, true
					if k.SCAutoParenter then
						doweight = not k.SCAutoParenter.DisableWeighting
						doweld = not k.SCAutoParenter.DisableWelding
						doparent = not k.SCAutoParenter.DisableParenting
					end
					
					--reweld everything
					if doweld then constraint.Weld(k, ent, 0, 0, 0, false, true) end
					
					if doweight then
						--set everything's mass
						if not k:IsVehicle() then
							if IsValid(kphys) then kphys:SetMass(self:GetClientNumber("hullweight", 500)) end
						else
							if IsValid(kphys) then kphys:SetMass(self:GetClientNumber("vehweight", 5000)) end
						end
					end
					
					--parent that shit up
					if doparent and not IsValid(ent:GetParent()) and not k:IsVehicle() then
						k:SetParent(ent)
					end
				else
					--if we're the parent set our mass and leave us alone
					if IsValid(kphys) then kphys:SetMass(self:GetClientNumber("parweight", 50000)) end
				end
				
				--undo the funky shit
				kphys:EnableCollisions(true)
				kphys:Sleep()
			end
		end

        ply:ChatPrint("[Auto-Parenter] - Finished Parenting!")
	end)
end

function TOOL:LeftClick( trace )
	if CLIENT then return true end
	if IsValid(trace.Entity) and trace.Entity:IsPlayer() then return end
	if SERVER and not util.IsValidPhysicsObject(trace.Entity, trace.PhysicsBone) then return false end
	if trace.Entity:IsWorld() then return false end
	
	local ent = trace.Entity
	local ply = self:GetOwner()
	local const = constraint.GetAllConstrainedEntities(ent)
	local phys = ent:GetPhysicsObject()
	
	phys:Sleep()

    ply:ChatPrint("[Auto-Parenter] - Beginning Parenting!")

	doshit(self, ent, ply, const)
	
	return true
end

function TOOL:Reload()
    if SERVER then return false end
    if not IsValid(LocalPlayer()) then return false end

    -- Reset everything to defaults
    LocalPlayer():ConCommand("auto_parent_hullweight 500")
    LocalPlayer():ConCommand("auto_parent_vehweight 5000")
    LocalPlayer():ConCommand("auto_parent_parweight 50000")

    return false
end

local CPanel = nil
function TOOL:Think()
	if CLIENT then
		if IsValid(CPanel) then
			local hull = self:GetClientNumber("hullweight", 500)
			local veh = self:GetClientNumber("vehweight", 5000)
			local par = self:GetClientNumber("parweight", 50000)
			
			if hull < 10000 then
				CPanel.HullLabel:SetText("Recommended: 20-2000")
				CPanel.HullLabel:SetTextColor(table.Copy(CPanel.DefaultTextColor))
			else
				CPanel.HullLabel:SetText("WARNING! MAY BE UNSTABLE!")
				CPanel.HullLabel:SetTextColor(Color(255,55,55,255))
			end
			if veh < 10000 then
				CPanel.VehicleLabel:SetText("Recommended: 1000-5000")
				CPanel.VehicleLabel:SetTextColor(table.Copy(CPanel.DefaultTextColor))
			else
				CPanel.VehicleLabel:SetText("WARNING! MAY BE UNSTABLE!")
				CPanel.VehicleLabel:SetTextColor(Color(255,55,55,255))
			end
			if par == 50000 then
				CPanel.ParentLabel:SetText("Recommended: 50000\nOnly change this slider if you really know what you're doing.")
				CPanel.ParentLabel:SetTextColor(table.Copy(CPanel.DefaultTextColor))
			else
				CPanel.ParentLabel:SetText("WARNING! MAY BE UNSTABLE!\nOnly change this slider if you really know what you're doing.")
				CPanel.ParentLabel:SetTextColor(Color(255,55,55,255))
			end
		end
	end
end

function TOOL.BuildCPanel(panel)
	CPanel = panel
	local BindLabel0 = {}

	BindLabel0.Text =
[[Adjust the weight sliders below if your ship is wobbling!

Some ships need lower weight values, while others will need much higher ones, experiment to find the best for your ship! Usually, however, you won't need or want to change the parent weight!

If your ship still wobbles even after you've messed with the sliders for a while, please add a larger parent prop and try again.

Always remember though, if you mess with these sliders too much you could just make the problem worse! If you find yourself in a situation where all of your ships wobble because you messed up the numbers, press your reload key to reset the tool to its default settings.]]
	
	BindLabel0.Description = "Basic Instructions1"
	CPanel:AddControl("Label", BindLabel0 )

	CPanel:AddControl("Slider", {
	    Label = "Vehicle Weight",
	    Type = "Int",
	    Min = "1",
	    Max = "50000",
	    Command = "auto_parent_vehweight"
	})

	CPanel.VehicleLabel = vgui.Create("DLabel",CPanel)
	CPanel.VehicleLabel:SetText("Recommended: 1000-5000")
	--CPanel.VehicleLabel:DockPadding(5,5,5,5)
	CPanel.VehicleLabel:DockMargin(0,-15,-10,0)
	CPanel:AddItem(CPanel.VehicleLabel)

	CPanel.DefaultTextColor = CPanel.VehicleLabel:GetTextColor()

	CPanel:AddControl("Slider", {
	    Label = "Hull Weight",
	    Type = "Int",
	    Min = "1",
	    Max = "50000",
	    Command = "auto_parent_hullweight"
	})

	CPanel.HullLabel = vgui.Create("DLabel",CPanel)
	CPanel.HullLabel:SetText("Recommended: 20-2000")
	CPanel.HullLabel:DockMargin(0,-15,-10,0)
	CPanel:AddItem(CPanel.HullLabel)

    CPanel:AddControl("Slider", {
	    Label = "Parent Weight",
	    Type = "Int",
	    Min = "1",
	    Max = "50000",
	    Command = "auto_parent_parweight"
	})

	CPanel.ParentLabel = vgui.Create("DLabel",CPanel)
	CPanel.ParentLabel:SetText("Recommended: 50000\nOnly change this slider if you really know what you're doing.")
	CPanel.ParentLabel:SizeToContents()
	CPanel.ParentLabel:DockMargin(0,-7.5,-10,0)
	CPanel:AddItem(CPanel.ParentLabel)

end