TOOL.Category = "Ship Tools"
TOOL.Name = "#Life Support Spawner"
TOOL.Command = nil
TOOL.ConfigName = ""
TOOL.Tab = "Space Combat 2"
TOOL.Information = {
	"left",
	"right",
	"reload"
}

TOOL.ClientConVar["model"] = "models/props_phx/life_support/panel_medium.mdl"
TOOL.ClientConVar["type"] = "Generator"
TOOL.ClientConVar["subtype"] = "Solar Panel"
TOOL.ClientConVar["weld"] = 0
TOOL.ClientConVar["freeze"] = 1
TOOL.ClientConVar["world"] = 0
TOOL.ClientConVar["use_protect"] = 1

-- Used by the tool to determine how much of something is in storage
local PlayerStorageData = {}

if CLIENT then
	language.Add("Tool.sc_generators.name", "Space Combat 2 Life Support Spawner")
	language.Add("Tool.sc_generators.desc", "Spawning Generators like a boss.")
	language.Add("Tool.sc_generators.left", "Spawn or Update Generator or Storage" )
	language.Add("Tool.sc_generators.right", "Copy Model from Entity, or Reset on World." )
	language.Add("Tool.sc_generators.reload", "Replace Entity with Generator or Storage" )
	language.Add("Undone_sc_generators", "Undone Life Support")

	-- GHOST FUNCTIONS COPY PASTED FROM WIREMOD

	-- basic UpdateGhost function that should cover most of wire's ghost updating needs [default]
	function TOOL:UpdateGhost( ent )
		if not IsValid(ent) then return end

		local Trace = self:GetOwner():GetEyeTrace()
		if not Trace.Hit then
			ent:SetNoDraw( true )
			return
		end

		-- don't draw the ghost if we hit nothing, a player, or an npc
		if IsValid(Trace.Entity) and (Trace.Entity:IsPlayer() or Trace.Entity:IsNPC()) then
			ent:SetNoDraw( true )
			return
		end

		ent:SetAngles( Trace.HitNormal:Angle() + Angle(90,0,0) )
		ent:SetPos( Trace.HitPos - Trace.HitNormal * ent:OBBMins().z )

		--show the ghost
		ent:SetNoDraw( false )
	end


	-- option tool Think function for updating the pos of the ghost and making one when needed [default]
	function TOOL:Think()
		local model = self:GetClientInfo( "model" )
		if not IsValid(self.GhostEntity) or self.GhostEntity:GetModel() ~= model then
			self:MakeGhostEntity( model, Vector(0,0,0), Angle(0,0,0) )
			if IsValid(self.GhostEntity) and CLIENT then self.GhostEntity:SetPredictable(true) end
		end
		self:UpdateGhost( self.GhostEntity )
	end
else -- Server Only
    -- Console Command to update storage values
    concommand.Add("sc_generators_storageupdate", function(Player, Command, Args)
        -- Don't run this if we didn't get an argument, bad shit happens.
        if not Args or not Args[1] or not Args[2] or not Args[3] then return end

        -- Update the PlayerStorageData table with all kinds of safety in place.
        local UniqueID = Player:SteamID64()
        PlayerStorageData[UniqueID] = PlayerStorageData[UniqueID] or {}
        PlayerStorageData[UniqueID][Args[1]] = PlayerStorageData[UniqueID][Args[1]] or {}
        PlayerStorageData[UniqueID][Args[1]][Args[2]] = tonumber(Args[3]) or 1
    end, nil, "Updates the Spawner Tool's resource modifiers. (StorageType, ResourceName, Modifier)")

    -- Hook to clean out the storage info when someone leaves.
    hook.Add("PlayerDisconnected", "sc_generators.CleanStorageData", function(Player)
        PlayerStorageData[Player:SteamID64()] = nil
    end)
end

function TOOL:LeftClick(trace)
	if CLIENT then return true end

    -- Selected Generator and Model
    local Type = self:GetClientInfo("type")
    local Subtype = self:GetClientInfo("subtype")
	local Model = self:GetClientInfo("model")

    -- Tool Settings
	local Weld = self:GetClientNumber("weld", 1)
	local WorldWeld = self:GetClientNumber("world", 0)
	local Freeze = self:GetClientNumber("freeze", 1)
	local UseProtection = self:GetClientNumber("use_protect", 0)

    -- Owner and Rotation
    local Trace = self:GetOwner():GetEyeTrace()
	local Owner = self:GetOwner()
	local SpawnAngle = Trace.HitNormal:Angle() + Angle(90,0,0)

    if Type == "Generator" then
        -- We only want to use valid generators
        if not GAMEMODE:GetGeneratorInfo(Subtype) then return false end
    else
        -- Only spawn storage types that have a default model
        if not GAMEMODE:GetDefaultStorageModel(Subtype) then return end
    end

    -- Check if we're trying to update a generator or storage entity
    if IsValid(Trace.Entity) and (not Trace.Entity.CPPICanTool or Trace.Entity:CPPICanTool(Owner)) then
        if Type == "Generator" and Trace.Entity:GetClass() == GAMEMODE:GetGeneratorInfo(Subtype):GetClass() then
            Trace.Entity:SetupGenerator(GAMEMODE:GetGeneratorInfo(Subtype))
            return true
        elseif Type == "Storage" and Trace.Entity:GetClass() == "sc_storage" then
            local UniqueID = Owner:SteamID64()
            PlayerStorageData[UniqueID] = PlayerStorageData[UniqueID] or {}
            Trace.Entity:SetStorageType(Subtype, PlayerStorageData[UniqueID][Subtype])
            return true
        end
    end

    -- Spawn the generator
	local NewEntity
    if Type == "Generator" then
        local GenInfo = GAMEMODE:GetGeneratorInfo(Subtype)
        NewEntity = ents.Create(GenInfo:GetClass())

        if GenInfo:GetForceModel() then
            Model = GenInfo:GetDefaultModel()
        end
    elseif Type == "Storage" then
        NewEntity = ents.Create("sc_storage")
    end

    NewEntity:SetModel(Model or "models/props_phx/life_support/panel_medium.mdl")
	NewEntity:SetPos(Trace.HitPos - Trace.HitNormal * NewEntity:OBBMins().z)
	NewEntity:SetAngles(SpawnAngle)
	NewEntity:SetPlayer(Owner)
	NewEntity:Spawn()

    if Type == "Generator" then
        NewEntity:SetupGenerator(GAMEMODE:GetGeneratorInfo(Subtype))
    elseif Type == "Storage" then
        local UniqueID = Owner:SteamID64()
        PlayerStorageData[UniqueID] = PlayerStorageData[UniqueID] or {}
        NewEntity:SetStorageType(Subtype, PlayerStorageData[UniqueID][Subtype])
    end

    -- Weld the entity if we had that enabled
	if Weld == 1 then
		if WeldWorld == 1 then
            -- Weld to props and World
			local const = constraint.Weld(NewEntity, Trace.Entity, 0, Trace.PhysicsBone, 0, true)
		elseif IsValid(Trace.Entity) then
            -- Weld to props but not to world
			local const = constraint.Weld(NewEntity, Trace.Entity, 0, Trace.PhysicsBone, 0, true)
		end
	end

    -- Decide if the entity should be frozen or unfrozen when spawned
	local Physics = NewEntity:GetPhysicsObject()
	if IsValid(Physics) then
		if Freeze == 1 then
			Physics:EnableMotion(false)
		end

		Physics:Wake()
	end

	undo.Create("sc_generators")
	undo.AddEntity(NewEntity)
	undo.SetPlayer(Owner)
	undo.Finish()
	Owner:AddCleanup("Generators", NewEntity)

	return true
end


function TOOL:RightClick(Trace)
    if SERVER then return end
	--Select Model
    if not IsValid(Trace.Entity) or Trace.Entity:IsPlayer() then
        local Type = self:GetClientInfo("type")
        if Type == "Generator" then
            local GeneratorInfo = GAMEMODE:GetGeneratorInfo(self:GetClientInfo("subtype"))
            if GeneratorInfo then
                RunConsoleCommand( "sc_generators_model", GeneratorInfo:GetDefaultModel())
            end
        elseif Type == "Storage" then
            local Model = GAMEMODE:GetDefaultStorageModel(self:GetClientInfo("subtype"))
            if Model then
                RunConsoleCommand( "sc_generators_model", Model)
            end
        end

        return false
    end

    RunConsoleCommand( "sc_generators_model", Trace.Entity:GetModel())
end

function TOOL:Reload(Trace)
	if not IsValid(Trace.Entity) or Trace.Entity:IsPlayer() then return false end

	local Owner = self:GetOwner()

	if not Trace.Entity.Owner == Owner then return false end
    if CLIENT then return true end

    -- Selected Generator and Model
    local Type = self:GetClientInfo("type")
    local Subtype = self:GetClientInfo("subtype")

    if Type == "Generator" then
        -- We only want to use valid generators
        if not GAMEMODE:GetGeneratorInfo(Subtype) then return false end
    else
        -- Only spawn storage types that have a default model
        if not GAMEMODE:GetDefaultStorageModel(Subtype) then return false end
    end

    -- Tool Settings
	local Weld = self:GetClientNumber("weld", 1)
	local Freeze = self:GetClientNumber("freeze", 1)
	local UseProtection = self:GetClientNumber("use_protect", 0)

    -- Get variables from trace hit entity
	local Replacing = Trace.Entity
	local SpawnPos = Replacing:GetPos()
	local SpawnAngle = Replacing:GetAngles()
	local Model = Replacing:GetModel()
	local Color = Replacing:GetColor()
	local Material = Replacing:GetMaterial()
	local Skin = Replacing:GetSkin()

    -- Spawn the generator
	local NewEntity
    if Type == "Generator" then
        local GenInfo = GAMEMODE:GetGeneratorInfo(Subtype)
        NewEntity = ents.Create(GenInfo:GetClass())

        if GenInfo:GetForceModel() then
            if GenInfo:GetDefaultModel() ~= Model then return false end -- Notify Player?
        end
    elseif Type == "Storage" then
        NewEntity = ents.Create("sc_storage")
    end

    NewEntity:SetModel(Model)
	NewEntity:SetPos(SpawnPos)
	NewEntity:SetAngles(SpawnAngle)
	NewEntity:SetColor(Color)
	NewEntity:SetMaterial(Material)
	NewEntity:SetSkin(Skin)
	NewEntity:SetPlayer(Owner)
	NewEntity:Spawn()

	duplicator.StoreEntityModifier( NewEntity, "colour", { Color = Color, RenderMode = 0, RenderFX = 0 } )
	duplicator.StoreEntityModifier( NewEntity, "material", {MaterialOverride = Material} )

    if Type == "Generator" then
        NewEntity:SetupGenerator(GAMEMODE:GetGeneratorInfo(Subtype))
    elseif Type == "Storage" then
        local UniqueID = Owner:SteamID64()
        PlayerStorageData[UniqueID] = PlayerStorageData[UniqueID] or {}
        NewEntity:SetStorageType(Subtype, PlayerStorageData[UniqueID][Subtype])
    end

    -- Weld the entity if we had that enabled
	if Weld then
		local cons = constraint.FindConstraint(Replacing, "Weld")
		local parent = cons and ( cons.Ent1 == Replacing and cons.Ent2 or cons.Ent1 ) or nil
		if IsValid(parent) then
			local const = constraint.Weld(NewEntity, parent, 0, 0, 0, true)
		end
	end

    -- Decide if the entity should be frozen or unfrozen when spawned
	local Physics = NewEntity:GetPhysicsObject()
	if IsValid(Physics) then
		local RepPhysics = Replacing:GetPhysicsObject()
		if Freeze or IsValid(RepPhysics) and RepPhysics:IsMoveable() then
			Physics:EnableMotion(false)
		end

		Physics:Wake()
	end

	-- Remove the old Entity if we're not at the limit for Storage/Generators
	local Class = NewEntity:GetClass()
	local Cvar = GetConVar("sbox_max"..Class)
	if not ( ( Owner:GetCount(Class) + 1 ) > ( Cvar and Cvar:GetInt() or 1000 ) ) then

		-- Remove all constraints (this stops ropes from hanging around)
		constraint.RemoveAll( Replacing )

		-- Remove it properly in 1 second
		timer.Simple( 1, function() if ( IsValid( Replacing ) ) then Replacing:Remove() end end )

		-- Make it non solid
		Replacing:SetNotSolid( true )
		Replacing:SetMoveType( MOVETYPE_NONE )
		Replacing:SetNoDraw( true )

			-- Send Effect, because why not
		local ed = EffectData()
		ed:SetEntity( Replacing )
		util.Effect( "entity_remove", ed, true, true )
	end

	undo.Create("sc_generators")
	undo.AddEntity(NewEntity)
	undo.SetPlayer(Owner)
	undo.Finish()
	Owner:AddCleanup("Generators", NewEntity)

	return true
end

local function CreateGeneratorsTab(CPanel, GeneratorsSheet)
    -- Create our information display first because we need these variables later
	local InfoPanel = vgui.Create("DPanel", GeneratorsSheet)
	InfoPanel:SetSize(GeneratorsSheet:GetWide() - 10, 140)
	InfoPanel:DockMargin(2, 2, 2, 2)
	InfoPanel:Dock(TOP)

	local BigIcon = vgui.Create("SpawnIcon", InfoPanel)
	BigIcon:SetSize(128, 128)
    BigIcon:Dock(LEFT)
	BigIcon:DockMargin(0, 6, 0, 6)
	BigIcon:SetModel("")

    local InfoTextPanel = vgui.Create("DPanel", InfoPanel)
    InfoTextPanel:Dock(FILL)

	local Title = vgui.Create("DLabel", InfoTextPanel)
	Title:SetText("Select a Generator")
	Title:SetSize(175, 0)
	Title:SetColor(Color(255,255,255, 255))
	Title:SetFont("WorkshopLarge")
	Title:SizeToContents()
    Title:DockMargin(5, 10, 2, 2)
    Title:Dock(TOP)

	local InfoLabel = vgui.Create("DLabel", InfoTextPanel)
	InfoLabel:SetText("Waiting for Selection")
	InfoLabel:SetWrap(true)
	InfoLabel:SetSize(175, 130)
	InfoLabel:SetColor(Color(255,255,255, 255))
	InfoLabel:SetFont("ToolInfo")
    InfoLabel:SetContentAlignment(8)
    InfoLabel:DockMargin(5, 10, 2, 2)
    InfoLabel:Dock(TOP)

    -- Create and populate generator panel
    local GeneratorScrollPanel = vgui.Create("DScrollPanel", GeneratorsSheet)
    GeneratorScrollPanel:SetSize(GeneratorsSheet:GetWide() - 10, 540)
    GeneratorScrollPanel:DockMargin(5, 5, 2, 2)
    GeneratorScrollPanel:Dock(TOP)

    local Categories = {}
	for Name, Info in pairs(GAMEMODE:GetGenerators()) do
        -- Check if we have created this category yet, and create it if we haven't
        local Category = Info:GetCategory()
        if not Categories[Category] then
		    local VGUICategory = vgui.Create("DCollapsibleCategory", GeneratorScrollPanel)
		    VGUICategory:SetLabel(Category)
            VGUICategory:SetContentAlignment(5)
            VGUICategory:DockMargin(2, 5, 2, 2)
            VGUICategory:Dock(TOP)

		    local CategoryGrid = vgui.Create("DGrid", VGUICategory)
		    CategoryGrid:Dock(TOP)
            CategoryGrid:SetCols(1)
            CategoryGrid:SetContentAlignment(5)
		    CategoryGrid:SetRowHeight(32)

            -- Why the hell does garry not provide the ability to set this automatically
            if ScrW() > 1600 then
		        CategoryGrid:SetColWide(328)
	        elseif ScrW() > 1280 then
		        CategoryGrid:SetColWide(270)
	        else
		        CategoryGrid:SetColWide(230)
	        end

            Categories[Category] = {Grid = CategoryGrid, Collapsible = VGUICategory}
        end

        -- Local function that we're going to reuse for all DoClick events
        local function OnClick()
            -- Run console commands to tell the server what we selected
			RunConsoleCommand("sc_generators_model", Info:GetDefaultModel())
			RunConsoleCommand("sc_generators_type", "Generator")
            RunConsoleCommand("sc_generators_subtype", Name)

            -- Update the information panel
			BigIcon:SetModel(Info:GetDefaultModel())
			Title:SetText(Name)
			Title:SizeToContents()
			InfoLabel:SetText(Info:GetDescription())
		end

        local CategoryGrid = Categories[Category].Grid
        local GeneratorPanel = vgui.Create("DPanel", CategoryGrid)
        GeneratorPanel:SetHeight(32)
        GeneratorPanel:Dock(TOP)

        -- Create the icon, dock left
		local GeneratorIcon = vgui.Create("SpawnIcon", GeneratorPanel)
		GeneratorIcon:SetModel(Info:GetDefaultModel())
		GeneratorIcon:SetToolTip(Name.."\n\n"..Info:GetDescription())
        GeneratorIcon:SetSize(32, 32)
        GeneratorIcon:Dock(LEFT)
		GeneratorIcon.DoClick = OnClick

        -- Create the label, dock fill
        local GeneratorLabel = vgui.Create("DLabel", GeneratorPanel)
        GeneratorLabel:SetText(Name)
        GeneratorLabel:SetFont("WorkshopLarge")
        GeneratorLabel:SetToolTip(Name.."\n\n"..Info:GetDescription())
        GeneratorLabel:SetContentAlignment(5)
        GeneratorLabel:DockMargin(2, 2, 5, 5)
        GeneratorLabel:Dock(FILL)
        GeneratorLabel:SetMouseInputEnabled(true)
        GeneratorLabel:SetKeyboardInputEnabled(true)
        GeneratorLabel:SetCursor("hand")
        GeneratorLabel.DoClick = OnClick

        -- Update grid and category
		CategoryGrid:AddItem(GeneratorPanel)
        CategoryGrid:InvalidateLayout(false)
        CategoryGrid:InvalidateParent(false)
	end
end

local function CreateStorageTab(CPanel, StorageSheet)
    -- Create our information display first because we need these variables later
	local InfoPanel = vgui.Create("DPanel", StorageSheet)
	InfoPanel:SetSize(StorageSheet:GetWide() - 10, 140)
	InfoPanel:DockMargin(2, 2, 2, 2)
	InfoPanel:Dock(TOP)

	local BigIcon = vgui.Create("SpawnIcon", InfoPanel)
	BigIcon:SetSize(128, 128)
    BigIcon:Dock(LEFT)
	BigIcon:DockMargin(0, 6, 0, 6)
	BigIcon:SetModel("")

    local InfoTextPanel = vgui.Create("DPanel", InfoPanel)
    InfoTextPanel:Dock(FILL)

	local Title = vgui.Create("DLabel", InfoTextPanel)
	Title:SetText("Select a Storage")
	Title:SetSize(175, 0)
	Title:SetFont("WorkshopLarge")
	Title:SizeToContents()
    Title:DockMargin(5, 10, 2, 2)
    Title:Dock(TOP)

	local InfoLabel = vgui.Create("DLabel", InfoTextPanel)
	InfoLabel:SetText("Waiting for Selection")
	InfoLabel:SetWrap(true)
	InfoLabel:SetSize(175, 130)
	InfoLabel:SetFont("ToolInfo")
    InfoLabel:SetContentAlignment(8)
    InfoLabel:DockMargin(5, 10, 2, 2)
    InfoLabel:Dock(TOP)

    -- Create and populate generator panel
    local StorageScrollPanel = vgui.Create("DScrollPanel", StorageSheet)
    StorageScrollPanel:SetSize(StorageSheet:GetWide() - 10, 540)
    StorageScrollPanel:DockMargin(5, 5, 2, 2)
    StorageScrollPanel:Dock(TOP)

    local VGUICategory = vgui.Create("DCollapsibleCategory", StorageScrollPanel)
	VGUICategory:SetLabel("Storages")
    VGUICategory:SetContentAlignment(5)
    VGUICategory:DockMargin(2, 5, 2, 2)
    VGUICategory:Dock(TOP)

	local CategoryGrid = vgui.Create("DGrid", VGUICategory)
	CategoryGrid:Dock(TOP)
    CategoryGrid:SetCols(1)
    CategoryGrid:SetContentAlignment(5)
	CategoryGrid:SetRowHeight(32)

    -- Why the hell does garry not provide the ability to set this automatically
    if ScrW() > 1600 then
		CategoryGrid:SetColWide(328)
	elseif ScrW() > 1280 then
		CategoryGrid:SetColWide(270)
	else
		CategoryGrid:SetColWide(230)
	end

    local UpdateCustomizationGrid -- Need to declare this beforehand or it'll error, defined later in the file
	for Name, Model in pairs(GAMEMODE:GetStorageModels()) do
        -- Local function that we're going to reuse for all DoClick events
        local function OnClick()
            -- Run console commands to tell the server what we selected
			RunConsoleCommand("sc_generators_model", Model)
			RunConsoleCommand("sc_generators_type", "Storage")
            RunConsoleCommand("sc_generators_subtype", Name)

            -- Update the information panel
			BigIcon:SetModel(Model)
			Title:SetText(Name)
			Title:SizeToContents()
			InfoLabel:SetText("This is a type of storage that holds "..Name.." resources.")

            -- Update the sliders
            UpdateCustomizationGrid(Name)
		end

        local StoragePanel = vgui.Create("DPanel", CategoryGrid)
        StoragePanel:SetHeight(32)
        StoragePanel:Dock(TOP)

        -- Create the icon, dock left
		local StorageIcon = vgui.Create("SpawnIcon", StoragePanel)
		StorageIcon:SetModel(Model)
		StorageIcon:SetToolTip(Name)
        StorageIcon:SetSize(32, 32)
        StorageIcon:Dock(LEFT)
		StorageIcon.DoClick = OnClick

        -- Create the label, dock fill
        local StorageLabel = vgui.Create("DLabel", StoragePanel)
        StorageLabel:SetText(Name)
        StorageLabel:SetFont("WorkshopLarge")
        StorageLabel:SetToolTip(Name)
        StorageLabel:SetContentAlignment(5)
        StorageLabel:DockMargin(2, 2, 5, 5)
        StorageLabel:Dock(FILL)
        StorageLabel:SetMouseInputEnabled(true)
        StorageLabel:SetKeyboardInputEnabled(true)
        StorageLabel:SetCursor("hand")
        StorageLabel.DoClick = OnClick

        -- Update grid and category
		CategoryGrid:AddItem(StoragePanel)
        CategoryGrid:InvalidateLayout(false)
        CategoryGrid:InvalidateParent(false)
	end

    local CustomizationCategory = vgui.Create("DCollapsibleCategory", StorageScrollPanel)
    CustomizationCategory:SetLabel("Storage Configuration")
    CustomizationCategory:SetContentAlignment(5)
    CustomizationCategory:DockMargin(2, 5, 2, 2)
    CustomizationCategory:Dock(TOP)

    local CustomizationLabel = vgui.Create("DLabel", CustomizationCategory)
	CustomizationLabel:SetText("This section lets you control what resources will be in your storage. If a box is checked, then the storage will contain that resource, if it is not check the storage will not contain that resource.")
	CustomizationLabel:SetWrap(true)
	CustomizationLabel:SetFont("ToolInfo")
    CustomizationLabel:SetContentAlignment(8)
    CustomizationLabel:SetHeight(100)
    CustomizationLabel:DockMargin(5, 10, 2, 2)
    CustomizationLabel:Dock(TOP)

    local CustomizationGrid = vgui.Create("DGrid", CustomizationCategory)
	CustomizationGrid:Dock(TOP)
    CustomizationGrid:SetCols(1)
    CustomizationGrid:SetContentAlignment(5)
	CustomizationGrid:SetRowHeight(24)

    -- Again, *stare*
    if ScrW() > 1600 then
		CustomizationGrid:SetColWide(328)
	elseif ScrW() > 1280 then
		CustomizationGrid:SetColWide(270)
	else
		CustomizationGrid:SetColWide(230)
	end

    local CustomizationPanels = {}
    UpdateCustomizationGrid = function(StorageType)
        -- Clean up any existing children
        for i,k in pairs(CustomizationPanels) do
            CustomizationGrid:RemoveItem(k)
            k:Remove()
        end

        -- Make sure our list got reset just in case
        CustomizationPanels = {}

        -- Get ready to add new children
        for Name, Default in pairs(GAMEMODE:GetResourcesOfType(StorageType)) do
            -- Panel to hold the stuff
            local CustomizationPanel = vgui.Create("DPanel", CustomizationGrid)
            CustomizationPanel:SetHeight(24)
            CustomizationPanel:Dock(TOP)
            CustomizationPanels[Name] = CustomizationPanel

            -- Create the icon, dock left
		    local CustomizationCheckBox = vgui.Create("DCheckBox", CustomizationPanel)
		    CustomizationCheckBox:SetToolTip(Name)
            CustomizationCheckBox:SetSize(24, 24)
            CustomizationCheckBox:Dock(LEFT)
            CustomizationCheckBox.OnChange = function(self, value)
                -- Inform the server that something changed
                RunConsoleCommand("sc_generators_storageupdate", StorageType, Name, value and 1 or 0)

                -- Save the value to a cookie so we can load it later
                cookie.Set("sc_generators_"..StorageType..Name, value and 1 or 0)
            end
            CustomizationCheckBox:SetValue(cookie.GetNumber("sc_generators_"..StorageType..Name, 1))

            -- Create the label, dock fill
            local CustomizationLabel = vgui.Create("DLabel", CustomizationPanel)
            CustomizationLabel:SetText(Name)
            CustomizationLabel:SetFont("WorkshopLarge")
            CustomizationLabel:SetToolTip(Name)
            CustomizationLabel:SetContentAlignment(5)
            CustomizationLabel:DockMargin(2, 2, 5, 5)
            CustomizationLabel:Dock(FILL)

            -- Update grid and category
		    CustomizationGrid:AddItem(CustomizationPanel)
            CustomizationGrid:InvalidateLayout(false)
            CustomizationGrid:InvalidateParent(false)
	    end
    end
end

local function CreateSearchTab(CPanel, SearchSheet)

end

local function CreateSettingsTab(CPanel, SettingsSheet)
    -- Create check boxes for our settings sheet
	local WeldButton = vgui.Create("DCheckBoxLabel", SettingsSheet)
	WeldButton:Dock(TOP)
	WeldButton:DockMargin(5, 10, 2, 2)
	WeldButton:SetValue(GetConVar("sc_generators_weld"):GetBool())
	WeldButton:SetText("Weld")
	WeldButton:SetConVar("sc_generators_weld")

	local FreezeButton = vgui.Create("DCheckBoxLabel", SettingsSheet)
	FreezeButton:Dock(TOP)
	FreezeButton:DockMargin(5, 10, 2, 2)
	FreezeButton:SetValue(GetConVar("sc_generators_freeze"):GetBool())
	FreezeButton:SetText("Freeze")
	FreezeButton:SetConVar("sc_generators_freeze")

	local AllowWorld = vgui.Create("DCheckBoxLabel", SettingsSheet)
	AllowWorld:Dock(TOP)
	AllowWorld:DockMargin(5, 10, 2, 2)
	AllowWorld:SetValue(GetConVar("sc_generators_world"):GetBool())
	AllowWorld:SetText("Allow World Welding")
	AllowWorld:SetConVar("sc_generators_world")

	local UseProtect = vgui.Create("DCheckBoxLabel", SettingsSheet)
	UseProtect:Dock(TOP)
	UseProtect:DockMargin(5, 10, 2, 2)
	UseProtect:SetValue(GetConVar("sc_generators_use_protect"):GetBool())
	UseProtect:SetText("Only PP friends can toggle your generators")
	UseProtect:SetConVar("sc_generators_use_protect")
end

function TOOL.BuildCPanel(CPanel)
    -- Some settings for our tool panel
	CPanel:SetSpacing(10)
    CPanel:SetHeight(750)
	CPanel:SetName("Space Combat 2 Entity Spawner")

    -- This fixes the tool menu being collapsable, which breaks the tool
    CPanel.Header.DoClick = function() end

    -- Property Sheet to hold our sheets
    local PropertySheet = vgui.Create("DPropertySheet", CPanel)
    PropertySheet:Dock(FILL)

    -- Make our sheets
    local GeneratorsSheet = vgui.Create("DPanel", PropertySheet)
    local StorageSheet = vgui.Create("DPanel", PropertySheet)
    local SearchSheet = vgui.Create("DPanel", PropertySheet)
    local SettingsSheet = vgui.Create("DPanel", PropertySheet)

    -- Dock our sheets
    PropertySheet:AddSheet("Generators", GeneratorsSheet, "icon16/heart.png", true, true)
    PropertySheet:AddSheet("Storage", StorageSheet, "icon16/wrench.png", true, true)
    PropertySheet:AddSheet("Search", SearchSheet, "icon16/wrench.png", true, true)
	PropertySheet:AddSheet("Settings", SettingsSheet, "icon16/wrench.png", true, true)

    GeneratorsSheet:Dock(FILL)
    StorageSheet:Dock(FILL)
    SearchSheet:Dock(FILL)
    SettingsSheet:Dock(FILL)

	-- Fill our sheets
    CreateGeneratorsTab(CPanel, GeneratorsSheet)
    CreateStorageTab(CPanel, StorageSheet)
    CreateSearchTab(CPanel, SearchSheet)
    CreateSettingsTab(CPanel, SettingsSheet)
end