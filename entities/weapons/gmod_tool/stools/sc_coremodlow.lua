TOOL.Category		= "Core Mods"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Low Slot"
TOOL.Command		= nil
TOOL.ConfigName		= ""

TOOL.ClientConVar[ "cap" ] = 0
TOOL.ClientConVar[ "capp" ] = 0
TOOL.ClientConVar[ "armor" ] = 0
TOOL.ClientConVar[ "armorp" ] = 0
TOOL.ClientConVar[ "ar_em" ] = 0
TOOL.ClientConVar[ "ar_ki" ] = 0
TOOL.ClientConVar[ "ar_th" ] = 0
TOOL.ClientConVar[ "ar_ex" ] = 0
TOOL.ClientConVar[ "hull" ] = 0
TOOL.ClientConVar[ "hullp" ] = 0
TOOL.ClientConVar[ "hu_em" ] = 0
TOOL.ClientConVar[ "hu_ki" ] = 0
TOOL.ClientConVar[ "hu_th" ] = 0
TOOL.ClientConVar[ "hu_ex" ] = 0

if CLIENT then
    language.Add( "Tool.sc_coremodlow.name", "Space Combat 2 Core Mod Creation Tool" )
    language.Add( "Tool.sc_coremodlow.desc", "Spawn a core mod." )
    language.Add( "Tool.sc_coremodlow.0", "Primary: Create/update core mod" )
	language.Add( "undone_sc_coremodlow", "Undone Core Mod" )
end

cleanup.Register( "sc_module" )

local MaxPoints = 50

local function GetRemainingPoints(ply)
	if SERVER then
		if not IsValid(ply) then return 0 end
		
		return MaxPoints - (ply:GetInfoNum("sc_coremodlow_cap", 0) + ply:GetInfoNum("sc_coremodlow_capp", 0) + 
			ply:GetInfoNum("sc_coremodlow_armor", 0) + ply:GetInfoNum("sc_coremodlow_armorp", 0) + 
			ply:GetInfoNum("sc_coremodlow_ar_em", 0) + ply:GetInfoNum("sc_coremodlow_ar_ex", 0) + ply:GetInfoNum("sc_coremodlow_ar_th", 0) + ply:GetInfoNum("sc_coremodlow_ar_ki", 0) +
			ply:GetInfoNum("sc_coremodlow_hull", 0) + ply:GetInfoNum("sc_coremodlow_hullp", 0) +
			ply:GetInfoNum("sc_coremodlow_hu_em", 0) + ply:GetInfoNum("sc_coremodlow_hu_ex", 0) + ply:GetInfoNum("sc_coremodlow_hu_th", 0) + ply:GetInfoNum("sc_coremodlow_hu_ki", 0))
	else
		return MaxPoints - (GetConVarNumber("sc_coremodlow_cap") + GetConVarNumber("sc_coremodlow_capp") + 
			GetConVarNumber("sc_coremodlow_armor") + GetConVarNumber("sc_coremodlow_armorp") +
			GetConVarNumber("sc_coremodlow_ar_em") + GetConVarNumber("sc_coremodlow_ar_ex") + GetConVarNumber("sc_coremodlow_ar_th") + GetConVarNumber("sc_coremodlow_ar_ki") +
			GetConVarNumber("sc_coremodlow_hull") + GetConVarNumber("sc_coremodlow_hullp") +
			GetConVarNumber("sc_coremodlow_hu_em") + GetConVarNumber("sc_coremodlow_hu_ex") + GetConVarNumber("sc_coremodlow_hu_th") + GetConVarNumber("sc_coremodlow_hu_ki"))
	end
end

function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end
	
	local ply = self:GetOwner()
	local type = "LOW"
	local mods = {CAP=math.Max(ply:GetInfoNum("sc_coremodlow_cap", 0), 0), CAPP=ply:GetInfoNum("sc_coremodlow_capp", 0), 
	ARMOR=math.Max(ply:GetInfoNum("sc_coremodlow_armor", 0), 0), ARMORP=ply:GetInfoNum("sc_coremodlow_armorp", 0),
	AR_EM=ply:GetInfoNum("sc_coremodlow_ar_em", 0), AR_EX=ply:GetInfoNum("sc_coremodlow_ar_ex", 0), AR_TH=ply:GetInfoNum("sc_coremodlow_ar_th", 0), AR_KI=ply:GetInfoNum("sc_coremodlow_ar_ki", 0),
	HULL=math.Max(ply:GetInfoNum("sc_coremodlow_hull", 0), 0), HULLP=ply:GetInfoNum("sc_coremodlow_hullp", 0),
	HU_EM=ply:GetInfoNum("sc_coremodlow_hu_em", 0), HU_EX=ply:GetInfoNum("sc_coremodlow_hu_ex", 0), HU_TH=ply:GetInfoNum("sc_coremodlow_hu_th", 0), HU_KI=ply:GetInfoNum("sc_coremodlow_hu_ki", 0)}
	
	for i,k in pairs(mods) do
		if k == 0 then
			mods[i] = nil
		end
	end
	
	if GetRemainingPoints(ply) < 0 then
		ply:ChatPrint("[Space Combat 2] - Using too many points!")
		return false
	end
	
	--Update existing Weapon
	if ( trace.Entity:IsValid() and string.find(trace.Entity:GetClass(), "sc_module") and trace.Entity.Owner == ply ) then
	
		trace.Entity:Setup(type, mods)
		ply:ChatPrint("[Space Combat 2] - Core module was updated!")
		
		return false
	else
		local Ang = trace.HitNormal:Angle() + Angle(90,0,0)
		local Pos = trace.HitPos
		local Core = Make_sc_module(ply, Pos, Ang, type, mods)
			
		local phys = Core:GetPhysicsObject()
		if (phys:IsValid() and trace.Entity:IsValid() ) then
			local weld = constraint.Weld(Core, trace.Entity, 0, trace.PhysicsBone, 0)
			local nocollide = constraint.NoCollide(Core, trace.Entity, 0, trace.PhysicsBone)
			phys:EnableMotion(false)
		end
		
		undo.Create("sc_module")
			undo.AddEntity( Core )
			undo.SetPlayer( ply )
		undo.Finish()

		ply:AddCleanup( "sc_module", Core )
		
		return true
	end
end

local CPanel = nil
function TOOL:Think()
	if CLIENT then
		if IsValid(CPanel) then
			CPanel.PointLabel:SetText("Points Remaining: "..GetRemainingPoints())
		end
	end
end

function TOOL.BuildCPanel(panel)
	CPanel = panel
	
	CPanel:ClearControls()
	CPanel:SetName("Space Combat 2 - Core Mods")
	
	CPanel.PointLabel = vgui.Create("DLabel", CPanel)
	CPanel.PointLabel:SetText("Points Remaining: 50")
	CPanel:AddItem(CPanel.PointLabel)
	
	CPanel:AddControl("Slider", {
	    Label = "Capacitor x2000",
	    Type = "Int",
	    Min = "0",
	    Max = "60",
	    Command = "sc_coremodlow_cap"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Capacitor % boost",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_capp"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Armor x2000",
	    Type = "Int",
	    Min = "0",
	    Max = "60",
	    Command = "sc_coremodlow_armor"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Armor % boost",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_armorp"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Armor EM Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_ar_em"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Armor Explosive Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_ar_ex"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Armor Kinetic Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_ar_ki"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Armor Thermal Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_ar_th"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Hull x2000",
	    Type = "Int",
	    Min = "0",
	    Max = "60",
	    Command = "sc_coremodlow_hull"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Hull % boost",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_hullp"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Hull EM Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_hu_em"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Hull Explosive Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_hu_ex"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Hull Kinetic Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_hu_ki"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Hull Thermal Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodlow_hu_th"
	})
end