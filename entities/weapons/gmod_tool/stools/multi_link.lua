TOOL.Category		= "Ship Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Multi-Device Linker"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Information = {
	"left",
	"right",
	{
		name  = "shift_left",
		icon2  = "gui/e.png",
		icon = "gui/lmb.png",

	},
	"reload"
}

if ( CLIENT ) then
    language.Add( "Tool.multi_link.name", "Multi-Link Tool" )
    language.Add( "Tool.multi_link.desc", "Link multiple entities to one node." )
    language.Add( "Tool.multi_link.left", "Selects an entity to link." )
    language.Add( "Tool.multi_link.shift_left", "Selects all entities in a 2048 unit radius." )
    language.Add( "Tool.multi_link.right", "Link all selected entities to this node." )
    language.Add( "Tool.multi_link.reload", "Clear Targets." )
end

TOOL.enttbl = {}

function TOOL:LeftClick( trace )
	local ply = self:GetOwner()
	if IsValid(trace.Entity) and trace.Entity:IsPlayer() then return false end
	if trace.Entity:IsWorld() then return false end
	if CLIENT then return true end
	if not util.IsValidPhysicsObject(trace.Entity, trace.PhysicsBone) then return false end

	local ent = trace.Entity
	if self:GetOwner():KeyDown(IN_USE) then
		for k,v in pairs(ents.FindInSphere(trace.HitPos, 2048)) do
			if IsValid(v) and v.IsLSEnt and self:IsPropOwner(ply, v) and (not v.CPPICanTool or v:CPPICanTool(ply)) and not v:IsPlayer() and not v:IsWeapon() then
				local eid = v:EntIndex()
				if not self.enttbl[eid] then
					local col = v:GetColor()
					self.enttbl[eid] = col
					v:SetColor(Color(0,0,255,255))
				else
					local col = self.enttbl[eid]
					v:SetColor(col)
					self.enttbl[eid] = nil
				end
			end
		end
	else
		if IsValid(ent) and not ent:IsPlayer() and not ent:IsWeapon() and ent.IsLSEnt and self:IsPropOwner(ply, ent) and (not ent.CPPICanTool or ent:CPPICanTool(ply)) then
			local eid = ent:EntIndex()
			if not self.enttbl[eid] then
				local col = ent:GetColor()
				self.enttbl[eid] = col
				ent:SetColor(Color(0,0,255,255))
			else
				local col = self.enttbl[eid]
				ent:SetColor(col)
				self.enttbl[eid] = nil
			end
		end
	end

	return true
end


function TOOL:RightClick( trace )
	if IsValid(trace.Entity) and trace.Entity:IsPlayer() then return false end
	if trace.Entity:IsWorld() then return false end
	if trace.Entity:GetClass() ~= "ship_core" then return false end
	if CLIENT then return true end
	if table.Count(self.enttbl) < 1 then return end
	if not util.IsValidPhysicsObject(trace.Entity, trace.PhysicsBone) then return false end

	local ent = trace.Entity
	for k,v in pairs(self.enttbl) do
		local prop = ents.GetByIndex(k)
		if IsValid(prop) then

			if not prop.IsNode then prop:Link(ent) end

			prop:SetColor(v)
			self.enttbl[k] = nil
		end
	end
	self.enttbl = {}
	return true
end

function TOOL:SendMessage( message )
	self:GetOwner():PrintMessage( HUD_PRINTCENTER, message )
end

function TOOL:Reload()
	if CLIENT then return false end
	if table.Count(self.enttbl) < 1 then return end
	for k,v in pairs(self.enttbl) do
		local prop = ents.GetByIndex(k)
		if IsValid(prop) then
			prop:SetColor(v)
			self.enttbl[k] = nil
		end
	end
	self.enttbl = {}
	return true
end

function TOOL:IsPropOwner(ply, ent)
	if not ent.CPPIGetOwner or ent:CPPIGetOwner() == ply then return true end
	return false
end
