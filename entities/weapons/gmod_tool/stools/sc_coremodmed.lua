TOOL.Category		= "Core Mods"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Med Slot"
TOOL.Command		= nil
TOOL.ConfigName		= ""

TOOL.ClientConVar[ "cap" ] = 0
TOOL.ClientConVar[ "capp" ] = 0
TOOL.ClientConVar[ "shield" ] = 0
TOOL.ClientConVar[ "shieldr" ] = 0
TOOL.ClientConVar[ "shieldp" ] = 0
TOOL.ClientConVar[ "sh_em" ] = 0
TOOL.ClientConVar[ "sh_ki" ] = 0
TOOL.ClientConVar[ "sh_th" ] = 0
TOOL.ClientConVar[ "sh_ex" ] = 0

if ( CLIENT ) then
    language.Add( "Tool.sc_coremodmed.name", "Space Combat 2 Core Mod Creation Tool" )
    language.Add( "Tool.sc_coremodmed.desc", "Spawn a core mod." )
    language.Add( "Tool.sc_coremodmed.0", "Primary: Create/update core mod" )
	language.Add( "undone_sc_coremodmed", "Undone Core Mod" )
end

cleanup.Register( "sc_module" )

local MaxPoints = 50

local function GetRemainingPoints(ply)
	if SERVER then
		if not IsValid(ply) then return 0 end
		
		return MaxPoints - (ply:GetInfoNum("sc_coremodmed_cap", 0) + ply:GetInfoNum("sc_coremodmed_capp", 0) + 
			ply:GetInfoNum("sc_coremodmed_shield", 0) + ply:GetInfoNum("sc_coremodmed_shieldp", 0) + ply:GetInfoNum("sc_coremodmed_shieldr", 0) + 
			ply:GetInfoNum("sc_coremodmed_sh_em", 0) + ply:GetInfoNum("sc_coremodmed_sh_ex", 0) + ply:GetInfoNum("sc_coremodmed_sh_th", 0) + ply:GetInfoNum("sc_coremodmed_sh_ki", 0))
	else
		return MaxPoints - (GetConVarNumber("sc_coremodmed_cap") + GetConVarNumber("sc_coremodmed_capp") + GetConVarNumber("sc_coremodmed_capr") + 
			GetConVarNumber("sc_coremodmed_shield") + GetConVarNumber("sc_coremodmed_shieldp") + GetConVarNumber("sc_coremodmed_shieldr") + 
			GetConVarNumber("sc_coremodmed_sh_em") + GetConVarNumber("sc_coremodmed_sh_ex") + GetConVarNumber("sc_coremodmed_sh_th") + GetConVarNumber("sc_coremodmed_sh_ki"))
	end
end

function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end
	
	local ply = self:GetOwner()
	local type = "MED"
	local mods = {CAP=math.Max(ply:GetInfoNum("sc_coremodmed_cap", 0), 0), CAPP=ply:GetInfoNum("sc_coremodmed_capp", 0), 
	SHIELD=math.Max(ply:GetInfoNum("sc_coremodmed_shield", 0), 0), SHIELDP=ply:GetInfoNum("sc_coremodmed_shieldp", 0), SHIELDR=ply:GetInfoNum("sc_coremodmed_shieldr", 0),
	SH_EM=ply:GetInfoNum("sc_coremodmed_sh_em", 0), SH_EX=ply:GetInfoNum("sc_coremodmed_sh_ex", 0), SH_TH=ply:GetInfoNum("sc_coremodmed_sh_th", 0), SH_KI=ply:GetInfoNum("sc_coremodmed_sh_ki", 0)}
	
	for i,k in pairs(mods) do
		if k == 0 then
			mods[i] = nil
		end
	end
	
	if GetRemainingPoints(ply) < 0 then
		ply:ChatPrint("[Space Combat 2] - Using too many points!")
		return false
	end
	
	--Update existing Weapon
	if ( trace.Entity:IsValid() and string.find(trace.Entity:GetClass(), "sc_module") and trace.Entity.Owner == ply ) then
	
		trace.Entity:Setup(type, mods)
		ply:ChatPrint("[Space Combat 2] - Core module was updated!")
		
		return true
	else
		local Ang = trace.HitNormal:Angle() + Angle(90,0,0)
		local Pos = trace.HitPos
		local Core = Make_sc_module(ply, Pos, Ang, type, mods)
			
		local phys = Core:GetPhysicsObject()
		if (phys:IsValid() and trace.Entity:IsValid() ) then
			local weld = constraint.Weld(Core, trace.Entity, 0, trace.PhysicsBone, 0)
			local nocollide = constraint.NoCollide(Core, trace.Entity, 0, trace.PhysicsBone)
			phys:EnableMotion(false)
		end

		undo.Create("sc_module")
			undo.AddEntity( Core )
			undo.SetPlayer( ply )
		undo.Finish()

		ply:AddCleanup( "sc_module", Core )
		
		return true
	end
end

local CPanel = nil
function TOOL:Think()
	if CLIENT then
		if IsValid(CPanel) then
			CPanel.PointLabel:SetText("Points Remaining: "..GetRemainingPoints())
		end
	end
end

function TOOL.BuildCPanel(panel)
	CPanel = panel
	
	CPanel:ClearControls()
	CPanel:SetName("Space Combat 2 - Core Mods")
	
	CPanel.PointLabel = vgui.Create("DLabel", CPanel)
	CPanel.PointLabel:SetText("Points Remaining: 50")
	CPanel:AddItem(CPanel.PointLabel)
	
	CPanel:AddControl("Slider", {
	    Label = "Capacitor x2000",
	    Type = "Int",
	    Min = "0",
	    Max = "60",
	    Command = "sc_coremodmed_cap"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Capacitor % boost",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodmed_capp"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Shield x2000",
	    Type = "Int",
	    Min = "0",
	    Max = "60",
	    Command = "sc_coremodmed_shield"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Shield % boost",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodmed_shieldp"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Shield Recharge",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodmed_shieldr"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Shield EM Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodmed_sh_em"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Shield Explosive Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodmed_sh_ex"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Shield Kinetic Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodmed_sh_ki"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Shield Thermal Resist",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodmed_sh_th"
	})
end