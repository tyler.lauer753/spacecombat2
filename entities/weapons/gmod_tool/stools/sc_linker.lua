TOOL.Category		= "Ship Tools"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#Device Linker"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Information = {
	"left"
}
TOOL.Device         = nil

if CLIENT then
    language.Add( "Tool.sc_linker.name", "Device Linking Tool" )
    language.Add( "Tool.sc_linker.desc", "Links a Space Combat device to a Ship Core." )
    language.Add( "Tool.sc_linker.left", "Select Device/Core" )
end

function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end
	if not trace.Entity:IsValid() then return false end

	if IsValid(self.Device) then
		if trace.Entity.IsNode and (not trace.Entity.CPPICanTool or trace.Entity:CPPICanTool(self:GetOwner())) then
			if self.Device.IsNode then
				self:GetOwner():ChatPrint("[Space Combat 2] - Cannot link a Ship Core to another Ship Core!")
				self.Device = nil
			else
				self:GetOwner():ChatPrint("[Space Combat 2] - Linked Device!")
				self.Device:Link(trace.Entity)
				self.Device = nil
			end
		end
	else
		if trace.Entity.IsLSEnt and (not trace.Entity.CPPICanTool or trace.Entity:CPPICanTool(self:GetOwner())) then
			self:GetOwner():ChatPrint("[Space Combat 2] - Selected Device!")
			self.Device = trace.Entity
		else
			self:GetOwner():ChatPrint("[Space Combat 2] - Device is not a Space Combat 2 device!")
		end
	end
end

function TOOL.BuildCPanel(CPanel)
	CPanel:AddControl("Header", { Text = "#Tool.sc_linker.name", Description = "#Tool.sc_linker.desc" })
end