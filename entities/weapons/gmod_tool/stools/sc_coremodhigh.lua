TOOL.Category		= "Core Mods"
TOOL.Tab			= "Space Combat 2"
TOOL.Name			= "#High Slot"
TOOL.Command		= nil
TOOL.ConfigName		= ""

TOOL.ClientConVar[ "cap" ] = 0
TOOL.ClientConVar[ "capp" ] = 0

if CLIENT then
    language.Add( "Tool.sc_coremodhigh.name", "Space Combat 2 Core Mod Creation Tool" )
    language.Add( "Tool.sc_coremodhigh.desc", "Spawn a core mod." )
    language.Add( "Tool.sc_coremodhigh.0", "Primary: Create/update core mod" )
	language.Add( "undone_sc_coremodhigh", "Undone Core Mod" )
end

cleanup.Register( "sc_module" )

local MaxPoints = 50

local function GetRemainingPoints(ply)
	if SERVER then
		if not IsValid(ply) then return 0 end
		
		return MaxPoints - (ply:GetInfoNum("sc_coremodhigh_cap", 0) + ply:GetInfoNum("sc_coremodhigh_capp", 0))
	else
		return MaxPoints - (GetConVarNumber("sc_coremodhigh_cap") + GetConVarNumber("sc_coremodhigh_capp"))
	end
end

function TOOL:LeftClick(trace)
	if not trace.HitPos then return false end
	if trace.Entity:IsPlayer() then return false end
	if CLIENT then return true end
	
	local ply = self:GetOwner()
	local type = "HIGH"
	local mods = {CAP=math.Max(ply:GetInfoNum("sc_coremodhigh_cap", 0), 0), CAPP=ply:GetInfoNum("sc_coremodhigh_capp", 0)}
	
	for i,k in pairs(mods) do
		if k == 0 then
			mods[i] = nil
		end
	end
	
	if GetRemainingPoints(ply) < 0 then
		ply:ChatPrint("[Space Combat 2] - Using too many points!")
		return false
	end
	
	--Update existing Weapon
	if ( trace.Entity:IsValid() and string.find(trace.Entity:GetClass(), "sc_module") and trace.Entity.Owner == ply ) then
	
		trace.Entity:Setup(type, mods)
		ply:ChatPrint("[Space Combat 2] - Core module was updated!")
		
		return false
	else
		local Ang = trace.HitNormal:Angle() + Angle(90,0,0)
		local Pos = trace.HitPos
		local Core = Make_sc_module(ply, Pos, Ang, type, mods)
		local PhysObj = Core:GetPhysicsObject()
		if not IsValid(PhysObj) then
			ErrorNoHalt("[Space Combat 2] - ERROR: High Core Mod spawned without a valid physics object! What?!\n")
			return
		end
		PhysObj:EnableMotion(false)
			
		local phys = Core:GetPhysicsObject()
		if (phys:IsValid() and trace.Entity:IsValid() ) then
			local weld = constraint.Weld(Core, trace.Entity, 0, trace.PhysicsBone, 0)
			local nocollide = constraint.NoCollide(Core, trace.Entity, 0, trace.PhysicsBone)
		end

		undo.Create("sc_module")
			undo.AddEntity( Core )
			undo.SetPlayer( ply )
		undo.Finish()

		ply:AddCleanup( "sc_module", Core )
		
		return true
	end
end

local CPanel = nil
function TOOL:Think()
	if CLIENT then
		if IsValid(CPanel) then
			CPanel.PointLabel:SetText("Points Remaining: "..GetRemainingPoints())
		end
	end
end

function TOOL.BuildCPanel(panel)
	CPanel = panel
	
	CPanel:ClearControls()
	CPanel:SetName("Space Combat 2 - Core Mods")
	
	CPanel.PointLabel = vgui.Create("DLabel", CPanel)
	CPanel.PointLabel:SetText("Points Remaining: 50")
	CPanel:AddItem(CPanel.PointLabel)
	
	CPanel:AddControl("Slider", {
	    Label = "Capacitor x2000",
	    Type = "Int",
	    Min = "0",
	    Max = "60",
	    Command = "sc_coremodhigh_cap"
	})
	
	CPanel:AddControl("Slider", {
	    Label = "Capacitor % boost",
	    Type = "Int",
	    Min = "-20",
	    Max = "60",
	    Command = "sc_coremodhigh_capp"
	})
end