TOOL.Category		= "Constraints"
TOOL.Name			= "Multi-NoCollide"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Information = {
	"left",
	"right",
	{ 
		name  = "shift_left",
		icon2  = "gui/e.png",
		icon = "gui/lmb.png",
		
	},
	"reload"
}

if ( CLIENT ) then
    language.Add( "Tool.multi_NoCollide.name", "Multi-NoCollide Tool" )
    language.Add( "Tool.multi_NoCollide.desc", "NoCollide multiple props to one prop." )
    language.Add( "Tool.multi_NoCollide.left", "Selects an entity to NoCollide" )
    language.Add( "Tool.multi_NoCollide.shift_left", "Selects all entities in a 2048 unit radius." )
    language.Add( "Tool.multi_NoCollide.right", "NoCollide all selected entities to this prop." )
    language.Add( "Tool.multi_NoCollide.reload", "Clear Targets." )
end

TOOL.enttbl = {}
function TOOL:LeftClick( trace )
	local ply = self:GetOwner()
	if CLIENT then return true end
	if IsValid(trace.Entity) and trace.Entity:IsPlayer() then return end
	if not util.IsValidPhysicsObject(trace.Entity, trace.PhysicsBone) then return false end
	if trace.Entity:IsWorld() then return false end
	
	local ent = trace.Entity
	if self:GetOwner():KeyDown(IN_USE) then
		for k,v in pairs(ents.FindInSphere(trace.HitPos, 2048)) do
			if IsValid(v) and not v:IsPlayer() and not v:IsWeapon() and self:IsPropOwner(ply, v) and v:CPPICanTool(ply) then
				local eid = v:EntIndex()
				if not (self.enttbl[eid]) then
					local col = v:GetColor()
					self.enttbl[eid] = col
					v:SetColor(Color(0,255,0,255))
				else
					local col = self.enttbl[eid]
					v:SetColor(col)
					self.enttbl[eid] = nil
				end
			end
		end
	else
		if ent:CPPICanTool(ply) then
			local eid = ent:EntIndex()
			if not self.enttbl[eid] then
				local col = ent:GetColor()
				self.enttbl[eid] = col
				ent:SetColor(Color(0,255,0,255))
			else
				local col = self.enttbl[eid]
				ent:SetColor(col)
				self.enttbl[eid] = nil
			end
		end
	end
	
	return true
end


function TOOL:RightClick( trace )
	if CLIENT then return true end
	if table.Count(self.enttbl) < 1 then return end
	if IsValid(trace.Entity) and trace.Entity:IsPlayer() then return end
	if not util.IsValidPhysicsObject(trace.Entity, trace.PhysicsBone) then return false end
	if trace.Entity:IsWorld() then return false end
	
	local ent = trace.Entity
	for k,v in pairs(self.enttbl) do
		local prop = ents.GetByIndex(k)
		if IsValid(prop) then
			prop:SetColor(v)
			constraint.NoCollide(prop,ent,0,0)
			self.enttbl[k] = nil
		end
	end
	self.enttbl = {}
	return true
end

function TOOL:SendMessage( message )
	self:GetOwner():PrintMessage( HUD_PRINTCENTER, message )
end

function TOOL:Reload()
	if CLIENT then return false end
	if table.Count(self.enttbl) < 1 then return end
	for k,v in pairs(self.enttbl) do
		local prop = ents.GetByIndex(k)
		if (prop:IsValid()) then
			prop:SetColor(v)
			self.enttbl[k] = nil
		end
	end
	self.enttbl = {}
	return true
end

function TOOL:IsPropOwner(ply, ent)
	if ent:CPPIGetOwner() == ply then return true end
	return false
end
