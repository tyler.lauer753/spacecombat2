--[[*******************************************
	Scalable Bertha Launch
		Something to fit the charge
		effect's flashiness.
	
	Author: Steeveeo
********************************************]]--

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.Emitter = ParticleEmitter(self.Pos)
	
	self.Lifetime = 6
	self.Duration = 0
	self.Dead = false
	
	--Flare Core
	for i=1, 2 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(2, 2.75) * self.Scale, 0.1, 3))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(350 * self.Scale)
		p:SetEndSize(150)
		p:SetColor(255, 255, 255)
	end
	
	--Flare Rays
	for i=1, 5 do
		local p = self.Emitter:Add("effects/flares/light-rays_001_ignorez", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(3, 3.65) * self.Scale, 0.1, 4))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(1500 * self.Scale)
		p:SetEndSize(150)
		p:SetColor(200, 255, 200)
	end
	
	--Flare Oh God My Eyes
	for i=1, 5 do
		local p = self.Emitter:Add("effects/flares/halo-flare_001_ignorez", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(1, 2) * self.Scale, 0.1, 2.5))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(0)
		p:SetEndSize(35000 * self.Scale)
		p:SetColor(200, 255, 200)
	end
	
	--SPARKS!
	for i=1, 75 * self.Scale do
		local p = self.Emitter:Add("effects/shipsplosion/sparks_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Rand(1.5, 2.5))
		p:SetVelocity((self.Normal + (VectorRand() * 0.75)) * math.Rand(10, 125))
		p:SetAirResistance(125)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(0)
		p:SetEndSize(math.random(500, 750) * self.Scale)
		p:SetStartLength(0)
		p:SetEndLength(math.random(1250, 3500) * self.Scale)
		p:SetColor(200, 255, 200)
	end
	
	--Fire
	for i=1, 40 * self.Scale do
		local velocity = (self.Normal + (VectorRand() * 0.6)):GetNormalized() * (math.Rand(350, 4500) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(5, 12) * self.Scale, 0.1, 15))
		p:SetVelocity(velocity)
		p:SetAirResistance(64)
		p:SetStartAlpha(16)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(125, 350) * self.Scale)
		p:SetEndSize(math.random(575, 1500) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.1, 0.1))
		p:SetColor(100, 255, 200)
	end
	
	--Plasma-y Smoke
	for i=1, 25 * self.Scale do
		local velocity = (self.Normal + (VectorRand() * 0.55)):GetNormalized() * (math.Rand(500, 4250) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1, 4) .. "add", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(5, 12) * self.Scale, 0.1, 15))
		p:SetVelocity(velocity)
		p:SetAirResistance(64)
		p:SetStartAlpha(8)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(125, 350) * self.Scale)
		p:SetEndSize(math.random(575, 1500) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.1, 0.1))
		p:SetColor(math.random(96, 104), math.random(240, 255), math.random(144,196))
	end
	
	--Shake Screen
	if GetConVar("sc_doScreenShakeFX"):GetBool() then
		util.ScreenShake(LocalPlayer():GetPos(), 15, 1, 3, 4096)
	end
	
	--Spawn shocky bits randomly until sound ends
	timer.Create("BerthaPfwoosh" .. CurTime(), 0.01, 500, function()
		--Clean up Emitter
		if IsValid(self) and not self.Dead then
			if self.Duration > 2.5 then
				--Random Chance based on time after explosion
				math.randomseed(CurTime())
				local rand = math.random(1, 10)
				local range = 10 - (10 * (self.Duration / self.Lifetime))
				
				if rand < range then
					for i = 1, math.random(1, 3 * range) do
						timer.Simple(math.Rand(0, 1), function()
							if not IsValid(self) then return end
							
							local pos = (self.Normal + (VectorRand() * 0.65)):GetNormalized() * (math.Rand(350, 3500) * self.Scale)
							local p = self.Emitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), self.Pos + pos)
							p:SetDieTime(math.Rand(0.025, 0.1))
							p:SetStartAlpha(255)
							p:SetEndAlpha(0)
							p:SetStartSize(math.Rand(25, 175) * self.Scale)
							p:SetEndSize(p:GetStartSize() + math.Rand(25, 75) * self.Scale)
							p:SetColor(220, 255, 255)
							p:SetRoll(math.Rand(0, 360))
						end)
					end
				end
			end
		end
	end) --End Shocky Bits
end

function EFFECT:Think( )
	if self.Duration < self.Lifetime then 
		local frameTime = FrameTime()
		
		--Increment Effect Timer
		self.Duration = self.Duration + frameTime
	else
		self.Dead = true
		self.Emitter:Finish()
		
		return false
	end
	return true
end
function EFFECT:Render( )
	return false
end
