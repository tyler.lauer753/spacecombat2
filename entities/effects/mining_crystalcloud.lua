--[[*********************************************
	Variable Ominous Crystal Glow
	
	Author: Steeveeo
**********************************************]]--

function EFFECT:Init( data )

	self.Parent = data:GetEntity()
    if not IsValid(self.Parent) then return end

	self.CrysType = self.Parent:GetCrysType()
	self.Emitter = ParticleEmitter(self.Parent:GetPos())
	
	self.SpriteMat = Material("effects/shipsplosion/fire_001")
	self.SpriteMat:SetFloat("$overbrightfactor", 15.0)
	
	self.NextEmit = CurTime()
	self.NextChildEmit = CurTime()
	
	----------------------
	-- Particle Settings
	----------------------
	
	--Default Green
	self.Col = Color(0, 255, 0, 64)
	
	--Blue
	if self.CrysType == 2 then
		self.Col = Color(0, 0, 255, 255)
	--Red
	elseif self.CrysType == 3 then
		self.Col = Color(255, 0, 0, 64)
	end
end

function EFFECT:Think( )
	if self.Emitter == nil then return false end --Garry why...

	if not IsValid(self.Parent) then
		self.Emitter:Finish()
		return false
	end

    if self.Parent:GetCrysType() ~= self.CrysType then
        self.CrysType = self.Parent:GetCrysType()

        --Default Green
	    self.Col = Color(0, 255, 0, 64)
	
	    --Blue
	    if self.CrysType == 2 then
		    self.Col = Color(0, 0, 255, 255)
	    --Red
	    elseif self.CrysType == 3 then
		    self.Col = Color(255, 0, 0, 64)
	    end
    end
	
	math.randomseed(SysTime())
	local maxDist = GetConVarNumber("mining_mineralParticleViewDist")
	maxDist = maxDist * maxDist
	
	--Get Camera
	local viewEnt = LocalPlayer()
	if IsValid(viewEnt) then
		if IsValid(LocalPlayer():GetViewEntity()) then
			viewEnt = LocalPlayer():GetViewEntity()
		end
	end
	
	if CurTime() > self.NextEmit then
		local pos = self.Parent:GetPos() + (self.Parent:GetUp() * math.Rand(10, 200))
		
		if pos:DistToSqr(viewEnt:GetPos()) < maxDist then
			local p = self.Emitter:Add(self.SpriteMat, pos)
			p:SetDieTime(math.Rand(10, 15))
			p:SetVelocity(VectorRand() * 5)
			p:SetStartAlpha(self.Col.a)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(math.random(150, 250))
			p:SetColor(self.Col.r, self.Col.g, self.Col.b)
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.25, 0.25))
		end
		
		self.NextEmit = CurTime() + 1
	end
	
	if CurTime() > self.NextChildEmit then
		local children = self.Parent:GetCrysChildren({})
		local numParticles = math.ceil(#children * 0.1)
		
		for i = 1, numParticles do
			local child = table.Random(children)
			
			if IsValid(child) then
				local pos = child:GetPos() + (child:GetUp() * math.Rand(10, 60))
				
				if pos:DistToSqr(viewEnt:GetPos()) < maxDist then
					local p = self.Emitter:Add(self.SpriteMat, pos)
					p:SetDieTime(math.Rand(8, 13))
					p:SetVelocity(VectorRand() * 5)
					p:SetStartAlpha(self.Col.a)
					p:SetEndAlpha(0)
					p:SetStartSize(0)
					p:SetEndSize(math.random(50, 125))
					p:SetColor(self.Col.r, self.Col.g, self.Col.b)
					p:SetRoll(math.Rand(0, 360))
					p:SetRollDelta(math.Rand(-0.25, 0.25))
				end
			end
		end
		
		self.NextChildEmit = CurTime() + 0.25
	end
	
	return true
end


function EFFECT:Render( )
	
	
	return false
					 
end
