/**********************************************
	Scalable Railgun Hit Effect
	
	Author: Steeveeo
***********************************************/

util.PrecacheSound( "ambient/explosions/explode_1.wav" )
util.PrecacheSound( "ambient/explosions/explode_3.wav" )
util.PrecacheSound( "ambient/explosions/explode_4.wav" )

function EFFECT:Init( data )

	self.Sounds = {
		"ambient/explosions/explode_1.wav",
		"ambient/explosions/explode_3.wav",
		"ambient/explosions/explode_4.wav"
	}
	
	//Particle Settings
	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.DieTime = CurTime() + 0.5
	self.Emitter = ParticleEmitter(self.Pos)
	
	--Setup Spark Trails
	self.Trails = {}
	local dir = self.Normal:Angle():Right()
	local ang = dir:Angle()
	for i = 1, 10 do
		ang:RotateAroundAxis(self.Normal, 360 / 10)
		
		local trail = {}
		trail.DieTime = CurTime() + math.Rand(0.15, 0.5)
		trail.Pos = self.Pos + (ang:Forward() * 350 * self.Scale)
		
		local velocity = ang:Forward() * (math.Rand(250, 275) * self.Scale) + (self.Normal * 10)
		trail.Velocity = (self.Normal + (ang:Forward() * 0.35)):GetNormalized() * (math.Rand(3000, 5000) * self.Scale)
		
		self.Trails[i] = trail
	end
	
	--Flare
	for i=1, 2 do
		local p = self.Emitter:Add("effects/flares/halo-flare_001_ignorez", self.Pos)
		p:SetDieTime(math.Rand(1, 2))
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(math.random(7500, 10000) * self.Scale)
		p:SetEndSize(0)
		p:SetColor(255, 225, 196)
	end
	
	--Play Sound
	math.randomseed(SysTime())
	sound.Play(self.Sounds[math.random(1,#self.Sounds)], self.Pos)
end

function EFFECT:Think( )
  	if CurTime() > self.DieTime then
		self.Emitter:Finish()
		return false
	else
		//Draw Trails
		for i = 1, #self.Trails do
			local trail = self.Trails[i]
			
			if CurTime() < trail.DieTime then
				--Smoke
				local velocity = (trail.Velocity * math.Rand(-0.1, 0.75)) + (VectorRand() * math.Rand(150, 300))
			
				local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), trail.Pos)
				p:SetDieTime(math.Rand(5, 7))
				p:SetVelocity(velocity)
				p:SetAirResistance(350)
				p:SetStartAlpha(255)
				p:SetEndAlpha(0)
				p:SetStartSize(math.random(100, 200) * self.Scale * ((trail.DieTime - CurTime()) / 0.25))
				p:SetEndSize(math.random(400, 500) * self.Scale)
				p:SetRoll(math.Rand(0,360))
				p:SetRollDelta(math.Rand(-0.5, 0.5))
				local grey = math.random(0,64)
				p:SetColor(grey, grey, grey)
				
				--Fire
				local velocity = (trail.Velocity * math.Rand(-0.1, 0.75)) + (VectorRand() * math.Rand(150, 300))
			
				local p = self.Emitter:Add("effects/shipsplosion/fire_001", trail.Pos)
				p:SetDieTime(math.Rand(1, 2))
				p:SetVelocity(velocity)
				p:SetAirResistance(350)
				p:SetStartAlpha(255)
				p:SetEndAlpha(0)
				p:SetStartSize(math.random(300, 400) * self.Scale * ((trail.DieTime - CurTime()) / 0.25))
				p:SetEndSize(math.random(400, 500) * self.Scale)
				p:SetRoll(math.Rand(0,360))
				p:SetRollDelta(math.Rand(-0.5, 0.5))
				p:SetColor(255, 240, 225)
			
				trail.Pos = trail.Pos + (trail.Velocity * FrameTime())
			end
		end
		
		return true
	end
end


function EFFECT:Render( )
	return false
end
