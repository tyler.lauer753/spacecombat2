--[[*********************************************
	Scalable Dusty Earthquake Effect
	(for before crystals spawn)
	
	Author: Steeveeo
**********************************************]]--

function EFFECT:Init( data )
	self.Normal = data:GetNormal()
	self.Pos = data:GetOrigin()
	self.Scale = data:GetScale()
	self.LifeTime = data:GetMagnitude()
	self.Life = 0
	self.Emitter = ParticleEmitter(self.Pos)
	
	self.NextEmit = CurTime()
end

function EFFECT:Think( )
	if self.Emitter == nil then return false end --Garry why...
	
	self.Life = self.Life + FrameTime()
	if self.Life > self.LifeTime then
		self.Emitter:Finish()
		return false
	end
	
	if CurTime() > self.NextEmit then
		local dir = self.Normal:Angle():Right()
		local ang = dir:Angle()
		local ringParticles = 5
		for i = 1, ringParticles do
			ang:RotateAroundAxis(self.Normal, math.Rand(0, 360))
			local velocity = self.Normal * (15 * self.Scale)
			local offset = ang:Forward() * (math.Rand(0, 50) * self.Scale) - (self.Normal * (30 * self.Scale))
			
			local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos + offset)
			p:SetDieTime(math.Rand(1, 3))
			p:SetVelocity(velocity)
			p:SetStartAlpha(64)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(1, 5) * self.Scale)
			p:SetEndSize(math.random(35, 50) * self.Scale)
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.5, 0.5))
			p:SetColor(212, 196, 181)
		end
		
		self.NextEmit = CurTime() + 0.1
	end
	
	return true
end

function EFFECT:Render( )
	return false
end
