/**********************************************
	Scalable Railgun Fire Effect
	
	Author: Steeveeo
***********************************************/

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.Entity = data:GetEntity()
	self.DieTime = CurTime() + 0.75
	self.ElectricalDelay = CurTime() + 0.25
	self.Emitter = ParticleEmitter(self.Pos)
	
	//Sparks
	for i=1, 25 do
		local velocity = (self.Normal + (VectorRand() * 0.35)):GetNormalized() * (math.Rand(0, 1250) * self.Scale)
		
		local p = self.Emitter:Add("effects/flares/light-rays_001", self.Pos)
		p:SetDieTime(math.Rand(0.75, 2))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(math.random(15, 20) * self.Scale)
		p:SetEndSize(0)
	end
	
	//Flare
	for i=1, 2 do
		local p = self.Emitter:Add("effects/flares/halo-flare_001", self.Pos)
		p:SetDieTime(math.Rand(0.25, 0.35))
		p:SetStartAlpha(255)
		p:SetEndAlpha(255)
		p:SetStartSize(350 * self.Scale)
		p:SetEndSize(0)
		p:SetColor(255, 255, 200)
	end
end

function EFFECT:Think( )
	if CurTime() < self.DieTime then
		if CurTime() > self.ElectricalDelay and IsValid(self.Entity) then
			//Electrical Effect
			local mins = self.Entity:OBBMins()
			local maxs = self.Entity:OBBMaxs()
			local pos = Vector(math.Rand(mins.x, maxs.x), math.Rand(mins.y, maxs.y), math.Rand(mins.z, maxs.z))
			local p = self.Emitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), self.Entity:LocalToWorld(pos))
			p:SetDieTime(math.Rand(0.25, 0.5))
			p:SetVelocity(self.Entity:GetVelocity())
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(math.random(10, 20) * self.Scale)
			p:SetRoll(math.Rand(0, 360))
			p:SetColor(200, 220, 255)
		end
		return true
	else
		self.Emitter:Finish()
		return false
	end
end

function EFFECT:Render( )
	return false
end
