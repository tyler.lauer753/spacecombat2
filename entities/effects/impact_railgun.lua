/**********************************************
	Scalable Railgun Hit Effect
	
	Author: Steeveeo
***********************************************/

util.PrecacheSound( "ambient/explosions/explode_1.wav" )
util.PrecacheSound( "ambient/explosions/explode_3.wav" )
util.PrecacheSound( "ambient/explosions/explode_4.wav" )

function EFFECT:Init( data )

	self.Sounds = {
		"ambient/explosions/explode_1.wav",
		"ambient/explosions/explode_3.wav",
		"ambient/explosions/explode_4.wav"
	}
	
	//Particle Settings
	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.DieTime = CurTime() + 0.5
	self.Emitter = ParticleEmitter(self.Pos)
	
	--Setup Spark Trails
	self.Trails = {}
	for i = 1, 5 do
		local trail = {}
		trail.DieTime = CurTime() + math.Rand(0.15, 0.25)
		trail.Velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * (math.Rand(3000, 5000) * self.Scale)
		trail.Pos = self.Pos
		
		self.Trails[i] = trail
	end
	
	--Flare
	for i=1, 5 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Rand(0.35, 0.75))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(750, 1000) * self.Scale)
		p:SetEndSize(0)
		p:SetColor(255, 225, 196)
	end
	
	--Sparks Long
	for i=1, math.Clamp(15 * (self.Scale), 5, 30) do
		local velocity = (self.Normal + (VectorRand() * 0.5)):GetNormalized() * (math.Rand(500, 750) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/sparks_002", self.Pos)
		p:SetDieTime(math.Rand(0.5, 0.65))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartLength(math.Rand(50, 75) * self.Scale)
		p:SetEndLength(p:GetStartLength() + (math.Rand(1000, 1250) * self.Scale))
		p:SetStartSize(math.Rand(250, 500) * self.Scale)
		p:SetEndSize(p:GetStartSize() + (math.Rand(250, 350) * self.Scale))
		p:SetRoll(math.Rand(0, 360))
		p:SetColor(255, 220, 200)
	end
	
	--Play Sound
	math.randomseed(SysTime())
	sound.Play(self.Sounds[math.random(1,#self.Sounds)], self.Pos)
end

function EFFECT:Think( )
  	if CurTime() > self.DieTime then
		self.Emitter:Finish()
		return false
	else
		//Draw Trails
		for i = 1, #self.Trails do
			local trail = self.Trails[i]
			
			if CurTime() < trail.DieTime then
				--Sparks
				local velocity = (trail.Velocity * math.Rand(-0.1, 0.75)) + (VectorRand() * math.Rand(150, 300))
			
				local p = self.Emitter:Add("effects/flares/light-rays_001", trail.Pos)
				p:SetDieTime(math.Rand(0.75, 1.5))
				p:SetVelocity(velocity)
				p:SetAirResistance(150)
				p:SetStartAlpha(255)
				p:SetEndAlpha(255)
				p:SetStartSize(math.random(300, 400) * self.Scale * ((trail.DieTime - CurTime()) / 0.25))
				p:SetEndSize(0)
				p:SetColor(255, 240, 225)
			
				trail.Pos = trail.Pos + (trail.Velocity * FrameTime())
			end
		end
		
		return true
	end
end


function EFFECT:Render( )
	return false
end
