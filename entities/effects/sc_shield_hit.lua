--[[*********************************************
	Adaptive Shield Hit Effect
	
	Author: Steeveeo
**********************************************]]--

--Parent a particle to an ent
local function parentParticle( particle, ent, pos, angle )
	if not IsValid(ent) then return end
	
	local parentData = {}
	
	parentData.Entity = ent
	parentData.Pos = ent:WorldToLocal(pos)
	parentData.Angle = ent:WorldToLocalAngles(angle)
	
	particle.parentData = parentData
end

--Think function for particles (local up here so I don't have to copy this three trillion times)
local function particleThink( particle )
	
	--Realign to parent
	if particle.parentData then
		local ent = particle.parentData.Entity
		local pos = particle.parentData.Pos
		local angle = particle.parentData.Angle
		
		if IsValid(ent) then
			particle:SetPos(ent:LocalToWorld(pos))
			particle:SetAngles(ent:LocalToWorldAngles(angle))
		end
	end
	
	particle:SetNextThink(CurTime())
end

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Dir = data:GetNormal() or Vector(0,0,0)
	self.Scale = data:GetScale()
	self.Entity = data:GetEntity()
	self.Type = data:GetMagnitude()
	self.Emitter = ParticleEmitter( self.Pos )
	self.PlaneEmitter = ParticleEmitter( self.Pos, true )
	
	--Basic Shield Splash
	for i=1, 2 do
		local pos = self.Pos + self.Dir * 2
		local ang = self.Dir:Angle() + Angle(0, 0, math.Rand(0, 360))
		
		local splash = self.PlaneEmitter:Add("particle/particle_glow_04", pos )
		splash:SetDieTime( math.Rand(0.1, 0.25) * (1 + math.log(1 + self.Scale)) )
		splash:SetStartSize( 0 )
		splash:SetEndSize( 75 * self.Scale )
		splash:SetStartAlpha( 255 )
		splash:SetEndAlpha( 0 )
		splash:SetColor(200, 255, 255)
		
		if IsValid(self.Entity) then
			splash:SetVelocity(self.Entity:GetVelocity())
		end
		
		splash:SetAngles( ang )
		splash:SetThinkFunction(particleThink)
		splash:SetNextThink(CurTime())
		
		parentParticle(splash, self.Entity, pos, ang)
	end
	
	---------------------------
	-- Type-Based Effects
	--[[
		Other = Type 0
		Kinetic = Type 1
		Thermal = Type 2
		Explosive = Type 3
		EMP = Type 4
	]]--------------------------
	
	--Kinetic Hit
	if self.Type == 1 then
		for i=1, self.Scale do
			local pos = self.Pos + self.Dir * 2
			local ang = self.Dir:Angle() + Angle(0, 0, math.Rand(0, 360))
			
			local splash = self.PlaneEmitter:Add("effects/shipsplosion/shockwave_001", pos)
			splash:SetDieTime( math.Rand(0.1, 0.35) * (1 + math.log(1 + self.Scale)) )
			splash:SetStartSize( 0 )
			splash:SetEndSize( 75 * self.Scale )
			splash:SetStartAlpha( 255 )
			splash:SetEndAlpha( 0 )
			splash:SetColor(200, 255, 255)
		
			if IsValid(self.Entity) then
				splash:SetVelocity(self.Entity:GetVelocity())
			end
			
			splash:SetAngles(ang)
			splash:SetThinkFunction(particleThink)
			splash:SetNextThink(CurTime())
		
			parentParticle(splash, self.Entity, pos, ang)
		end
	
	--Thermal Hit
	elseif self.Type == 2 then
		--Hot Splash
		for i=1, 1 + (self.Scale * 0.25) do
			local pos = self.Pos + self.Dir * 2
			local ang = self.Dir:Angle() + Angle(0, 0, math.Rand(0, 360))
			
			local splash = self.PlaneEmitter:Add("effects/shipsplosion/smoke_004add", pos)
			splash:SetDieTime( math.Rand(0.1, 0.35) * (1 + math.log(1 + self.Scale)) )
			splash:SetStartSize( 35 * self.Scale )
			splash:SetEndSize( 50 * self.Scale )
			splash:SetStartAlpha( 255 )
			splash:SetEndAlpha( 0 )
			splash:SetColor(255, 144, 0)
		
			if IsValid(self.Entity) then
				splash:SetVelocity(self.Entity:GetVelocity())
			end
			
			splash:SetAngles(ang)
			splash:SetThinkFunction(particleThink)
			splash:SetNextThink(CurTime())
		
			parentParticle(splash, self.Entity, pos, ang)
		end
		
		--Sparks
		for i=1, math.random(1, 3) * self.Scale do
			local p = self.Emitter:Add("effects/shipsplosion/smoke_004add", self.Pos)
			p:SetDieTime( math.Rand(0.5, 0.75) )
			p:SetVelocity( self.Entity:GetVelocity() + (VectorRand() * 0.25):GetNormalized() * (math.Rand(80, 100) * (1 + math.log(1 + self.Scale))) )
			p:SetStartAlpha( 255 )
			p:SetEndAlpha( 0 )
			p:SetStartSize( math.random(0.75, 0.85) * self.Scale )
			p:SetEndSize( 0 )
			p:SetStartLength( math.random(2, 7) * self.Scale )
			p:SetEndLength( math.random(5, 7) * self.Scale )
			p:SetColor( 255, 220, 200 )
			p:SetRoll( math.Rand(0, 360) )
			p:SetRollDelta( math.Rand(-0.25, 0.25) )
		end
	
	--Explosive Hit
	elseif self.Type == 3 then
	
		local dieTime = math.Rand(0.25, 0.5) * (1 + math.log(1 + self.Scale))
		
		--Shock Inner
		for i=1, 5 do
			local pos = self.Pos + self.Dir * 2
			local ang = self.Dir:Angle() + Angle(0, 0, math.Rand(0, 360))
			
			local splash = self.PlaneEmitter:Add("effects/shipsplosion/shockwave_001", pos)
			splash:SetDieTime( dieTime * math.Rand(0.65, 1) )
			splash:SetStartSize( 0 )
			splash:SetEndSize( 150 * self.Scale )
			splash:SetStartAlpha( 255 )
			splash:SetEndAlpha( 0 )
			splash:SetColor( 200, 255, 255 )
		
			if IsValid(self.Entity) then
				splash:SetVelocity(self.Entity:GetVelocity())
			end
			
			splash:SetAngles( ang )
			splash:SetThinkFunction(particleThink)
			splash:SetNextThink(CurTime())
		
			parentParticle(splash, self.Entity, pos, ang)
		end
		
		--Shock Outer
		for i=1, 2 do
			local pos = self.Pos + self.Dir * 2
			local ang = self.Dir:Angle() + Angle(0, 0, math.Rand(0, 360))
			
			local splash = self.PlaneEmitter:Add("effects/shipsplosion/shockwave_001", pos)
			splash:SetDieTime( dieTime )
			splash:SetStartSize( 0 )
			splash:SetEndSize( 150 * self.Scale )
			splash:SetStartAlpha( 255 )
			splash:SetEndAlpha( 0 )
			splash:SetColor( 200, 255, 255 )
		
			if IsValid(self.Entity) then
				splash:SetVelocity(self.Entity:GetVelocity())
			end
			
			splash:SetAngles( ang )
			splash:SetThinkFunction( particleThink )
			splash:SetNextThink( CurTime() )
		
			parentParticle(splash, self.Entity, pos, ang)
		end
		
		--Hot Splash
		for i=1, 1 + (self.Scale * 0.25) do
			local pos = self.Pos + self.Dir * 2
			local ang = self.Dir:Angle() + Angle(0, 0, math.Rand(0, 360))
			
			local splash = self.PlaneEmitter:Add("effects/shipsplosion/smoke_004add", pos)
			splash:SetDieTime( math.Rand(0.1, 0.35) * (1 + math.log(1 + self.Scale)) )
			splash:SetStartSize( 1 * self.Scale )
			splash:SetEndSize( 50 * self.Scale )
			splash:SetStartAlpha( 255 )
			splash:SetEndAlpha( 0 )
			splash:SetColor( 200, 255, 255 )
		
			if IsValid(self.Entity) then
				splash:SetVelocity(self.Entity:GetVelocity())
			end
			
			splash:SetAngles( ang )
			splash:SetThinkFunction( particleThink )
			splash:SetNextThink( CurTime() )
		
			parentParticle(splash, self.Entity, pos, ang)
		end
	
	--EM Hit
	elseif self.Type == 4 then
		--Shocky Particles
		for i=1, math.random(3, 5 * self.Scale) do
			local pos = self.Pos + self.Dir * 2
			local ang = self.Dir:Angle() + Angle(0, 0, math.Rand(0, 360))
			
			local splash = self.PlaneEmitter:Add("effects/shipsplosion/lightning_00" .. math.random(1,4), pos)
			splash:SetDieTime( math.Rand(0.2, 0.5) )
			splash:SetStartSize( 0 )
			splash:SetEndSize( 50 * self.Scale )
			splash:SetStartAlpha( 64 )
			splash:SetEndAlpha( 0 )
			splash:SetColor( 225, 255, 255 )
		
			if IsValid(self.Entity) then
				splash:SetVelocity(self.Entity:GetVelocity())
			end
			
			splash:SetAngles( ang )
			splash:SetThinkFunction( particleThink )
			splash:SetNextThink( CurTime() )
		
			parentParticle(splash, self.Entity, pos, ang)
		end
	
	--Other
	else
		--TODO: What do?
	end
	
	self.Emitter:Finish()
	self.PlaneEmitter:Finish()
end

function EFFECT:Think()
	return false
end

function EFFECT:Render()
	return false
end
