--[[*********************************************
	Large Hull Explosion Effect w/ Fire Jet
	
	Author: Steeveeo
**********************************************]]--

local sound_Explosions = {}

sound_Explosions[1] = {}
sound_Explosions[1].Near = Sound("shipsplosion/sc_explode_01_near.mp3")
sound_Explosions[1].Mid = Sound("shipsplosion/sc_explode_01_mid.mp3")
sound_Explosions[1].Far = Sound("shipsplosion/sc_explode_01_far.mp3")

sound_Explosions[2] = {}
sound_Explosions[2].Near = Sound("shipsplosion/sc_explode_02_near.mp3")
sound_Explosions[2].Mid = Sound("shipsplosion/sc_explode_02_mid.mp3")
sound_Explosions[2].Far = Sound("shipsplosion/sc_explode_02_far.mp3")

sound_Explosions[3] = {}
sound_Explosions[3].Near = Sound("shipsplosion/sc_explode_03_near.mp3")
sound_Explosions[3].Mid = Sound("shipsplosion/sc_explode_03_mid.mp3")
sound_Explosions[3].Far = Sound("shipsplosion/sc_explode_03_far.mp3")

sound_Explosions[4] = {}
sound_Explosions[4].Near = Sound("shipsplosion/sc_explode_04_near.mp3")
sound_Explosions[4].Mid = Sound("shipsplosion/sc_explode_04_mid.mp3")
sound_Explosions[4].Far = Sound("shipsplosion/sc_explode_04_far.mp3")

function EFFECT:Init( data )
	self.Normal = data:GetNormal()
	self.Parent = data:GetEntity()
	
	if not IsValid(self.Parent) then return false end
	
	self.Pos = self.Parent:WorldToLocal(data:GetOrigin())
	self.LifeTime = 0.5
	self.Emitter = ParticleEmitter(self.Parent:LocalToWorld(self.Pos))
	
	self.Dist = LocalPlayer():GetPos():Distance(data:GetOrigin())
	self.FarDist = GetConVar("sc_shipDeathSoundRadius_Max"):GetFloat()
	self.MidDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_MidFactor"):GetFloat()
	self.NearDist = self.FarDist * GetConVar("sc_shipDeathSoundRadius_NearFactor"):GetFloat()
	
	--Fire Jet
	timer.Create("sc_largesplode_" .. SysTime(), 0.05, self.LifeTime * 20, function()
		if !IsValid(self.Parent) then return end
		
		for i=1, 5 do
			local velocity = (self.Normal + (VectorRand() * 0.45)):GetNormalized() * math.Rand(750, 3500)
			
			local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Parent:LocalToWorld(self.Pos))
			p:SetDieTime(math.Rand(0.35, 0.65))
			p:SetVelocity(velocity)
			p:SetAirResistance(25)
			p:SetStartAlpha(255)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(400, 450))
			p:SetEndSize(math.random(650, 800))
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.25, 0.25))
		end
		
		local velocity = (self.Normal + (VectorRand() * 0.45)):GetNormalized() * math.Rand(750, 3500)
		
		local p = self.Emitter:Add("effects/flares/light-rays_001", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(0.35, 0.65))
		p:SetVelocity(velocity)
		p:SetAirResistance(25)
		p:SetStartAlpha(128)
		p:SetEndAlpha(0)
		p:SetStartSize(0)
		p:SetEndSize(7500)
		p:SetColor(255, 196, 144)
	end)
	
	--Fire Main
	for i=1, 30 do
		local velocity = VectorRand() * math.Rand(200, 1500)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(4, 6))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(200, 450))
		p:SetEndSize(math.random(500, 800))
		p:SetRoll(math.Rand(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
	end
	
	--Fire Highlights
	for i=1, 20 do
		local velocity = VectorRand() * math.Rand(200, 1500)
		
		local p = self.Emitter:Add("particles/flamelet" .. math.random(1,5), self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(4, 6))
		p:SetVelocity(velocity)
		p:SetAirResistance(75)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(200, 450))
		p:SetEndSize(math.random(500, 800))
		p:SetRoll(math.random(-360, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		p:SetColor(96, 96, 255)
	end
	
	--Flare
	for i=1, 5 do
		local p = self.Emitter:Add("effects/flares/halo-flare_001_ignorez", self.Parent:LocalToWorld(self.Pos))
		p:SetDieTime(math.Rand(1, 1.5))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(5000)
		p:SetEndSize(1250)
		p:SetColor(255, 144, 0)
	end
	
	--Play Sound
	math.randomseed(CurTime())
	local sound = sound_Explosions[math.random(1, #sound_Explosions)]
	if self.Dist < self.NearDist then
		LocalPlayer():EmitSound(sound.Near, 511, 50, 1)
	elseif self.Dist < self.MidDist then
		LocalPlayer():EmitSound(sound.Mid, 511, 50, 1)
	elseif self.Dist < self.FarDist then
		LocalPlayer():EmitSound(sound.Far, 511, 50, 1)
	end
	
	--Shake Screen
	if GetConVar("sc_doScreenShakeFX"):GetBool() then
		util.ScreenShake(self.Pos, 3, 0.1, 0.5, 4096)
	end
end

function EFFECT:Think( )
	if self.Emitter == nil then return false end
	if not IsValid(self.Parent) then
		self.Emitter:Finish()
		return false
	end
	
  	self.LifeTime = self.LifeTime - FrameTime()
	if self.LifeTime <= 0 then
		self.Emitter:Finish()
		return false
	end
	
	return true
end

function EFFECT:Render( )
	return false
end
