/**********************************************
	Scalable Cannon Fire Effect
	(Rather conventional "tank barrel" look)
	
	Author: Steeveeo
***********************************************/

function EFFECT:Init( data )

	self.Pos = data:GetOrigin()
	self.Normal = data:GetNormal()
	self.Scale = data:GetScale() or 1
	self.Emitter = ParticleEmitter(self.Pos)
	
	//Exhaust
	local dir = self.Normal:Angle():Right()
	for i=1, 5 do
		local velocity = (dir + (VectorRand() * 0.25)) * (math.Rand(50, 500) * self.Scale)
		
		//Right
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos + ((self.Normal * -5) * self.Scale))
		p:SetDieTime(math.Clamp(math.Rand(1, 3) * self.Scale, 0.5, 8))
		p:SetVelocity(velocity)
		p:SetAirResistance(150)
		p:SetStartAlpha(128)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(15, 25) * self.Scale)
		p:SetEndSize(math.random(25, 50) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.1, 0.1))
		local grey = math.random(16,128)
		p:SetColor(grey, grey, grey)
		
		//Left
		p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos + ((self.Normal * -5) * self.Scale))
		p:SetDieTime(math.Clamp(math.Rand(1, 3) * self.Scale, 0.5, 8))
		p:SetVelocity(velocity * -1)
		p:SetAirResistance(150)
		p:SetStartAlpha(128)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(15, 25) * self.Scale)
		p:SetEndSize(math.random(25, 50) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.1, 0.1))
		local grey = math.random(16,128)
		p:SetColor(grey, grey, grey)
	end
	
	//Smoke Plume
	for i=1, 15 do
		local velocity = (self.Normal + (VectorRand() * 0.15)):GetNormalized() * (math.Rand(0, 2500) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(1, 3) * self.Scale, 0.5, 8))
		p:SetVelocity(velocity)
		p:SetAirResistance(300)
		p:SetStartAlpha(128)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(15, 35) * self.Scale)
		p:SetEndSize(math.random(75, 150) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-0.25, 0.25))
		local grey = math.random(64,196)
		p:SetColor(grey, grey, grey)
	end
	
	//Fire
	for i=1, 10 do
		local velocity = (self.Normal + (VectorRand() * 0.15)):GetNormalized() * (math.Rand(0, 2500) * self.Scale)
		
		local p = self.Emitter:Add("effects/shipsplosion/fire_001", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(0.1, 0.3) * self.Scale, 0.1, 3))
		p:SetVelocity(velocity)
		p:SetGravity(self.Normal * 300 * self.Scale)
		p:SetAirResistance(300)
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(25, 50) * self.Scale)
		p:SetEndSize(math.random(75, 150) * self.Scale)
		p:SetRoll(math.random(0, 360))
		p:SetRollDelta(math.Rand(-1, 1))
		p:SetColor(255, 255, 200)
	end
	
	//Flare
	for i=1, 2 do
		local p = self.Emitter:Add("sprites/light_ignorez", self.Pos)
		p:SetDieTime(math.Clamp(math.Rand(0.1, 0.15) * self.Scale, 0.1, 0.25))
		p:SetStartAlpha(255)
		p:SetEndAlpha(0)
		p:SetStartSize(350 * self.Scale)
		p:SetEndSize(350 * self.Scale)
		p:SetColor(255, 255, 200)
	end
	
	self.Emitter:Finish()
end

function EFFECT:Think( )
  	return false
end

function EFFECT:Render( )
	return false
end
