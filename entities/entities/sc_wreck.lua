AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_anim"
ENT.PrintName	= "Ship Wreck"
ENT.Author	= "Lt.Brandon + Steeveeo"

ENT.Spawnable	= false
ENT.AdminSpawnable = false

function ENT:SetupDataTables()
    SC.NWAccessors.CreateNWAccessor(self, "LifeRemaining", "number", 1200)
end

if CLIENT then
    function ENT:Initialize()
	    self.Emitter = nil
	    self.EmitterPos = Vector()
	    self.NextSmokeEmit = CurTime()
	    self.NextSparkEmit = CurTime() + math.Rand(3, 10)
    end

	function ENT:OnRemove()
		if self.Emitter ~= nil and self.Emitter ~= NULL and type(self.Emitter) == "CLuaEmitter" then
            self.Emitter:Finish()
            self.Emitter = nil
        end
	end

    function ENT:Draw()
	    self:DrawModel()

		--Set Effect Scale
		if self.Scale == nil then
			self.Scale = (self:OBBMaxs() - self:OBBMins()):Length() / 500
		end

		--Init Particles
		if (self.Emitter == nil or self.Emitter == NULL or type(self.Emitter) ~= "CLuaEmitter") and self:GetLifeRemaining() >= 300 then
			self.EmitterPos = self:NearestPoint(self:LocalToWorld(VectorRand() * 1500))
			self.Emitter = ParticleEmitter(self.EmitterPos)
		end

		--Check for end of particles
		if (self.Emitter ~= nil and self.Emitter ~= NULL and type(self.Emitter) == "CLuaEmitter") and self:GetLifeRemaining() < 300 then
			self.Emitter:Finish()
			self.Emitter = nil
		end

		--Smoke
		if CurTime() > self.NextSmokeEmit and self:GetLifeRemaining() >= 300 then
			local pos = Vector(math.Rand(self:OBBMins().x, self:OBBMaxs().x), math.Rand(self:OBBMins().y, self:OBBMaxs().y), math.Rand(self:OBBMins().z, self:OBBMaxs().z))
			local p = self.Emitter:Add("effects/shipsplosion/smoke_00" .. math.random(1,4), self:LocalToWorld(pos))
			p:SetDieTime(math.Rand(20, 50))
			p:SetVelocity(VectorRand() * math.Rand(0, 5))
			p:SetStartAlpha(16)
			p:SetEndAlpha(0)
			p:SetStartSize(math.random(0, 2) * self.Scale)
			p:SetEndSize(math.random(350, 700) * self.Scale)
			p:SetRoll(math.Rand(0, 360))

			local grey = math.random(16, 96)
			p:SetColor(grey, grey, grey)

			self.NextSmokeEmit = CurTime() + 2
		end

		--Spark
		if CurTime() > self.NextSparkEmit and self:GetLifeRemaining() >= 900 then
			local pos = Vector(math.Rand(self:OBBMins().x, self:OBBMaxs().x), math.Rand(self:OBBMins().y, self:OBBMaxs().y), math.Rand(self:OBBMins().z, self:OBBMaxs().z))
			timer.Create("sc_wreck_sparks" .. self:EntIndex(), 0.05, math.random(1, 10), function()
				if not IsValid(self) then return end

				local p = self.Emitter:Add("sprites/light_ignorez", self:LocalToWorld(pos))
				p:SetDieTime(math.Rand(0.15, 0.2))
				p:SetStartAlpha(0)
				p:SetEndAlpha(255)
				p:SetStartSize(math.random(1, 5) * self.Scale)
				p:SetEndSize(math.random(5, 50) * self.Scale)
				p:SetRoll(math.Rand(0, 360))
				p:SetColor(220, 220, 255)
			end)

			self.NextSparkEmit = CurTime() + math.Rand(3, 15)
		end
    end
elseif SERVER then
    function ENT:Initialize()
	    self:PhysicsInit( SOLID_VPHYSICS )
	    self:SetMoveType( MOVETYPE_VPHYSICS )
	    self:SetSolid( SOLID_VPHYSICS )
	    self:SetUseType( SIMPLE_USE )

		--Kill self after a bit
	    timer.Create("sc_wreck" .. self:EntIndex(), 10, 0, function()
			self:SetLifeRemaining(self:GetLifeRemaining() - 20)

			if self:GetLifeRemaining() <= 0 then
				SC.KillEnt(self)
			end
		end)

	    self.SC_Immune = true --make weapons ignore me
	    self.SB_Ignore = true --make spacebuild ignore me
	    self.Untouchable = true
	    self.Unconstrainable = true
	    self.PhysgunDisabled = true
	    self.CanTool = function()
		    return false
	    end

	    --Ownership crap
	    if(NADMOD) then --If we're using NADMOD PP, then use its function
		    NADMOD.SetOwnerWorld(self)
	    elseif(CPPI) then --If we're using SPP, then use its function
		    self:SetNWString("Owner", "World")
	    else --Well fuck it, lets just use our own!
		    self.Owner = game.GetWorld()
	    end

	    local phys = self:GetPhysicsObject()
	    if (phys:IsValid()) then
		    phys:Wake()
		    phys:EnableGravity(false)
		    phys:EnableDrag(false)
		    phys:EnableCollisions(true)
		    phys:EnableMotion(false)
	    end
    end

	function ENT:OnRemove()
		timer.Destroy("sc_wreck" .. self:EntIndex())
	end
end