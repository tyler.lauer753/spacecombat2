AddCSLuaFile()

include("includes/turretdefinitions.lua")

SC = SC or {}
SC.Turrets = SC.Turrets or {}

ENT.Type				= "anim"
ENT.Base 				= "base_moduleentity"
ENT.PrintName			= "SC2 Turret"
ENT.Author				= "Lt.Brandon"
ENT.Category			= "Space Combat 2"
ENT.Contact				= ""
ENT.Purpose				= ""

ENT.Spawnable			= false
ENT.AdminSpawnable		= false
ENT.IsSCTurret          = true
ENT.OffsetAng           = Angle(0, 0, 0)

SC.Turrets.TurretTypes = SC.Turrets.TurretTypes or {}
SC.Turrets.TurretWeaponCount = SC.Turrets.TurretWeaponCount or {}
SC.Turrets.TurretList = SC.Turrets.TurretList or {}

function SC.AddTurretType(Path, WeaponCount, CreationFunction)
    if Path == nil or not WeaponCount or not CreationFunction then
        SC.Error("Tried to add invalid turret type!\n"..debug.traceback(), 5)
        return
    end

    SC.Turrets.TurretTypes[Path] = CreationFunction
    SC.Turrets.TurretWeaponCount[Path] = WeaponCount

    if not table.HasValue(SC.Turrets.TurretList, Path) then
        table.insert(SC.Turrets.TurretList, Path)
    end
end

function SC.GetTurretWeaponCount(Path)
    return SC.Turrets.TurretWeaponCount[Path] or 0
end

function SC.GetTurretList()
    return SC.Turrets.TurretList
end

local base = scripted_ents.Get("base_moduleentity")
hook.Remove("InitPostEntity", "sc_turret_post_entity_init")
hook.Add("InitPostEntity", "sc_turret_post_entity_init", function()
    base = scripted_ents.Get("base_moduleentity")

    hook.Run("SC.RegisterTurrets")
    hook.Run("SC.PostRegisterTurrets")
end)

hook.Remove("OnReloaded", "sc_turret_post_reload")
hook.Add("OnReloaded", "sc_turret_post_reload", function()
    base = scripted_ents.Get("base_moduleentity")

    hook.Run("SC.RegisterTurrets")
    hook.Run("SC.PostRegisterTurrets")
end)

function ENT:SharedInit()
    base.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "Elevation", "number", 0, 1, function(self, Name, Value)
        if not CLIENT then return end
        if not self:IsDormant() and self:GetTurretType() ~= "not-set" and IsValid(self.PivotHolo) then
			self.PivotHolo:SetAngles(self.ElevationHolo:LocalToWorldAngles(Angle(Value, 0, 0)))
        end
    end)

    SC.NWAccessors.CreateNWAccessor(self, "Bearing", "number", 0, 1, function(self, Name, Value)
        if not CLIENT then return end
        if not self:IsDormant() and self:GetTurretType() ~= "not-set" and IsValid(self.BearingHolo) then
            self.BearingHolo:SetAngles(self:LocalToWorldAngles(Angle(0, Value, 0)))
        end
    end)

    SC.NWAccessors.CreateNWAccessor(self, "TurretType", "cachedstring", "not-set", 1, function(self, Name, Value)
        if not CLIENT then return end
        if IsValid(self) and SC.Turrets.TurretTypes[Value] then
            self:CleanupHolograms()
            SC.Turrets.TurretTypes[Value](self)
            self:SetModuleName(Value)
        end
    end)

    SC.NWAccessors.CreateNWAccessor(self, "ProjectileRecipe", "cachedstring", "FAIL")
    SC.NWAccessors.CreateNWAccessor(self, "ProjectileRecipeOwner", "cachedstring", "NULL")
    SC.NWAccessors.CreateNWAccessor(self, "LauncherType", "cachedstring", "FAIL")

    self:SetModuleName("Turret")

    self.Holograms = {}

    self:CallOnRemove("Cleanup Holograms", function()
        self:CleanupHolograms()
    end)
end

if CLIENT then
    function ENT:CheckMaterials()
        if (not self.NextMatCheck) or self.NextMatCheck < CurTime() then
            if self:GetMaterial() ~= self.HoloMaterial then
                self.HoloMaterial = self:GetMaterial()
                for _,v in pairs(self.Holograms) do
                    local al = v:GetColor().a
                    if al == 255 then v:SetMaterial(self.HoloMaterial) end
                end
            end
            local CC, CH = self:GetColor(), self.HoloColor
            if CH and (CC.r ~= CH.r or CC.g ~= CH.g or CC.b ~= CH.b) then
                self.HoloColor = Color(CC.r,CC.g,CC.b)
                local AColor = Color(CC.r,CC.g,CC.b,255)
                for _,v in pairs(self.Holograms) do
                    local al = v:GetColor().a
                    if al == 255 then v:SetColor(AColor) end
                end
            end
            self.NextMatCheck = CurTime() + 1
        end
    end

    function ENT:Think()
        base.Think(self)

        self:CheckMaterials()
    end

    hook.Add("NotifyShouldTransmit", "sc_turret_notifyshouldtransmit", function(Ent, ShouldTransmit)
        if not IsValid(Ent) or not Ent.IsSCTurret then return end
        if ShouldTransmit and not IsValid(Ent.Holograms[1]) and SC.Turrets.TurretTypes[Ent:GetTurretType()] then
            SC.Turrets.TurretTypes[Ent:GetTurretType()](Ent)
			Ent.PivotHolo:SetAngles(self.ElevationHolo:LocalToWorldAngles(Angle(Ent:GetElevation(), 0, 0)))
            Ent.BearingHolo:SetAngles(self:LocalToWorldAngles(Angle(0, Ent:GetBearing(), 0)))
        else
            Ent:CleanupHolograms()
        end
    end)
end

function ENT:CreateHologram(model, pos, ang, col, par, scale, relativetoself)
    if CLIENT then
        local h = ClientsideModel(model, (col and col.a or 255) == 255 and RENDERMODE_NORMAL or RENDERMODE_TRANSALPHA)
        h:SetPos((relativetoself and self or par):LocalToWorld(pos))
        h:SetAngles((relativetoself and self or par):LocalToWorldAngles(ang))
        h:SetModelScale(scale or Vector(1, 1, 1))
        h:SetColor(col or Color(255, 255, 255, 255))
        h:SetParent(par)
        local Min, Max = h:GetModelRenderBounds()
        h:SetRenderBounds(Min, Max, Vector(100, 100, 100))

        table.insert(self.Holograms, h)

        return h
    else
        local h = ents.Create("info_target")
        h:SetPos((relativetoself and self or par):LocalToWorld(pos))
        h:SetAngles((relativetoself and self or par):LocalToWorldAngles(ang))
        h:SetParent(par)

        function h:UpdateTransmitState()
	        return TRANSMIT_NEVER
        end

        h:Spawn()

        table.insert(self.Holograms, h)

        return h
    end
end

function ENT:CleanupHolograms()
    for i,k in pairs(self.Holograms) do
        if IsValid(k) then
            k:Remove()
        end
    end
end

if CLIENT then return end

function ENT:CreateLauncher(AttachEntity, AttachPos, Upgrades)
    local LauncherData = GAMEMODE.Launchers.Types.LoadedTypes[self:GetLauncherType()]
    if not LauncherData then
        SC.Error("Tried to create turret launcher with invalid launcher data", 5)
        return
    end

    local NewLauncher = LauncherData:CreateLauncher(self.Owner, AttachEntity, nil, Upgrades)

    if not NewLauncher then
        SC.Error("Failed to create launcher for turret", 5)
        return
    end

    NewLauncher:SetRecipeByName(self:GetProjectileRecipe(), self:GetProjectileRecipeOwner())
    NewLauncher:SetWorldPosition(AttachPos)
    NewLauncher:SetCore(self:GetProtector())

    NewLauncher.OnSpawnedCallback = function(Launcher)
        if not IsValid(self) then return end
        self:LauncherOnSpawned(Launcher)
    end

    NewLauncher.OnRemovedCallback = function(Launcher)
        if not IsValid(self) then return end
        self:LauncherOnRemoved(Launcher)
    end

    NewLauncher.OnStartedFiringCallback = function(Launcher)
        if not IsValid(self) then return end
        self:LauncherOnStartedFiring(Launcher)
    end

    NewLauncher.OnStoppedFiringCallback = function(Launcher)
        if not IsValid(self) then return end
        self:LauncherOnStoppedFiring(Launcher)
    end

    NewLauncher.OnReloadingStartedCallback = function(Launcher)
        if not IsValid(self) then return end
        self:LauncherOnReloadingStarted(Launcher)
    end

    NewLauncher.OnReloadingFinishedCallback = function(Launcher)
        if not IsValid(self) then return end
        self:LauncherOnReloadingFinished(Launcher)
    end

    NewLauncher.PostProjectileFiredCallback = function(Launcher, Projectile)
        if not IsValid(self) then return end
        self:LauncherPostProjectileFired(Launcher, Projectile)
    end

    NewLauncher.PreProjectileFiredCallback = function(Launcher, Projectile)
        if not IsValid(self) then return end
        self:LauncherPreProjectileFired(Launcher, Projectile)
    end

    NewLauncher.OnOverheatedCallback = function(Launcher)
        if not IsValid(self) then return end
        self:LauncherOnOverheated(Launcher)
    end

    NewLauncher.OnCooledDownCallback = function(Launcher)
        if not IsValid(self) then return end
        self:LauncherOnCooledDown(Launcher)
    end

    return NewLauncher
end

function ENT:SetupWeapons()
	self.WeaponCount = math.floor(self.WeaponCount)

	-- TODO: Make sure weapon is valid before setup

    local NumAttachmentPositions = table.Count(self.AttachmentPositions)
	if self.WeaponCount == NumAttachmentPositions then
		for i,k in pairs(self.AttachmentPositions) do
            self.Weapons[i] = self:CreateLauncher(self.PivotHolo, self:LocalToWorld(k))
		end
	elseif self.WeaponCount < NumAttachmentPositions then
		local AttachmentPointsPerWep = math.floor(NumAttachmentPositions / self.WeaponCount)
		for i = 1, self.WeaponCount do
			local AttachmentPos = self.AttachmentPositions[i * AttachmentPointsPerWep]
            self.Weapons[i] = self:CreateLauncher(self.PivotHolo, self:LocalToWorld(AttachmentPos))

            -- TODO: Set the launcher to fire from the remaining positions
		end
	else
		local WepsPerAttachmentPoint = NumAttachmentPositions / self.WeaponCount
		for i = 1, self.WeaponCount do
			local AttachmentPos = self.AttachmentPositions[math.Clamp(math.Round(WepsPerAttachmentPoint*i),1,NumAttachmentPositions)]
			self.Weapons[i] = self:CreateLauncher(self.PivotHolo, self:LocalToWorld(AttachmentPos))
		end
	end
end

function ENT:SetupPhysics()
	local phys = self:GetPhysicsObject()
	if IsValid(phys) then -- Check if the physics object has already been created to avoid having to delete and re-spawn it.
		phys:Wake()
		phys:EnableGravity(true)
		phys:EnableDrag(true)
		phys:EnableCollisions(true)
		phys:EnableMotion(false)
	else -- Recreate physics object.
		self:PhysicsInit(SOLID_VPHYSICS)
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
        self:SetUseType(SIMPLE_USE)
		local phys = self:GetPhysicsObject() -- Complete Initialisation of the physics object
        if IsValid(phys) then -- This block could be replaced with another call to SetupPhysics, but if this check right here is needed, there's a chance it'll stack overflow
            phys:Wake()
            phys:EnableGravity(true)
            phys:EnableDrag(true)
            phys:EnableCollisions(true)
            phys:EnableMotion(false)
        end
    end
end

function ENT:Initialize()
	if CLIENT then return end
    self.BaseClass.Initialize(self)

	self.AttachmentPositions = {}
	self.Holograms = {}
	self.Weapons = {}
	self.WeaponCount = 0

	self.Firing = false
	self.Active = false
	self.Vec = Vector(0,0,0)
	self.Target = NULL

	self.AimSpeed = 10
	self.MaxElevation = 75
	self.MinElevation = -10
	self.Elevation = 0
	self.ElevationDelta = 0
	self.Bearing = 0
    self.HoloColor = Color(255, 255, 255, 255)

	self.Muted = false
	self.SoundPlaying = false

	-- Create every possible input and output, as unused ones will be removed. Creating them on Initialize ensures the duplicator can link them up if it needs to
	self.Inputs = WireLib.CreateSpecialInputs( self,
		{"Active", "Fire", "Reload", "Target"},
		{"NORMAL", "NORMAL", "NORMAL", "ENTITY"}
	)
	self.Outputs = WireLib.CreateSpecialOutputs( self,
		{ "Can Fire", "Reloading", "Ammo", "Max Ammo"},
		{ "NORMAL", "NORMAL", "NORMAL", "NORMAL"}
	)

	--Cleanup
	self:CallOnRemove("Turret Removed", function()
		if self.SoundObjLoop and self.SoundObjLoop:IsPlaying() then self.SoundObjLoop:Stop() end
		if self.SoundObjStop and self.SoundObjStop:IsPlaying() then self.SoundObjStop:Stop() end
	end)
end

function ENT:GetWirePorts()
    return {}, {}
end

function ENT:CanCycle()
    return IsValid(self:GetProtector()) and self:GetFitting().CanRun
end

function ENT:OnCycleStarted()
end

function ENT:OnCycleFinished()
end

function ENT:LauncherOnSpawned(Launcher)
    if Launcher == self.Weapons[1] then
        self:SetHasCycle(true)
    end
end

function ENT:LauncherOnRemoved(Launcher)
    for i,k in pairs(self.Weapons) do
        if k == Launcher then
            self.Weapons[i] = nil
            break
        end
    end
end

function ENT:LauncherOnStartedFiring(Launcher)
    if Launcher == self.Weapons[1] then
        self:SetCycling(true)
    end
end

function ENT:LauncherOnStoppedFiring(Launcher)
    if Launcher == self.Weapons[1] then
        self:SetCycling(false)
    end
end

function ENT:LauncherOnReloadingStarted(Launcher)
    --self:SetReloading(true)
end

function ENT:LauncherOnReloadingFinished(Launcher)
    --self:SetReloading(false)
end

function ENT:LauncherPostProjectileFired(Launcher, Projectile)
    if self:GetCycling() and Launcher == self.Weapons[1] then
        self:OnCycleStarted()
    end
end

function ENT:LauncherPreProjectileFired(Launcher, Projectile)
    if Launcher == self.Weapons[1] then
        self:OnCycleFinished()
    end
end

function ENT:LauncherOnOverheated(Launcher)
    --self:SetOverheated(true)
end

function ENT:LauncherOnCooledDown(Launcher)
    --self:SetOverheated(false)
end

function ENT:OnModuleEnabled()
    for i, Launcher in pairs(self.Weapons) do
        if IsValid(Launcher) then
            Launcher:SetCore(self:GetProtector())
        end
    end
end

function ENT:OnModuleDisabled()
    for i, Launcher in pairs(self.Weapons) do
        if IsValid(Launcher) then
            Launcher:SetCore(nil)
        end
    end
end

function ENT:OnRemove()
    base.OnRemove(self)
    self:CleanupTurret()
end

function ENT:CleanupTurret()
    for i, Launcher in pairs(self.Weapons) do
        if IsValid(Launcher) then
            Launcher:SetCore(nil)
            Launcher:StopFiring()
            Launcher:Remove()
        end

        self.Weapons[i] = nil
    end

	self:CleanupHolograms()
end

function ENT:ApplySettings(data)
    if not data or not data.turret or not data.turret.type or not data.launcher or not data.recipe then
        self.Owner:ChatPrint("[Space Combat 2] - Failed to update turret, invalid data was sent!")
        return false
    end

    if self:GetTurretType() ~= "not-set" and data.turret.type ~= self:GetTurretType() then
        self.Owner:ChatPrint("[Space Combat 2] - Failed to update turret, you need to use the same turret type you selected when spawning the turret!")
        return false
    end

    self:CleanupTurret()

    -- TODO: Validate the data

    self.WeaponCount = math.Clamp(data.turret.count,1,20)
    self:SetTurretType(data.turret.type)
    self:SetLauncherType(data.launcher)
    self:SetProjectileRecipe(data.recipe)
    self:SetProjectileRecipeOwner(data.owner or "NULL")

	if SC.Turrets.TurretTypes[self:GetTurretType()] then
		SC.Turrets.TurretTypes[self:GetTurretType()](self)
	else
		self.Owner:ChatPrint("[Space Combat 2] - INVALID TURRET CLASS DETECTED. Removing Entity "..tostring(self).." CLASS: "..data.turret.type)
		self:Remove()
		return true
	end

    self:SetModel(self.BaseModel)
    self:SetupPhysics()
    self:SetupWeapons()

    self.Invalid = false
    return true
end

function ENT:SaveSCInfo()
    -- Do not change any of these variable names, it breaks duplications. Adding new variables is fine, though. - Lt.Brandon
    return {LauncherType=self:GetLauncherType(), ProjectileRecipeOwner=self:GetProjectileRecipeOwner(), ProjectileType=self:GetProjectileRecipe(), TurretType=self:GetTurretType(), WeaponCount=self.WeaponCount}
end

local OldTurretSizesToNew = {
    Fighter = "Tiny",
    Frigate = "Small",
    Cruiser = "Medium",
    Battleship = "Large",
    Capital = "X-Large"
}

function ENT:LoadSCInfo(Info)
    local function Invalidate(msg)
        self.Owner:ChatPrint("[Space Combat 2] - "..msg..", PLEASE UPDATE "..tostring(self))
        self.Invalid = true
        self:SetupPhysics()
    end

    -- Convert old launcher names
    if Info and not Info.TurretType then
        if Info.adsf and Info.TurretSubType then
            local ConvertedSize = OldTurretSizesToNew[Info.adsf]
            Info.TurretType = ConvertedSize.."/"..Info.TurretSubType

            if not SC.Turrets.TurretTypes[Info.TurretType] then
                Info.TurretType = nil
            end
        end
    end

    -- Fail if our data isn't good
    if not Info or not Info.LauncherType or not Info.ProjectileRecipeOwner or not Info.ProjectileType or not Info.TurretType then
        Invalidate("INVALID DUPLICATION DATA DETECTED")
        return
    end

    local Data = {
        turret = {
            type = Info.TurretType or SC.Turrets.TurretTypes[self:GetTurretType()],
            count = math.Clamp(Info.WeaponCount or SC.Turrets.TurretWeaponCount[self:GetTurretType()],1,20)
        },
        launcher=Info.LauncherType,
        owner=Info.ProjectileRecipeOwner,
        recipe=Info.ProjectileType
    }

    self:ApplySettings(Data)
end

function ENT:PlayRotationSounds()
	if not self.SoundPlaying then
		if self.SoundObjStop and self.SoundObjStop:IsPlaying() then
			self.SoundObjStop:Stop()
		end

		if not self.Muted then
			self.SoundObjLoop = CreateSound(self, self.SoundStartTurn)
			self.SoundObjLoop:PlayEx(self.SoundVolume, self.SoundPitch)
		end

		self.SoundPlaying = true
	end
end

function ENT:StopRotationSounds()
	if self.SoundPlaying then
		if self.SoundObjLoop and self.SoundObjLoop:IsPlaying() then
			self.SoundObjLoop:Stop()
		end

		if not self.Muted then
			self.SoundObjStop = CreateSound(self, self.SoundStopTurn)
			self.SoundObjStop:PlayEx(self.SoundVolume, self.SoundPitch)
		end

		self.SoundPlaying = false
	end
end

local rad2deg = 180 / math.pi
local function Elevation(ent, vec)
	if not IsValid(ent) then return 0 end

	local pos = ent:WorldToLocal(vec)
	local len = pos:Length()
	return rad2deg*math.asin(pos.z / len)
end

local function Bearing(ent, vec)
	if not IsValid(ent) then return 0 end

	local pos = ent:WorldToLocal(vec)
	return rad2deg*-math.atan2(pos.y, pos.x)
end

function ENT:Think()
	if CLIENT then return end
    if self.Invalid then return end
    if IsValid(self.Target) then self.Vec = self.Target:LocalToWorld(self.Target:OBBCenter()) end
    if not self:GetProtector() then self:GetFitting().Status = "Offline" end
    if not IsValid(self.ElevationHolo) or not IsValid(self.BearingHolo) then return end

    for i,k in ipairs(self.Weapons) do
        k.Fitting = self.SC_Fitting
    end

    if IsValid(self.Weapons[1]) then
        local MainLauncher = self.Weapons[1]

        if self:GetCycling() and not MainLauncher:IsReloading() and not MainLauncher:IsOverheated() and MainLauncher:HasResources() then
            local FireDelay = 60 / (MainLauncher:IsThrottled() and MainLauncher.ThrottledShotsPerMinute or MainLauncher.ShotsPerMinute)

            if MainLauncher.HasBurst then
                FireDelay = FireDelay + MainLauncher.ShotsPerBurst * MainLauncher.TimeBetweenBurstShots
            end

            self:SetCycleDuration(FireDelay)
            self:SetCyclePercent(math.Clamp(1 - (MainLauncher.NextFire - CurTime()) / FireDelay, 0, 1))
        else
            self:SetCyclePercent(0)
        end

        if WireLib ~= nil then
            WireLib.TriggerOutput(self, "Can Fire", MainLauncher:CanFire() and 1 or 0)
        end

        if MainLauncher.HasMagazine then
            if WireLib ~= nil then
                WireLib.TriggerOutput(self, "Reloading", MainLauncher:IsReloading() and 1 or 0)
                WireLib.TriggerOutput(self, "Ammo", MainLauncher.Ammo)
                WireLib.TriggerOutput(self, "Max Ammo", MainLauncher.MaxAmmo)
            end
        end
    end

	if IsValid(self:GetNode()) then
		self.ElevationDelta = self.Elevation

        -- Calculate Bearing and Elevation to target
        if self.Active and not (self.Vec == vector_origin) then
            self.Bearing = -Bearing(self.BearingHolo, self.Vec)
            self.Elevation = Elevation(self.ElevationHolo, self.Vec)
        else
            local angles = self:WorldToLocalAngles(self.ElevationHolo:GetAngles()) - self.OffsetAng
            angles:Normalize()
            self.Bearing = -angles.yaw
            self.Elevation = -angles.pitch
        end

        --Are we rotating?
        if math.abs(self.Bearing) > 0.1 or math.abs(self.ElevationDelta) > 0.1 then
            self:PlayRotationSounds()

            if self.Elevation < self.MinElevation then self.Elevation = self.MinElevation elseif self.Elevation > self.MaxElevation then self.Elevation = self.MaxElevation end

            self.ElevationDelta = self.ElevationDelta - self.Elevation

            if self.ElevationDelta < -self.AimSpeed then
                self.Elevation = self.Elevation + self.ElevationDelta + self.AimSpeed
            elseif self.ElevationDelta > self.AimSpeed then
                self.Elevation = self.Elevation + self.ElevationDelta - self.AimSpeed
            end

            if self.Bearing < -self.AimSpeed then self.Bearing = -self.AimSpeed elseif self.Bearing > self.AimSpeed then self.Bearing = self.AimSpeed end

            self.BearingHolo:SetAngles(self.BearingHolo:LocalToWorldAngles(Angle(0, self.Bearing, 0)))
            self.PivotHolo:SetAngles(self.ElevationHolo:LocalToWorldAngles(Angle(-self.Elevation, 0, 0)))

            self:SetBearing(self:WorldToLocalAngles(self.BearingHolo:GetAngles()).yaw)
            self:SetElevation(-self.Elevation)
        else
            self:StopRotationSounds()
        end

        if math.abs(self.Bearing) < self.AimSpeed and math.abs(self.ElevationDelta) < self.AimSpeed then
            for i,k in ipairs(self.Weapons) do
                if self.Firing and self.Active then
                    k:StartFiring()
                else
                    k:StopFiring()
                end
            end
        end
	else
		self:StopRotationSounds()
	end

	self:DoOutputs()
	self:NextThink(CurTime() + 0.1)
	return true
end

function ENT:TriggerInput(iname, value)
	if iname == "Fire" then
		self.Firing = (value ~= 0)

		if not self.Firing then
			for i,k in ipairs(self.Weapons) do
				k:StopFiring()
			end
		end
	elseif iname == "Active" then
		self.Active = (value ~= 0)

		if not self.Active then
			for i,k in ipairs(self.Weapons) do
				k:StopFiring()
			end
		end
	elseif iname == "Reload" and value ~= 0 then
		for i,k in ipairs(self.Weapons) do
			k:StartReloading()
		end
	elseif iname == "Target" then
		self.Target = value
		for i,k in ipairs(self.Weapons) do
			k:SetTarget(Value)
        end
    end
end

function ENT:DoOutputs()

end

if SERVER then
	duplicator.RegisterEntityClass("sc_turret", SC.MakeEnt, "Data")
end