--[[
	Minable Mineral Deposit Child Ent for SC2 Mining

	Author: Steeveeo + Fuhrball
]]--
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_gmodentity"

ENT.PrintName = "Mineral Deposit Child"
ENT.Author = "Steeveeo + Fuhrball"
ENT.Contact = ""
ENT.Purpose = "I am a mineral deposit child, and will devour your soul!"
ENT.Instructions = ""
ENT.RenderGroup = RENDERGROUP_BOTH

ENT.Spawnable = false
ENT.AdminSpawnable = false

local breachParticleSizeDivisor = 50
local quakeParticleSizeDivisor = 75

if CLIENT then
    -- This is kind of hacky and only works with one model right now, this should get moved into a config file for each crystal model!
    -- The Parent crystal uses a version of this that works on anything, but it looks bad on the normal crystals.
    ENT.MaterialFormat = [[
VertexLitGeneric
{
	$model 					"1"
	$basetexture		 	"spacecombat2\ltbrandon\crystals\crystal_01_defuse"
	$bumpmap				"spacecombat2\ltbrandon\crystals\crystal_01_normal"
	$envmap 				env_cubemap
	$envmaptint				"[%.2f %.2f %.2f]"
	$color2					"[%.2f %.2f %.2f]"
	$surfaceprop			"metal"
	$phong 1
	$halflambert 1
}
    ]]

    ENT.CrystalMatNamePrefix = "crystalmat_"

    -- These are vectors instead of colors because they are float-based and not 0 - 255
    ENT.EnvColor = Vector(0.5, 0.12, 0.12)
    ENT.Color = Vector(4, 0.3, 0)
    ENT.ParticleColor = Color(255, 0, 0, 64)

    -- If this bool is set to true then the material will be updated
    ENT.MaterialNeedsUpdate = false

    -- Create a default material to use until we get updated by the parent
    ENT.CrystalMaterial = GAMEMODE.MaterialFromVMT("crystalmat", string.format(ENT.MaterialFormat, ENT.EnvColor.x, ENT.EnvColor.y, ENT.EnvColor.z, ENT.Color.x, ENT.Color.y, ENT.Color.z))

	local Emitter = nil
	local LastEmit = 0
	ENT.NextEmit = CurTime()

	--Gets the global Emitter for all crystals, or creates one if not set
	function ENT:GetEmitter()
		if Emitter == nil then
			Emitter = ParticleEmitter(Vector(0, 0, 0))
		end

		return Emitter
	end

	function ENT:CanEmit()
		local globalDelay = GetConVarNumber("mining_mineralParticleDelay")

		if CurTime() < LastEmit + globalDelay or CurTime() < self.NextEmit then
			return false
		end

		return true
	end

    function ENT:Initialize()
		self:DrawShadow(false)

		self.SpriteMat = Material("effects/shipsplosion/fire_001")
        self.SpriteMat:SetFloat("$overbrightfactor", 15.0)

        self:SetMaterial("!"..self.CrystalMaterial:GetName())

        SC.NWAccessors.CreateNWAccessor(self, "EnvColor", "vector", self.EnvColor, 1, function(self, Name, Value)
            self.MaterialNeedsUpdate = true
        end)

        SC.NWAccessors.CreateNWAccessor(self, "MatColor", "vector", self.Color, 1, function(self, Name, Value)
            self.MaterialNeedsUpdate = true
        end)

        SC.NWAccessors.CreateNWAccessor(self, "ParticleColor", "color", self.ParticleColor)
    end

    function ENT:Think()
		--Determine if we should draw this
		self:SetNoDraw(true)
		local viewEnt = LocalPlayer()
		if IsValid(viewEnt) then
			if IsValid(LocalPlayer():GetViewEntity()) then
				viewEnt = LocalPlayer():GetViewEntity()
			end
		end

		--Determine maximum view distance (recalc per frame to deal with updated settings, non-expensive)
		local maxDist = GetConVarNumber("mining_mineralChildViewDist")
		local fadeDist = GetConVarNumber("mining_mineralChildFadeDist")
		maxDist = maxDist * maxDist
		fadeDist = fadeDist * fadeDist

		local dist = viewEnt:GetPos():DistToSqr(self:GetPos())
		if dist < maxDist then
            self:SetNoDraw(false)

            -- Update material if we need to
            if self.MaterialNeedsUpdate and not self.UpdatingMaterial then
                timer.Simple(0.1, function()
                    self.EnvColor = self:GetEnvColor()
                    self.Color = self:GetMatColor()
                    self.CrystalMaterial = GAMEMODE.MaterialFromVMT(
                        string.sub(self.CrystalMatNamePrefix..string.sub(game.GetMap(), 1, 12)
                        ..string.Replace(util.TypeToString(self.Color.x)..util.TypeToString(self.Color.y)..util.TypeToString(self.Color.z), '.', '')
                        , 1, 36),
                        string.format(self.MaterialFormat, self.EnvColor.x, self.EnvColor.y, self.EnvColor.z, self.Color.x, self.Color.y, self.Color.z)
                    )

                    self:SetMaterial("!"..self.CrystalMaterial:GetName())
                    self.UpdatingMaterial = false
                end)
                self.MaterialNeedsUpdate = false
                self.UpdatingMaterial = true
            end

			--Determine Alpha
			local alpha = 255
			if dist > fadeDist then
				local fadeDiff = maxDist - fadeDist
				local fadeOffset = dist - fadeDist
				local fraction = fadeOffset / fadeDiff

				alpha = 255 - (255 * fraction)
			end

			self:SetColor(Color(255, 255, 255, alpha))
			self:SetRenderMode(RENDERMODE_TRANSALPHA)
		end

	    return true
    end

	function ENT:Draw()
		self:DrawModel()

		--Determine if we are in range to emit
		local viewEnt = LocalPlayer()
		if IsValid(viewEnt) then
			if IsValid(LocalPlayer():GetViewEntity()) then
				viewEnt = LocalPlayer():GetViewEntity()
			end
		end

		--Determine maximum view distance (recalc per frame to deal with updated settings, non-expensive)
		local emitDist = GetConVarNumber("mining_mineralParticleViewDist")
		local dist = viewEnt:GetPos():DistToSqr(self:GetPos())
		emitDist = emitDist * emitDist

        if dist < emitDist and self:CanEmit() then
            local ParticleColor = self:GetParticleColor()
			local pos = self:GetPos() + (self:GetUp() * math.Rand(0, self:OBBMaxs().z))

			local p = self:GetEmitter():Add(self.SpriteMat, pos)
			p:SetDieTime(math.Rand(10, 20))
			p:SetVelocity(VectorRand() * 5)
			p:SetStartAlpha(ParticleColor.a)
			p:SetEndAlpha(0)
			p:SetStartSize(0)
			p:SetEndSize(math.random(50, 250))
			p:SetColor(ParticleColor.r, ParticleColor.g, ParticleColor.b)
			p:SetRoll(math.Rand(0, 360))
			p:SetRollDelta(math.Rand(-0.25, 0.25))

			LastEmit = CurTime()
			self.NextEmit = CurTime() + math.Rand(1, 10)
		end
	end

elseif SERVER then
    ENT.MineralParent = nil
	ENT.CrystalType = 0

	ENT.SpawnParticle_Magnitude = 4
	ENT.SpawnQuake_Amp = 8
	ENT.SpawnQuake_Freq = 200
	ENT.SpawnQuake_Time = 6
	ENT.SpawnQuake_Dist = 1000

	ENT.IsDecaying = false
	ENT.DecaySpeed = 1
	ENT.DecayHeight = 0

	ENT.BreachQuake_Amp = 32
	ENT.BreachQuake_Freq = 200
	ENT.BreachQuake_Time = 1
	ENT.BreachQuake_Dist = 300
	ENT.BreachQuake_DamageRadius = 175
	ENT.BreachQuake_Damage = {
		KIN = 150000,
		EXP = 5000,
		THERM = 500,
		PIERCE = 15000
	}

    ENT.SpawnRumbles = {}
    ENT.SpawnRumbles[1] = "npc/antlion/rumble1.wav"
    ENT.SpawnRumbles[2] = "ambient/levels/caves/rumble1.wav"
    ENT.SpawnRumbles[3] = "ambient/levels/caves/rumble3.wav"

    ENT.BreachSounds = {}
    ENT.BreachSounds[1] = "physics/concrete/boulder_impact_hard1.wav"
    ENT.BreachSounds[2] = "physics/concrete/boulder_impact_hard2.wav"
    ENT.BreachSounds[3] = "physics/concrete/boulder_impact_hard3.wav"
    ENT.BreachSounds[4] = "physics/concrete/boulder_impact_hard4.wav"
    ENT.BreachSounds[5] = "physics/concrete/concrete_break2.wav"
    ENT.BreachSounds[6] = "physics/concrete/concrete_break3.wav"

    for k,v in pairs(ENT.SpawnRumbles) do
	    util.PrecacheSound(v)
    end

    for k,v in pairs(ENT.BreachSounds) do
	    util.PrecacheSound(v)
    end

    function ENT:Initialize()
	    self:SetName( "Mineral Deposit" )
	    self:PhysicsInit( SOLID_VPHYSICS )
	    self:SetMoveType( MOVETYPE_VPHYSICS )
	    self:SetSolid( SOLID_VPHYSICS )
        self:DrawShadow( false )

        SC.NWAccessors.CreateNWAccessor(self, "EnvColor", "vector", Vector(0.5, 0.12, 0.12))
        SC.NWAccessors.CreateNWAccessor(self, "MatColor", "vector", Vector(4, 0.3, 0))
        SC.NWAccessors.CreateNWAccessor(self, "ParticleColor", "color", Color(255, 0, 0, 64))

	    self.SC_Immune = true --make weapons ignore me
	    self.SB_Ignore = true --make spacebuild ignore me
	    self.Untouchable = true
	    self.Unconstrainable = true
	    self.PhysgunDisabled = true
	    self.CanTool = function()
		    return false
	    end

	    --Ownership crap
	    if(NADMOD) then --If we're using NADMOD PP, then use its function
		    NADMOD.SetOwnerWorld(self)
	    elseif(CPPI) then--If we're using SPP, then use its function
		    self:SetNWString("Owner", "World")
	    else --Well fuck it, lets just use our own!
		    self.Owner = game.GetWorld()
	    end

	    local phys = self:GetPhysicsObject()
	    if (phys:IsValid()) then
		    phys:Sleep()
		    phys:EnableGravity( false )
		    phys:EnableCollisions( true )
		    phys:EnableMotion( false )
		    phys:SetMass( 50000 )
	    end


	    --===================
	    --Spawn Sequence
	    --===================

	    --Rumble
	    self:DoQuake(self.SpawnQuake_Amp, self.SpawnQuake_Freq, self.SpawnQuake_Time, self.SpawnQuake_Dist, 28)
	    self:EmitSound(table.Random(self.SpawnRumbles), 75, 100)

		local boxSize = self:OBBMaxs() - self:OBBMins()
		local boxLength = boxSize:Length()

	    local effect = {
		    Scale = boxLength / quakeParticleSizeDivisor,
		    Magnitude = self.SpawnParticle_Magnitude,
		    Normal = self:GetUp(),
		    Origin = self:GetPos()
	    }
	    SC.CreateEffect("mining_earthquake", effect)

	    --Hide under surface
	    self.CurHeight = 0
        self.MaxHeight = boxSize.z
        local SpawnedPos = self:GetPos()

        self:SetPos(self:GetPos() - (self:GetUp() * self.MaxHeight))

        local MovementPerStep = -(self:GetPos() - SpawnedPos) / 20

	    timer.Simple(self.SpawnParticle_Magnitude, function()
			if not IsValid(self) then return end

		    --Breach Surface
		    self:DoQuake(self.BreachQuake_Amp, self.BreachQuake_Freq, self.BreachQuake_Time, self.BreachQuake_Dist, 28)
		    self:EmitSound(table.Random(self.BreachSounds), 75, 100)

		    local effect = {
			    Scale = boxLength / breachParticleSizeDivisor,
			    Normal = self:GetUp(),
			    Origin = self:GetPos() + (self:GetUp() * self.MaxHeight)
		    }
		    SC.CreateEffect("mining_mineralbreach", effect)

		    SC.Explode(self:GetPos() + (self:GetUp() * self.MaxHeight), self.BreachQuake_DamageRadius, self.BreachQuake_DamageRadius, 10000, self.BreachQuake_Damage, self, self)

		    timer.Create("BreachAnim" .. self:EntIndex(), 0.01, 20, function()
				if not IsValid(self) then return end

                self.CurHeight = self.MaxHeight
                self:SetPos(self:GetPos() + MovementPerStep)
		    end)
	    end)
    end

    function ENT:Think()
	    if not self.Resources then
			self:Remove()
			return
		end

		--Check if we're out of contents
	    self.Total = 0
	    for i,k in pairs(self.Resources) do
		    self.Total = self.Total + k
	    end

	    if self.Total <= 0 then
			--TODO: Death Effect
		    self:Remove()
	    end

		if self.IsDecaying then
			local decayAmount = self.DecaySpeed * 0.033
			self:SetPos(self:LocalToWorld(Vector(0, 0, -decayAmount)))
			self.DecayHeight = self.DecayHeight + decayAmount

			--Finish?
			if self.DecayHeight >= self:OBBMaxs().z then
				self:Remove()
				return false
			end

			self:NextThink(CurTime() + 0.033) --30 "FPS"
		else
			self:NextThink(CurTime() + 1)
		end
	    return true
    end

	function ENT:Decay(decayTime)
		if not self.IsDecaying then
			self.IsDecaying = true
			self.DecaySpeed = self:OBBMaxs().z / (decayTime or 5)
			self.DecayHeight = 0

			--Start Quake
			self:DoQuake(self.BreachQuake_Amp * 0.5, self.BreachQuake_Freq, decayTime or 5, self.BreachQuake_Dist, 28)
			self:EmitSound(table.Random(self.SpawnRumbles), 75, 100)

			local effect = {
				Scale = self.SpawnParticle_Scale,
				Magnitude = self.SpawnParticle_Magnitude,
				Normal = self:GetUp(),
				Origin = self:GetPos()
			}
			SC.CreateEffect("mining_earthquake", effect)
		end
	end

    function ENT:DoQuake(Amplitude, Frequency, Duration, Radius, Flags)
	    local QuakeEnt = ents.Create("env_shake")
	    QuakeEnt:SetKeyValue("amplitude", Amplitude)
	    QuakeEnt:SetKeyValue("frequency", Frequency)
	    QuakeEnt:SetKeyValue("duration", Duration)
	    QuakeEnt:SetKeyValue("radius", Radius)
	    QuakeEnt:SetKeyValue("spawnflags", Flags)

	    QuakeEnt:SetPos(self:GetPos() + (self:GetUp() * 15))
	    QuakeEnt:Spawn()
	    QuakeEnt:Fire("StartShake")

	    --Remove quake ent after completion
	    timer.Simple(Duration, function()
		    QuakeEnt:Remove()
	    end)
    end

	function ENT:MiningSequence(damage)
        local efficientDamage, inefficientDamage = 0, 0

        if type(damage) == "table" then
	        --Convert damage types into mining effectiveness
	        inefficientDamage = damage.RoidMiner
	        efficientDamage = damage.MineralMiner
        else
            efficientDamage = damage
        end

	    local Receive = {}
	    for i,k in pairs(self.Resources) do
		    math.randomseed(SysTime())

		    --Grab resources from crystal
		    local inefficient = math.Round(math.min(k, inefficientDamage * math.random(0.7, 1.2)))
		    local efficient = math.Round(math.min(k, efficientDamage * math.random(0.7, 1.2)))

		    Receive[i] = math.Clamp(inefficient + efficient, 0, self.Resources[i])

		    --Remove resource from crystal
		    k = k - efficient

		    --Asteroid Mining Lasers also work, but they destroy the crystal faster than they return resources
		    k = k - (inefficient * math.random(1, 10))

		    --Tick down internal resources
		    self.Resources[i] = k
		    if self.Resources[i] <= 0 then self.Resources[i] = nil end
	    end

	    return Receive
    end

    --Placeholder functions
    function ENT:OnRemove()
    end

    function ENT:Touch()
    end

    function ENT:StartTouch()
    end

    function ENT:EndTouch()
    end
end