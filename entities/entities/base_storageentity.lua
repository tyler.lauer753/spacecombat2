AddCSLuaFile()

DEFINE_BASECLASS("base_lsentity")

ENT.PrintName = "SB Infinity Storage"
ENT.Author = "Divran"
ENT.Contact = "arviddivran@gmail.com"
ENT.Purpose = "Base for storage entities"
ENT.Instructions = "Derive from this for anything with resource storage"

ENT.Spawnable = false
ENT.AdminOnly = false

local base = scripted_ents.Get("base_lsentity")
hook.Add( "InitPostEntity", "base_storage_post_entity_init", function()
	base = scripted_ents.Get("base_lsentity")
end)

--------------------------------------------------
-- Setup
--------------------------------------------------

function ENT:SharedInit()
	base.SharedInit(self)

    -- LSSU = Life Support Storage Update, this string can't be cached so it needs to be short to avoid too much overhead
    SC.NWAccessors.CreateNWAccessor(self, "Storage", "custom", {}, 1, nil, "LSSU")
end

if CLIENT then return end

function ENT:Setup()
	self:SetBaseMultiplier(self:GetPhysicsObject():GetVolume() * 10)

	self.temp_storage = {}
	self.Consumption = {}
    self.HideOverlay = false

	self:SetupStorage()
    self:SetupWirePorts()

    if (self:GetStorageType() == nil) then
        return
    end

    local Storage = self:GetStorage(nil, true)
    Storage[self:GetStorageType()] = GAMEMODE:NewStorage(self:GetStorageType(), 0)

    local ResourcesOfType = GAMEMODE:GetResourcesOfType(StorageType)
    local total = 0
    for i,k in pairs(self.temp_storage) do
        if ResourcesOfType[i] then
            total = total + k
        end
    end

    for i,k in pairs(self.temp_storage) do
        local default = GAMEMODE:NewResource(i)
        if default and default:GetType() == self:GetStorageType() then
            local max = (default:GetSize() * k * self:GetStorageOfType(self:GetStorageType()):GetSize()) / total
            Storage[self:GetStorageType()]:AddResourceType(i, max)
        end
    end
end

--------------------------------------------------
-- Wire stuff
--------------------------------------------------
function ENT:GetWirePorts()
	local inputs = {}
	local outputs = {}

	inputs[#inputs+1] = "Hide overlay"

	return inputs, outputs
end

function ENT:TriggerInput( name, value )
    if name == "Hide overlay" then
		self.HideOverlay = (value ~= 0)
	end
end

function ENT:OnUnlink()
	return self:OnUnlinked( self:GetNode() )
end

--------------------------------------------------
-- AddResource
--------------------------------------------------
function ENT:GetStorageType()
    return self.type
end

function ENT:SetStorageType(type)
    if type == nil or type == "" then return end

    local storage = {}
    storage[type] = GAMEMODE:NewStorage(type, self:GetBaseMultiplier() * 0.01905)
    self:SetStorage(storage, true)
    self.type = type
end

function ENT:AddResource(name, mul)
    if not name or not mul then return end
	self.temp_storage[name] = mul
end

function ENT:SetupStorage() end

--------------------------------------------------
-- Multipliers
--------------------------------------------------
function ENT:GetBaseMultiplier()
	return self.basemul
end

function ENT:SetBaseMultiplier( mul )
	self.basemul = mul
end

--------------------------------------------------
-- UpdateOverlay
--------------------------------------------------
function ENT:UpdateOverlay()
	if self.HideOverlay then
		self:SetOverlayText( "" )
		self:SetOverlayData( {} )
	else
		local storages =  self:GetStorageOfType(self:GetStorageType()) --TODO: Replace with internal storage types ONLY when dealing with dedicated storages

		local str = {}
		if self:GetStorageType() then
			str[1] = "==[ " .. self:GetStorageType() .. " Storage ]=="
		end

		if storages then
			for name,storage in pairs(storages.resources) do
				local amount = storage:GetAmount()
				local max = storage:GetMaxAmount()

				if max ~= 0 then
					local percent = math.Round(amount / max * 100)

					str[#str+1] = string.format( "%s: %s/%s (%s%%)",
												name,
												nicenumber.formatDecimal( amount ),
												nicenumber.formatDecimal( max ),
												percent )
				end
			end
		end

		self:SetOverlayText( table.concat( str, "\n" ) )
	end
end


--------------------------------------------------
-- Think
--------------------------------------------------

function ENT:Think()
	local ret = base.Think( self )
	self:UpdateOverlay()
	return ret
end