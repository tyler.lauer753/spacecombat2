--[[
	Personal Ore Receiver for SB3 Mining

	Author: F�hrball w/ Steeveeo
]]--
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_lsentity"

ENT.PrintName = "Personal Miner Receiver"
ENT.Author = "Fuhrball"

ENT.Purpose = "Where the ore goes for the Personal Miner."
ENT.Instructions = "Supply energy, Link to Personal Miner, Turn on."

hook.Add("InitPostEntity", "personal_ore_receiver_toolinit", function()
    local Generator = GAMEMODE:NewGeneratorInfo()
    Generator:SetName("Personal Ore Receiver")
    Generator:SetClass("personal_ore_receiver")
    Generator:SetDescription("Where the ore goes for the Personal Miner.")
    Generator:SetCategory("Mining")
    Generator:SetDefaultModel("models/sbep_community/d12shieldemitter.mdl")

    GAMEMODE:RegisterGenerator(Generator)
end)

function ENT:SetupGenerator()
end

if SERVER then
    --Variable Definitions
    ENT.EnergyUsage = 3000
    ENT.EnergySupply = false
    ENT.ResourceReturn = 25
    ENT.RefireRate = 1

    function ENT:Initialize()
	    self.BaseClass.Initialize(self)

	    self.Miners = {}
	    self.ActiveMiners = {}

	    --Wire IO
	    if WireLib ~= nil then
		    self.Outputs = WireLib.CreateSpecialOutputs(self, { "Miners Active", "Miners Connected", "Energy Usage", "Mined" }, {"NORMAL", "NORMAL", "NORMAL", "ARRAY"} )
	    end
    end

    function ENT:AddMiner(Miner)
	    table.insert(self.Miners,Miner)
    end

    function ENT:RemoveMiner(Index)
	    table.remove(self.Miners,Index)
    end

    function ENT:Think()
	    --Reset Wire
	    Wire_TriggerOutput(self, "Miners Active", 0)
	    Wire_TriggerOutput(self, "Miners Connected", 0)
	    Wire_TriggerOutput(self, "Energy Usage", 0)
	    Wire_TriggerOutput(self, "Mined", {})

	    --Which miners are active, and are they still valid entities?
	    local MinerChecks = self.Miners
	    self.Miners = {}
	    self.ActiveMiners = {}
	    self.EnergySupply = false

	    --Power Check for attempted enabling
	    if IsValid(self:GetNode()) and self:GetAmount("Energy") >= self.EnergyUsage then
		    self.EnergySupply = true
	    end

	    --Filters out Miner Table for nil entries
	    for k,Miner in pairs(MinerChecks) do
		    if IsValid(Miner) and (#self.Miners <= 4) then
			    table.insert(self.Miners, Miner)

			    if Miner.Pointing then
				    table.insert(self.ActiveMiners, Miner)
			    end
		    end
	    end

	    Wire_TriggerOutput(self, "Miners Connected", #self.Miners)

	    --Display/Wire change while inactive
	    if #self.ActiveMiners < 1 then
		    self.Active = 0
	    end

	    --Resource Grab per Active Miner
	    for k,AMiner in pairs(self.ActiveMiners) do

		    --Secondary power check for laser maintain
		    self.EnergySupply = false

		    if self:GetAmount("Energy") < self.EnergyUsage or not IsValid(self:GetNode()) then
			    self.Active = 0
		    else
			    --It drains energy
			    self:ConsumeResource("Energy", self.EnergyUsage)
			    self.EnergySupply = true

			    --An Entity is present
			    if IsValid(AMiner.Ent) then
				    local Ent = AMiner.Ent

				    --Is it a roid?
				    if Ent.MiningSequence then
					    --Mine the 'Roid!
					    local Receive = Ent:MiningSequence(self.ResourceReturn)

					    --Give Returns
					    self:SupplyResources(Receive)
					    Wire_TriggerOutput(self, "Mined", Receive)
				    else
					    Ent:TakeDamage(self.Damage,self)
				    end
			    end
		    end
	    end

	    Wire_TriggerOutput(self, "Miners Active", #self.ActiveMiners)
	    Wire_TriggerOutput(self, "Energy Usage", (#self.ActiveMiners * self.EnergyUsage))

	    self:NextThink(CurTime() + self.RefireRate)
	    return true
    end

    duplicator.RegisterEntityClass("personal_ore_receiver", GAMEMODE.MakeEnt, "Data")
end