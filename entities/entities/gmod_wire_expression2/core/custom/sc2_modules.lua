--[[
    These functions can be used on any entity derived from base_moduleentity

    Function List:
        n isSC2Module
        n sc2ModuleEnabled
        n sc2ModuleCycling
        n sc2ModuleHasCycle
        s sc2ModuleName
        n sc2ModuleCyclePercent
        n sc2ModuleCycleDuration
        t sc2ModuleCycleActivationCost
        t sc2ModulePassiveBonuses
]]

e2function number entity:isSC2Module()
	if IsValid(this) and this.IsSCModule then
		return 1
    end

    return 0
end

e2function number entity:sc2ModuleEnabled()
	if IsValid(this) and this.IsSCModule and this:GetEnabled() then
		return 1
    end

    return 0
end

e2function number entity:sc2ModuleCycling()
	if IsValid(this) and this.IsSCModule and this:GetCycling() then
		return 1
    end

    return 0
end

e2function number entity:sc2ModuleHasCycle()
	if IsValid(this) and this.IsSCModule and this:GetHasCycle() then
		return 1
    end

    return 0
end

e2function string entity:sc2ModuleName()
	if IsValid(this) and this.IsSCModule then
		return this:GetModuleName()
    end

    return ""
end

e2function number entity:sc2ModuleCyclePercent()
	if IsValid(this) and this.IsSCModule and this:GetHasCycle() then
		return this:GetCyclePercent()
    end

    return 0
end

e2function number entity:sc2ModuleCycleDuration()
	if IsValid(this) and this.IsSCModule and this:GetHasCycle() then
		return this:GetCycleDuration()
    end

    return 0
end

e2function table entity:sc2ModuleCycleActivationCost()
    if IsValid(this) and this.IsSCModule then
        local Out = {n = {}, s = {}, ntypes = {}, stypes={}, size=0, istable=true}

        for i,k in pairs(this:GetCycleActivationCost()) do
            if type(i) == "string" and type(k) == "number" then
                Out.s[i] = k
                Out.stypes[i] = "n"
                Out.size = Out.size + 1
            end
        end

        return Out
    end

    return {}
end

e2function table entity:sc2ModulePassiveBonuses()
    if IsValid(this) and this.IsSCModule then
        local Out = {n = {}, s = {}, ntypes = {}, stypes={}, size=0, istable=true}

        for i,k in pairs(this:GetPassiveBonuses()) do
            if type(i) == "string" and type(k) == "number" then
                Out.s[i] = k
                Out.stypes[i] = "n"
                Out.size = Out.size + 1
            end
        end

        return Out
    end

    return {}
end