--[[
	Minable Terrestrial Rock Entity

	Author: Steeveeo
]]--
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "mining_asteroid"

ENT.PrintName = "Ore-Rich Rock"
ENT.Author = "Steeveeo"
ENT.Contact = "http://diaspora-community.com"
ENT.Purpose = "I got a rock..."
ENT.Instructions = ""

ENT.Spawnable = false
ENT.AdminSpawnable = true

if SERVER then
    -----
    ----All of this will be set by the server, these are just placeholders!--
    -----

    --Default hold values
    ENT.Resources = {Veldspar = 5000}
    ENT.HasCloud = false

    ----End of Placeholders--

    --This is called right before this spawns,
    --Server should already have provided values by now.
    function ENT:Initialize()
	    self.BaseClass.Initialize(self)
		
		self:SetName("Ore-Rich Rock")
    end

    function ENT:Think()
	    return true
    end

    function ENT:OnRemove()
		--[[
	    for i,k in pairs(GAMEMODE.Mining.Asteroids) do
		    if k == self then
			    table.remove(GAMEMODE.Mining.Asteroids, i)
			    break
		    end
	    end
		]]
    end

    --Placeholder functions
    function ENT:Touch()
    end

    function ENT:StartTouch()
    end

    function ENT:EndTouch()
    end
end