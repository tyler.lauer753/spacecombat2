AddCSLuaFile()
DEFINE_BASECLASS("base_anim")

ENT.PrintName = "Infinity Planet"
ENT.Author = "Radon"
ENT.Contact = "sb@inp.io"

ENT.celestial = nil -- This refers to what will be the parent object which houses env and ent data

ENT.Spawnable = false
ENT.AdminOnly = false

function ENT:Initialize()
	if SERVER then
		self:SetModel("models/props_lab/huladoll.mdl")
	end
end

if SERVER then

	function ENT:StartTouch( ent )
		--Add player to the environment
        if not ent.GetEnvironment or ent:GetEnvironment() ~= self:GetCelestial():GetEnvironment() then
			self:GetCelestial():GetEnvironment():SetEnvironment(ent, self:GetCelestial():GetEnvironment())
		end
	end

	function ENT:EndTouch( ent )
		--Remove player from the environment
        if not ent.GetEnvironment or ent:GetEnvironment() == self:GetCelestial():GetEnvironment() then
            GAMEMODE:getSpace():SetEnvironment(ent)
		end
	end

	function ENT:Think()
		self:GetCelestial():Think()
        self:NextThink(CurTime() + 1)
        return true
	end

end

if CLIENT then
	function ENT:Draw()
		-- So the model doesn't draw
	end
end

function ENT:CanTool()
	return false -- So the ent cannot be tooled ( parent, rope etc )
end

function ENT:GravGunPunt()
	return false -- So the player can't move the planet
end

function ENT:GravGunPickupAllowed()
	return false -- So the player can't pick the planet up and run off with it.
end
