AddCSLuaFile()

DEFINE_BASECLASS("base_moduleentity")

ENT.PrintName = "Base Life Support Generator"
ENT.Author = "Divran, Lt.Brandon"
ENT.Contact = "arviddivran@gmail.com"
ENT.Purpose = "Base for resource generation"
ENT.Instructions = "Derive from this for anything with resource generation"

ENT.Spawnable = false
ENT.AdminOnly = false

local base = scripted_ents.Get("base_moduleentity")
hook.Add( "InitPostEntity", "base_generatorentity_post_entity_init", function()
	base = scripted_ents.Get("base_moduleentity")
end)

--------------------------------------------------
-- Shared Init & Setup
--------------------------------------------------
function ENT:SharedInit()
    base.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "Multiplier", "number", 1)
	SC.NWAccessors.CreateNWAccessor(self, "BaseMultiplier", "number", 1)
	SC.NWAccessors.CreateNWAccessor(self, "GeneratorClass", "cachedstring", "Drone")
	SC.NWAccessors.CreateNWAccessor(self, "Production", "table", {})

	self.Production_num = 0
	self.Consumption_num = 0
end

function ENT:SetupGenerator()
    -- Empty function to make it so non-sc_generator classes can easily be spawned from the LS Spawner tool
end

--------------------------------------------------
-- Overlay Text
--------------------------------------------------

function ENT:GetResourceOverlayText()
    local basemul = self:GetBaseMultiplier() * self:GetMultiplier()

	local Consumption = {}
	for name, v in pairs(self:GetCycleActivationCost()) do
		local required = basemul * v.mul

        if v.atmosphere then
            if self.GetEnvironment then
                local Environment = self:GetEnvironment()
                local HasResource = Environment:GetResource(name) ~= nil
                if HasResource then
                    local Resource = Environment:GetResource(name)
                    local available = Resource:GetAmount()

                    local percent = 0
			        if required > 0 and HasResource then
				        percent = math.Round(available / required * 100)
				        if percent > 100 then percent = ">100" end
			        end

                    Consumption[#Consumption+1] = string.format("%s: %s/s (Available: %s%%) From Environment",
				    name,
				    nicenumber.formatDecimal(required),
				    percent)
                end
            end
        else
			local available = self:GetAmount(name)

			local delta = math.abs(self:GetDelta(name))
			local time = delta > 0 and nicenumber.nicetime((available - required) / delta) or 0

			local percent = 0
			if required > 0 then
				percent = math.Round(available / required * 100)
				if percent > 100 then percent = ">100" end
			end

			Consumption[#Consumption+1] = string.format("%s: %s/s (Available: %s%%, %s) ",
				name,
				nicenumber.formatDecimal(required),
				percent,
				time)
        end
	end
	if #Consumption > 0  then
		Consumption = "==[ Consumption ]==\n"..table.concat(Consumption, "\n")
	else
		Consumption = ""
	end

	local Production = {}
	for name, v in pairs(self:GetProduction()) do
		local producing = basemul * v.mul

        if v.atmosphere then
            if self.GetEnvironment then
                local Environment = self:GetEnvironment()
                local HasResource = Environment:GetResource(name) ~= nil
                local Resource = Environment:GetResource(name)
                local max = HasResource and Resource:GetMaxAmount() or 0
                local amount = HasResource and Resource:GetAmount() or 0

                local percent = 0
			    if max > 0 then
				    percent = math.Round(amount / max * 100)
			    end

			    Production[#Production+1] = string.format("%s: %s/s (Available: %s%%) Into Environment",
				    name,
				    nicenumber.formatDecimal(producing),
				    percent)
            end
        else
			local amount = self:GetAmount(name)
			local max = self:GetMaxAmount(name)

			local delta = math.abs(self:GetDelta(name))
			local time = delta > 0 and nicenumber.nicetime((max - amount) / delta) or 0

			local percent = 0
			if max > 0 then
				percent = math.Round(amount / max * 100)
			end

			Production[#Production+1] = string.format("%s: %s/s (Available: %s%%, %s) ",
				name,
				nicenumber.formatDecimal(producing),
				percent,
				time)
            end
	end
	if #Production > 0  then
		Production = (Consumption ~= "" and "\n\n" or "").."==[ Production ]==\n"..table.concat(Production, "\n")
	else
		Production = ""
	end

    return Consumption, Production
end

function ENT:GetFittingText()
    local Fitting = self:GetFitting()
    local Text = "\n\n-Fitting-"

    if Fitting.CPU > 0 then Text = Text.."\nCPU: "..Fitting.CPU end
    if Fitting.PG > 0 then Text = Text.."\nPG: "..Fitting.PG end
    if Fitting.Slot ~= "None" then Text = Text.."\nSlot: "..Fitting.Slot end

	if Text == "\n\n-Fitting-" then
		Text = ""
	end

	Text = Text.."\nClass: "..self:GetGeneratorClass()
    Text = Text.."\nLink Status: "..Fitting.Status

    return Text
end

if CLIENT then
	function ENT:Think()
        if not self:BeingLookedAtByLocalPlayer() then return end

        if self:GetHideOverlay() then
            self:SetOverlayText("")
            return
        end

        local Text = self:GetModuleName().."\n\n"
        local BonusText = self:GetBonusText()
        local ConsumptionText, ProductionText = self:GetResourceOverlayText()
        local FittingText = self:GetFittingText()

        if self:GetHasCycle() then
            Text = Text.."Status: "
            if self:GetCycling() then
                Text = Text.."Cycling\n"
            else
                Text = Text.."Off\n"
            end
        end

        Text = Text.."Multiplier: "..(math.floor(self:GetMultiplier() * 100) / 100).."\n\n"

        if BonusText then
            Text = Text..BonusText.."\n"
        end

		Text = Text..ConsumptionText..ProductionText.."\n"

        if self:GetHasCycle() then
			if self:GetCycleDuration() > 1 then
				Text = Text.."\nDuration: "..sc_ds(self:GetCycleDuration())
				Text = Text.."\nCycle: "..sc_ds(self:GetCyclePercent() * 100) .. "%"
			end
        end

        Text = Text..FittingText

        self:SetOverlayText(Text)
    end
end

--------------------------------------------------
-- Fitting
--------------------------------------------------

local GeneratorClasses = {
    111980,
    780005,
    1521819,
    6798551,
    17074000,
    27349448,
    54698904,
    109397808
}

function ENT:CalculateUsableClasses()
    local volume = self:GetPhysicsObject():GetVolume()

    self.UsableClasses = {
        "Drone",
        "Fighter",
        "Frigate",
        "Cruiser",
        "Battlecruiser",
        "Battleship",
        "Dreadnaught",
        "Titan"
    }

    local Class = "Unusably Large"

    for i,k in pairs(self.UsableClasses) do
        if volume > GeneratorClasses[i] then
            self.UsableClasses[i] = nil
        else
            Class = k
            break
        end
    end

	self:SetGeneratorClass(Class)
end

function ENT:SetupFitting(CPU, PG, Slot, Classes)
    self:CalculateUsableClasses()
    self:SetFitting({
		CPU = CPU or 0,
		PG = PG or 0,
		Slot = Slot or "None",
		CanRun = false,
		Status = "Offline",
		Classes = Classes or self.UsableClasses
	})
end

--------------------------------------------------
-- Shared Production & Consumption
--------------------------------------------------
function ENT:AddProduction(name, mul, autoshutdown, onfull, onempty, atmosphere)
	local Production = self:GetProduction()
	if not Production[name] then self.Production_num = self.Production_num + 1 end
	Production[name] = {
		mul = mul,
		autoshutdown = autoshutdown == nil and true or autoshutdown,
		onfull = onfull,
		onempty = onempty,
        atmosphere = atmosphere
	}
	self:SetProduction(Production)
end

function ENT:AddConsumption(name, mul, autoshutdown, onfull, onempty, atmosphere)
	local Consumption = self:GetCycleActivationCost()
	if not Consumption[name] then self.Consumption_num = self.Consumption_num + 1 end
	Consumption[name] = {
		mul = mul,
		autoshutdown = autoshutdown or false,
		onfull = onfull,
		onempty = onempty,
        atmosphere = atmosphere
	}
	self:SetCycleActivationCost(Consumption)
end

function ENT:RemoveProduction(name)
	local Production = self:GetProduction()
    if Production[name] then self.Production_num = self.Production_num - 1 end
    Production[name] = nil
	self:SetProduction(Production)
end

function ENT:RemoveConsumption(name)
	local Consumption = self:GetCycleActivationCost()
    if Consumption[name] then self.Consumption_num = self.Consumption_num - 1 end
    Consumption[name] = nil
	self:SetCycleActivationCost(Consumption)
end

if CLIENT then return end

function ENT:Setup()
    local volume = self:GetPhysicsObject():GetVolume()
	self:SetMultiplier(1)
	self:SetBaseMultiplier(volume / 1000)
    self:SetupFitting(0, 0, "None")
	self:SetCycleDuration(1)
	self:SetUseType(SIMPLE_USE)
	self:SetupWirePorts()
	self:SetTurnOnSound("ambient/machines/thumper_startup1.wav")
	self:SetTurnOffSound("vehicles/apc/apc_shutdown.wav")
	self:SetLoopSound("ambient/machines/combine_shield_loop3.wav")
	self:SetEnableSound("buttons/button16.wav")
end

--------------------------------------------------
-- Serverside Production & Consumption Checks
--------------------------------------------------
function ENT:CheckFullEmpty(v, name, production)
	-- call onfull if the storage is full
	if v.onfull then
        if v.atmosphere then
            if self.GetEnvironment then
                local Environment = self:GetEnvironment()
                if Environment and Environment:GetResource(name) ~= nil then
                    if Environment:GetResource(name):GetAmount() == Environment:GetResource(name):GetMaxAmount() then
                        v.onfull(self, name)
                    end
                end
            end
        elseif self:GetAmount(name) == self:GetMaxAmount(name) then
		    v.onfull(self, name)
        end
	end

	if v.onempty then
		if production then
            if v.atmosphere then
                if self.GetEnvironment then
                    local Environment = self:GetEnvironment()
                    if Environment and Environment:GetResource(name) ~= nil then
                        if Environment:GetResource(name):GetAmount() == 0 then
                            v.onempty(self, name)
                        end
                    end
                end
			elseif self:GetAmount(name) == 0 then
				v.onempty(self, name)
			end
		else
			-- call onempty if the storage is less than the amount required
			local amount_required = self:GetBaseMultiplier() * self:GetMultiplier() * v.mul
			if v.atmosphere then
                if self.GetEnvironment then
                    local Environment = self:GetEnvironment()
                    if Environment and Environment:GetResource(name) ~= nil then
                        if Environment:GetResource(name):GetAmount() < amount_required then
							v.onempty(self, name)
                        end
                    end
                end
			elseif self:GetAmount(name) < amount_required then
				v.onempty(self, name)
			end
		end
	end
end

function ENT:CheckResources()
    local basemul = self:GetBaseMultiplier() * self:GetMultiplier()
	local Consumption = self:GetCycleActivationCost()
	local Production = self:GetProduction()

    -- First call onfull and onempty of all resources
	for name, v in pairs(Consumption) do self:CheckFullEmpty(v, name, false) end
	for name, v in pairs(Production) do self:CheckFullEmpty(v, name, true) end

	-- Next, check if we have enough of each resource that this generator consumes
	for name, v in pairs(Consumption) do
		local mul = v.mul
		local autoshutdown = v.autoshutdown

		local amount = self:GetAmount(name)
		local amount_required = basemul * mul

        -- New Feature, check if the atmosphere has the resource if we're taking it from there
        local HasResource = false
        if v.atmosphere then
            if self.GetEnvironment then
                local Environment = self:GetEnvironment()
                HasResource = Environment:GetResource(name) ~= nil

                if HasResource then
                    HasResource = Environment:GetResource(name):GetAmount() >= amount_required
                end
            end
        else
            HasResource = amount >= amount_required
        end

		if not HasResource and autoshutdown then
			return false
		end
	end

	-- Then check if the network is already full of all resources that this generator produces
	local produce_num = 0
	for name, v in pairs(Production) do
		local amount = self:GetAmount(name)
		local max = self:GetMaxAmount(name)
		local autoshutdown = v.autoshutdown

		local HasResource = false
        if v.atmosphere then
            if self.GetEnvironment then
                local Environment = self:GetEnvironment()
                HasResource = Environment:GetResource(name) ~= nil

                if HasResource then
                    HasResource = Environment:GetResource(name):GetAmount() == Environment:GetResource(name):GetMaxAmount()
                end
            end
        else
            HasResource = amount == max
        end

		if HasResource then
			if autoshutdown then
				produce_num = produce_num + 1
			end
		end
	end

    if produce_num == self.Production_num then return false end

    return true
end

function ENT:ConsumeResources()
	local basemul = self:GetBaseMultiplier() * self:GetMultiplier()

	-- First, consume the resources
	for name, v in pairs(self:GetCycleActivationCost()) do
		local mul = v.mul
		local amount_required = basemul * mul

        if v.atmosphere then
            if self.GetEnvironment then
                local Environment = self:GetEnvironment()
                local Resource = Environment:GetResource(name)
                if Resource then
                    Resource:ConsumeResource(amount_required)
                end
            end
        else
            self:ConsumeResource(name, amount_required)
        end

		-- Check onfull/onempty again after consumption
		self:CheckFullEmpty(v, name)
	end
end

function ENT:GenerateResources()
	local basemul = self:GetBaseMultiplier() * self:GetMultiplier()

	-- Lastly, produce the resources
	for name, v in pairs(self:GetProduction()) do
		if v.atmosphere then
            if self.GetEnvironment then
                local Environment = self:GetEnvironment()
                if Environment and Environment:GetResource(name) then
                    Environment:GetResource(name):SupplyResource(basemul * v.mul)
                end
            end
        else
            self:SupplyResource(name, basemul * v.mul)
        end

		-- Check onfull/onempty again after production
		self:CheckFullEmpty(v, name, true)
	end
end


--------------------------------------------------
-- Wire inputs / outputs
--------------------------------------------------
function ENT:GetWirePorts()
	local inputs, outputs = {"On", "Mute", "Hide overlay"}, {"On"}
	if not self.NoMultiplierInput then table.insert(inputs, 2, "Multiplier") end -- insert this just before "Mute"

	for name, v in pairs(self:GetCycleActivationCost()) do
		outputs[#outputs+1] = "Required "..(v.atmosphere and "Atmospheric " or "")..name
	end

	for name, v in pairs(self:GetProduction()) do
		outputs[#outputs+1] = "Producing "..(v.atmosphere and "Atmospheric " or "")..name
	end

	return inputs, outputs
end

function ENT:TriggerInput(name, value)
	if name == "Multiplier" then
		self:SetMultiplier(math.max(value, 1))
	end

	base.TriggerInput(self, name, value)
end

function ENT:UpdateOutputs()
	local basemul = self:GetBaseMultiplier() * self:GetMultiplier()

	if self.old_mul ~= basemul + (self:GetCycling() and 1 or 0) then
		self.old_mul = basemul + (self:GetCycling() and 1 or 0) -- adding this here just to make sure this if statement runs whenever this changes

        if WireLib ~= nil then
            for name, v in pairs(self:GetCycleActivationCost()) do
                WireLib.TriggerOutput(self, "Required "..(v.atmosphere and "Atmospheric " or "")..name, basemul * v.mul)
            end

            for name, v in pairs(self:GetProduction()) do
                WireLib.TriggerOutput(self, "Producing "..(v.atmosphere and "Atmospheric " or "")..name, basemul * v.mul)
            end
        end
	end
end

--------------------------------------------------
-- Think
--------------------------------------------------
function ENT:CanCycle()
	return self:GetHasCycle() and IsValid(self:GetProtector()) and self:GetFitting().CanRun and self:CheckResources()
end

function ENT:OnCycleStarted()
	-- Update wire
	self:UpdateOutputs()
end

function ENT:OnCycleFinished()
	-- Generate the resources
	self:GenerateResources()
end