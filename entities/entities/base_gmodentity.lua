
AddCSLuaFile()
DEFINE_BASECLASS( "base_anim" )

ENT.PrintName		= ""
ENT.Author			= ""
ENT.Contact			= ""
ENT.Purpose			= ""
ENT.Instructions	= ""

ENT.Spawnable			= false
ENT.OverlayText			= ""

ENT.RenderGroup		= RENDERGROUP_BOTH

if ( CLIENT ) then

	ENT.LabelColor = Color( 255, 255, 255, 255 )

	function ENT:BeingLookedAtByLocalPlayer()
	    local trace = LocalPlayer():GetEyeTrace()

		if trace.Entity ~= self then return false end
		if trace.HitPos:Distance(LocalPlayer():GetShootPos()) > 200 then return false end

		return true
	end

end

function ENT:Draw()
	self:DrawModel()
	if ( CLIENT and self:BeingLookedAtByLocalPlayer() and (self:GetOverlayText() ~= "" or next( self:GetOverlayData() ) ~= nil)  ) then
		AddWorldTip( self:EntIndex(), self:GetOverlayText(), 0.5, self:GetPos(), self  )
	end
end

function ENT:UpdateOverlayData(ply)
	if self.SetOverlayData and self.SetOverlayText and ((self.OverlayData and next(self.OverlayData)) ~= nil or self.OverlayText ~= nil)
		and self.OverlayData_UpdateTime and self.OverlayData_UpdateTime.time > (self.OverlayData_UpdateTime[ply] or 0) then

		self.OverlayData_UpdateTime[ply] = CurTime()

		if self.OverlayData and next(self.OverlayData) ~= nil then
			net.Start("SetOverlayData")
			net.WriteEntity(self)
			net.WriteTable(self.OverlayData)
			net.Send(ply)
		end

		if (self.OverlayText ~= nil) then
			net.Start("SetOverlayText")
			net.WriteEntity(self)
			net.WriteString(self.OverlayText)
			net.Send(ply)
		end
	end
end

if SERVER then
	timer.Create("GMOD_OVERLAYUPDATE", 0.1, 0, function()
		for _, ply in ipairs(player.GetAll()) do
			local ent = ply:GetEyeTrace().Entity
			--Wiremod has its own overlay stuff, so lets not override them.

			if IsValid( ent ) and not ent.IsWire and ent.UpdateOverlayData then
				ent:UpdateOverlayData(ply)
			end
		end
	end)
end

net.Receive("SetOverlayData", function()
	if not CLIENT then return end

	local ent = net.ReadEntity()
	local Table = net.ReadTable()

	if not IsValid(ent) or not ent.SetOverlayData then return end

	ent:SetOverlayData(Table)
end)

function ENT:SetOverlayData( Table )
	if self.OverlayData == Table or not Table then return end

	self.OverlayData = Table

	if not self.OverlayData_UpdateTime then	self.OverlayData_UpdateTime = {} end
	self.OverlayData_UpdateTime.time = CurTime()
end

function ENT:GetOverlayData()
	return self.OverlayData or {}
end

net.Receive("SetOverlayText", function()
	if not CLIENT then return end

	local ent = net.ReadEntity()
	local text = net.ReadString()

	if not IsValid(ent) or not ent.SetOverlayText then return end

	ent:SetOverlayText(text)
end)

function ENT:SetOverlayText( text )
	if self.OverlayText == text or not text then return end

	self.OverlayText = text

	if not self.OverlayData_UpdateTime then	self.OverlayData_UpdateTime = {} end
	self.OverlayData_UpdateTime.time = CurTime()
end

function ENT:GetOverlayText()

	local txt = self.OverlayText

	if ( txt == nil or txt == "" ) then
		return ""
	end

	return txt

end


function ENT:SetPlayer( ply )

	if ( IsValid(ply) ) then

		self:SetVar( "Founder", ply )
		self:SetVar( "FounderIndex", ply:SteamID64() )

		self:SetNetworkedString( "FounderName", ply:Nick() )

	end

end

function ENT:GetPlayer()
    local ply = self:GetVar("Founder", NULL)

    --Player has disconnected, attempt to use fallback
	if not IsValid(ply) then
		for _,oply in pairs(player.GetAll()) do
			--Found the reconnected instance
			if oply:SteamID64() == self:GetPlayerIndex() then
				ply = oply
                self:SetPlayer(oply)
				break
			end
		end

		--If we still didn't find anyone, return nil
		if not IsValid(ply) then
            -- but wait, theres more!
            if CPPI then
                -- this is a dirty hax that should be fixed on the prop protection side
                -- damn dirty bastards and their inability to do things and remain compatible with gmod code!
                ply = self:CPPIGetOwner()

                if IsValid(ply) then return ply end
            end

			return nil
		end
	end

	return ply
end

function ENT:GetPlayerIndex()

	return self:GetVar( "FounderIndex", 0 )

end

function ENT:GetPlayerName()

	local ply = self:GetPlayer()
	if ( IsValid( ply ) ) then
		return ply:Nick()
	end

	return self:GetNetworkedString( "FounderName" )

end
