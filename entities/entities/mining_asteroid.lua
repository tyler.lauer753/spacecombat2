--[[
	Minable Asteroid Ent for Diaspora Mining

	Author: Steeveeo
]]--
AddCSLuaFile()

ENT.Type = "anim"
ENT.Base = "base_gmodentity"

ENT.PrintName = "Asteroid"
ENT.Author = "Steeveeo"
ENT.Contact = "http://diaspora-community.com"
ENT.Purpose = "I am an asteroid, chalk full of stuff!"
ENT.Instructions = ""

ENT.Spawnable = false
ENT.AdminSpawnable = true

if SERVER then
    -----
    ----All of this will be set by the server, these are just placeholders!--
    -----

    --Default hold values
    ENT.Resources = {Veldspar = 5000}
    ENT.HasCloud = false

    ----End of Placeholders--

    --This is called right before this spawns,
    --Server should already have provided values by now.
    function ENT:Initialize()
	    self:SetName("Asteroid")
	    self:PhysicsInit(SOLID_VPHYSICS)
	    self:SetMoveType(MOVETYPE_VPHYSICS)
	    self:SetSolid(SOLID_VPHYSICS)
	    self:DrawShadow(false)
		self:SetRenderMode(RENDERMODE_TRANSALPHA)

	    self.SC_Immune = true --make weapons ignore me
	    self.SB_Ignore = true --make spacebuild ignore me
	    self.Untouchable = true
	    self.Unconstrainable = true
	    self.PhysgunDisabled = true
	    self.CanTool = function()
		    return false
	    end

	    --Ownership crap
	    if(NADMOD) then --If we're using NADMOD PP, then use its function
		    NADMOD.SetOwnerWorld(self)
	    elseif(CPPI) then --If we're using SPP, then use its function
		    self:SetNWString("Owner", "World")
	    else --Well fuck it, lets just use our own!
		    self.Owner = game.GetWorld()
	    end

	    local phys = self:GetPhysicsObject()
	    if (phys:IsValid()) then
		    phys:Sleep()
		    phys:EnableGravity(false)
		    phys:EnableCollisions(true)
		    phys:EnableMotion(false)
		    phys:SetMass(50000)
	    end
    end

    function ENT:Think()
	    return true
    end

	--Runs after MiningSequence ends
	function ENT:OnMined()
		if not self.IsDead then
			local total = 0
			for i,k in pairs(self.Resources) do
				total = total + k
			end

			if total <= 0 then
				self:SetColor(Color(255, 255, 255, 0))

				local effect = {}
				effect.Origin = self:GetPos()
				effect.Entity = self
				SC.CreateEffect("mining_asteroiddeath", effect)

				self.IsDead = true
				timer.Simple(1, function()
					self:Remove()
				end)
			end
		end
	end

    --Placeholder functions
    function ENT:OnRemove()
    end
	
    function ENT:Touch()
    end

    function ENT:StartTouch()
    end

    function ENT:EndTouch()
    end
end