﻿AddCSLuaFile()

ENT.Type 			= "anim"
ENT.Base 			= "base_moduleentity"
ENT.PrintName		= "Repair Device"
ENT.Author			= "Lt.Brandon"
ENT.Category 		= "Space Combat"

ENT.Spawnable 		= false
ENT.AdminSpawnable 	= false

local base = scripted_ents.Get("base_moduleentity")
local scentity = scripted_ents.Get("base_scentity")
hook.Add( "InitPostEntity", "sc_module_booster_post_entity_init", function()
	base = scripted_ents.Get("base_moduleentity")
    scentity = scripted_ents.Get("base_scentity")
end)

function ENT:SharedInit()
    base.SharedInit(self)

    SC.NWAccessors.CreateNWAccessor(self, "Type", "cachedstring", "Hull")
    SC.NWAccessors.CreateNWAccessor(self, "BoostAmount", "number", 0)
end

function ENT:GetBonusText()
    local Bonuses = self:GetPassiveBonuses()

    local Text = self:GetType().." Boost: "..self:GetBoostAmount().." per Cycle\n"

    if next(Bonuses) then
        Text = Text.."\nPassive Bonuses:\n"
        for i,k in pairs(Bonuses) do
            Text = i..": "..k.."\n"
        end
    end
    return Text
end

if SERVER then
    function ENT:Initialize()
        self:SetModel("models/snakesvx/node_s.mdl")

        self.Boost = {
            TYPE = "Shield",
            ITEM = nil,
            ITEMAMOUNT = 0,
            AMOUNT = 1.00,
            CAP = 1.00,
            DUR = 1.00
        }

        base.Initialize(self)
    end

    local classes = { }
    classes["Tiny"] = 1
    classes["Small"] = 2
    classes["Medium"] = 3
    classes["Large"] = 4
    classes["X-Large"] = 5

    local reptypes = {}
    reptypes["Shield"] = {
        Booster = {
            Amount = 5800,
            CapacitorUsage = 4000,
            Time = 10,
            CPU = 130,
            PG = 180,
            Item = {"Tiny Capacitor Charge", "Small Capacitor Charge", "Medium Capacitor Charge", "Large Capacitor Charge", "X-Large Capacitor Charge"},
            ItemAmount = {1,1,1,1,1}
        },

        Infuser = {
            Amount = 10500,
            CapacitorUsage = 26000,
            Time = 20,
            CPU = 180,
            PG = 250
        }
    }

    reptypes["Armor"] = {
        Repair = {
            Amount = 13200,
            CapacitorUsage = 32000,
            Time = 25,
            CPU = 180,
            PG = 120
        },

        Nanites = {
            Amount = 6000,
            CapacitorUsage = 8000,
            Time = 10,
            CPU = 250,
            PG = 150,
            Item = "Nanite Canister",
            ItemAmount = {1,4,16,64,256}
        }
    }

    reptypes["Hull"] = {
        Repair = {
            Amount = 16000,
            CapacitorUsage = 30000,
            Time = 15,
            CPU = 150,
            PG = 180
        },

        Nanites = {
            Amount = 13000,
            CapacitorUsage = 15000,
            Time = 10,
            CPU = 225,
            PG = 250,
            Item = "Nanite Canister",
            ItemAmount = {1,4,16,64,256}
        }
    }

    reptypes["Capacitor"] = {
        Booster = {
            Amount = 20000,
            Time = 20,
            CPU = 300,
            PG = 600,
            Item = {"Tiny Capacitor Charge", "Small Capacitor Charge", "Medium Capacitor Charge", "Large Capacitor Charge", "X-Large Capacitor Charge"},
            ItemAmount = {1,1,1,1,1}
        }
    }

    local CapacitorExponent = 4.0
    local BoostExponent = 5.2
	local CPUExponent = 3.2
	local PGExponent = 3.5
    local MaxTime = 60

    duplicator.RegisterEntityClass("sc_module_booster", SC.MakeEnt, "Data")
    SC.ClassAlias("sc_module_booster", "sc_repair")
    function ENT:LoadSCInfo(Info)
        self.SCDupeInfo = Info or {}

        local Rep = Info.rep
        local Type = Info.type
        local Size = Info.size

        if reptypes[Rep] and reptypes[Rep][Type] and classes[Size] then
            local Level = classes[Size]
            local BoosterInfo = reptypes[Rep][Type]
            local Item = type(BoosterInfo.Item) == "table" and (BoosterInfo.Item or {})[Level] or BoosterInfo.Item
            local ItemAmount = type(BoosterInfo.ItemAmount) == "table" and (BoosterInfo.ItemAmount or {})[Level] or BoosterInfo.ItemAmount

            self.Boost = {
                TYPE = Rep,
                ITEM = Item,
                ITEMAMOUNT = ItemAmount or 0,
                AMOUNT = Level ^ BoostExponent * BoosterInfo.Amount,
                CAP = Level ^ CapacitorExponent * (BoosterInfo.CapacitorUsage or 0),
                DUR = math.min(math.max(Level * 0.5,1) * BoosterInfo.Time, MaxTime)
            }

            self:SetFitting({
                Slot = "Upgrade",
                CPU = math.floor(BoosterInfo.CPU * Level ^ CPUExponent),
                PG = math.floor(BoosterInfo.PG * Level ^ PGExponent),
                Status = "Offline",
                CanRun = false,
                Classes = {}
            })

            self:SetModuleName(Size.." "..Rep.." "..Type)
            self:SetType(Rep)
            self:SetBoostAmount(self.Boost.AMOUNT)

            local Cost = {}
            Cost["Energy"] = self.Boost.CAP
            if self.Boost.ITEM then Cost[self.Boost.ITEM] = self.Boost.ITEMAMOUNT end

            self:SetCycleActivationCost(Cost)
            self:SetCycleDuration(self.Boost.DUR)
        else
            self:SetModuleName("INVALID REPAIR MODULE")
            self:SetType("INVALID")
            self:SetHasCycle(false)
        end

        if IsValid(self:GetNode()) then
		    self:GetNode():UpdateModifiers()
	    end
    end

    function ENT:SaveSCInfo()
        return scentity.SaveSCInfo(self)
    end

    function ENT:OnCycleStarted()
        --What kind of boost should we give?
        if self.Boost.TYPE == "Shield" then
            self:GetNode():SupplyShield(self.Boost.AMOUNT)
        elseif self.Boost.TYPE == "Armor" then
            self:GetNode():SupplyArmor(self.Boost.AMOUNT)
        elseif self.Boost.TYPE == "Hull" then
            self:GetNode():SupplyHull(self.Boost.AMOUNT)
        elseif self.Boost.TYPE == "Capacitor" then
            self:GetNode():SupplyCap(self.Boost.AMOUNT)
        else
            return false
        end
    end
end