AddCSLuaFile()

DEFINE_BASECLASS("base_lsentity")

ENT.PrintName = "Suit Dispenser"
ENT.Author = "Lt.Brandon, Divran, and Steeveeo"
ENT.Contact = "diaspora-community.com"
ENT.Purpose = "A giant vacuum set to reverse."
ENT.Instructions = "Flip switch, get pelted with life support cans."

ENT.Spawnable = false
ENT.AdminOnly = false

hook.Add("InitPostEntity", "sc_dispenser_toolinit", function()
    local Generator = GAMEMODE:NewGeneratorInfo()
    Generator:SetName("Suit Dispenser")
    Generator:SetClass("sc_dispenser")
    Generator:SetDescription("Flip switch, get pelted with life support cans.")
    Generator:SetCategory("Life Support")
    Generator:SetDefaultModel("models/slyfo_2/acc_food_fooddispenser.mdl")

    GAMEMODE:RegisterGenerator(Generator)
end)

function ENT:SetupGenerator()
end

if CLIENT then
	return
end

function ENT:Setup()
	self:SetUseType( SIMPLE_USE )
	self.cooldown = 0
	self.local_canisters = 0

	self.disableProdution = false
	self.ownerOnly = false

	self:SetupWirePorts()
	self:UpdateOutputs()
end


------------------------------------------------------------ LOCAL STORAGE
local SUIT_STORAGE = 900
local energy_cost = 35
local oxygen_cost = 5
local max_production = 30	--Cans per second, should take about 30 seconds per full suit
function ENT:GenerateLocalStorage()
	local local_storage = ""
	if not self.disableProduction then
		if self.local_canisters < SUIT_STORAGE then
			--TODO: Max Usage input
			local energy = self:GetAmount("Energy")
			local oxygen = self:GetAmount("Oxygen")

			--Pick the lowest amount we can make with our stores
			local maxEn = math.floor(energy / energy_cost)
			local maxO2 = math.floor(oxygen / oxygen_cost)
			local empty = math.floor(SUIT_STORAGE - self.local_canisters)
			local max = math.min(max_production, maxEn, maxO2, empty)
			if max > 0 then
				self:ConsumeResource("Energy", max * energy_cost)
				self:ConsumeResource("Oxygen", max * oxygen_cost)
				self.local_canisters = self.local_canisters + max
			end

			local_storage = "\nFilling Canisters" .. ("."):rep( CurTime() % 3 )
		end
	else
		local_storage = "\nCanister Production disabled..."
	end

	return local_storage
end

function ENT:CheckLocalStorage( ply, max, current_amount, wanted_amount )
	if self.local_canisters > 0 and wanted_amount > 0 then
		wanted_amount = math.min( wanted_amount, self.local_canisters )

		self.local_canisters = self.local_canisters - wanted_amount
		ply.SB4Suit:SupplyIntake( wanted_amount, true )

		wanted_amount = max - ply.SB4Suit:getIntake():GetAmount() -- see if they still want more
		if wanted_amount == 0 then -- now they don't want any more, so play the sound and exit
    		self:EmitSound( "buttons/lever5.wav" )
			return true
		end

		if self:GetAmount( "Lifesupport Canister" ) == 0 then return true end -- there's nothing more in the system, abort now so that we don't emit the error sound
	end

	return false
end
------------------------------------------------------------ LOCAL STORAGE

--------------------------------------------------
-- Wire stuff
--------------------------------------------------
function ENT:GetWirePorts()
	local inputs = {}
	local outputs = {}

	inputs[#inputs+1] = "Disable Production"
	inputs[#inputs+1] = "Owner Only"

	return inputs, outputs
end

function ENT:TriggerInput( name, value )
    if name == "Disable Production" then
		self.disableProduction = (value ~= 0)
	elseif name == "Owner Only" then
		self.ownerOnly = (value ~= 0)
	end
end

function ENT:UpdateOutputs()
	local local_storage = self:GenerateLocalStorage()
	local amount = self:GetAmount( "Lifesupport Canister" ) + self.local_canisters

	local canisters = nicenumber.formatDecimal(amount)
	local percent = math.Round( amount/SUIT_STORAGE * 100)

	if self.ownerOnly then
		self:SetOverlayText( string.format( "==[ Owner Locked ]==\nAvailable Lifesupport Canisters: %s\nWhich is %s%% of suit storage capacity%s", canisters, percent, local_storage ) )
	else
		self:SetOverlayText( string.format( "Available Lifesupport Canisters: %s\nWhich is %s%% of suit storage capacity%s", canisters, percent, local_storage ) )
	end
end

function ENT:Use( ply )
	if not IsValid( ply ) or not ply.SB4Suit then return end
	if ply:GetShootPos():Distance( self:GetPos() ) > 256 then return end
	if self.cooldown > CurTime() then return end

	self.cooldown = CurTime() + 1

	--Owner Lock
	if self.ownerOnly and self.CPPICanTool and ply ~= self:CPPIGetOwner() then
		self:EmitSound( "buttons/weapon_cant_buy.wav" )
		return
	end

	-- Take all their outtake first
	local amt = ply.SB4Suit:getOuttake():GetAmount()
	ply.SB4Suit:ConsumeOuttake(amt)
	self:SupplyResource("Empty Canister", amt)

	local max = ply.SB4Suit:getIntake():GetMaxAmount()
	local current_amount = ply.SB4Suit:getIntake():GetAmount()

	local wanted_amount = max - current_amount

	-- check local storage first
	if self:CheckLocalStorage( ply, max, current_amount, wanted_amount ) then return end

	if wanted_amount == 0 then
    	self:EmitSound( "buttons/weapon_cant_buy.wav" )
		return
	end

	local current_stored = self:GetAmount( "Lifesupport Canister" )

	if current_stored == 0 then
    	self:EmitSound( "buttons/weapon_cant_buy.wav" )
		return
	end

	wanted_amount = math.min( wanted_amount, current_stored )

	if self:ConsumeResource("Lifesupport Canister", wanted_amount) then
		ply.SB4Suit:SupplyIntake(wanted_amount, true)
		self:EmitSound( "buttons/lever5.wav" )
	else
		self:EmitSound( "buttons/weapon_cant_buy.wav" )
	end
end

duplicator.RegisterEntityClass("sc_dispenser", GAMEMODE.MakeEnt, "Data")
