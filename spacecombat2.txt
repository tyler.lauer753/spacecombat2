"spacecombat2"
{
	"base"		"base"
	"title"		"Space Combat 2"
	"maps"		"^sb_"
	"menusystem"	"1"

	"settings"
	{
		1
		{
			"name"		"sc_wreckage_enable"
			"text"		"Enable Wreckage"
			"help"		"Enables ship wrecks spawning when ships are destroyed."
			"type"		"CheckBox"
			"default"	"1"
            "singleplayer"	"1"
		}

		2
		{
			"name"		"mining_asteroidRespawnEnable"
			"text"		"Enable Asteroid Respawning"
			"help"		"Enables asteroid respawning after some time."
			"type"		"CheckBox"
			"default"	"1"
            "singleplayer"	"1"
		}

        3
		{
			"name"		"sb_infiniteresources"
			"text"		"Enable Infinite Resource"
			"help"		"Enables infinte resources on planets."
			"type"		"CheckBox"
			"default"	"0"
            "singleplayer"	"1"
		}

        4
		{
			"name"		"sb_adminspacenoclip"
			"text"		"Enable Admin Space Noclip"
			"help"		"Allow admins to noclip in space."
			"type"		"CheckBox"
			"default"	"1"
            "singleplayer"	"1"
		}

        5
		{
			"name"		"sc_fastcorecalculations"
			"text"		"Enable Fast Core Calculations"
			"help"		"Switches between fast or accurate core calculations."
			"type"		"CheckBox"
			"default"	"1"
            "singleplayer"	"1"
		}
	}
}
